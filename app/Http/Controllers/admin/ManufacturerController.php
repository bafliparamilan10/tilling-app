<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\admin\ManufacturerRepositoryInterface;
use App\DataTables\ManufacturerDataTable;
use App\Http\Requests\admin\CreateManufactureRequest;
use App\Models\Manufacturer;
use App\Models\Suppliers;
use Illuminate\Support\Facades\Crypt;

class ManufacturerController extends Controller
{
    private ManufacturerRepositoryInterface $manufactureRepository;

    public function __construct(ManufacturerRepositoryInterface $manufactureRepository)
    {
        $this->manufactureRepository = $manufactureRepository;
    }

    public function list(ManufacturerDataTable $dataTable)
    {
        return $dataTable->render('admin.manufacturer.list');
    }

    public function addManufacturer()
    {
        $suppliers = Suppliers::where('status', 1)->orderBy('id', 'DESC')->get();

        return view('admin.manufacturer.add',compact('suppliers'));
    }

    public function manufacturerEdit($id)
    {
        $id = Crypt::decrypt($id);
        $suppliers = Suppliers::where('status', 1)->orderBy('id', 'DESC')->get();
        $manufacturerData = Manufacturer::find($id);

        return view('admin.manufacturer.edit',compact('suppliers','manufacturerData'));
    }

    public function manufacturerCreate(CreateManufactureRequest $request)
    {
        $data =  $this->manufactureRepository->createManufacturer($request->all());
        return $data;
    }

    public function manufacturerView($id)
    {
        $id = Crypt::decrypt($id);
        $manufactureData = Manufacturer::where('id', $id)->first();
        $totalSuppliers = Suppliers::where('manufacture_id', $id)->count();

        return view('admin.manufacturer.view', compact('manufactureData','totalSuppliers'));
    }

    public function deleteManufacturer(Request $request)
    {
        $data =  $this->manufactureRepository->deleteManufacturer($request['id']);
        return $data;
    }

    public function ManufacturerEnableDisable(Request $request)
    {
        $response = $this->manufactureRepository->manufacturerEnableDisable($request->all());
        return  $response;
    }

}
