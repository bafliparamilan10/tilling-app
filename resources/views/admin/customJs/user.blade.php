<script>
    $(function () {
        // $('.select2').select2();
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });

        //datepiker

        $('#dob').datetimepicker({
            format: 'DD-MM-YYYY',
            defaultDate: new Date(),
            maxDate: new Date(),
        });
        $("#dob").attr("readonly", true);

        $('#userForm').validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name : {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                user_type: {
                    required: true,
                },
                city_id: {
                    required: true,
                },
                state_id: {
                    required: true,
                },
                dob: {
                    required: true,
                },
                gender: {
                    required: true,
                },
                status: {
                    required: true,
                },

            },
            messages: {
                first_name: {
                    required: "Please enter first name"
                },
                last_name: {
                    required: "Please enter last name"
                },
                user_type: {
                    required: "Please select user type"
                },
                email: {
                    required: "Please enter a email address",
                    email: "Please enter a valid email address"
                },
                city_id: {
                   required: "please select city"
                },
                state_id: {
                   required: "please select state"
                },
                dob: {
                   required: "please select date of birth"
                },
                gender: {
                   required: "please select gender"
                },
                status: {
                   required: "please select status"
                },

            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function (form) {
                var form = $('#userForm')[0];
                var data = new FormData(form);
                submitForm(data);
            }
        });

        $('#usereditForm').validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name : {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                user_type: {
                    required: true,
                },
                city_id: {
                    required: true,
                },
                state_id: {
                    required: true,
                },
                dob: {
                    required: true,
                },
                gender: {
                    required: true,
                },
                status: {
                    required: true,
                },

            },
            messages: {
                first_name: {
                    required: "Please enter first name"
                },
                last_name: {
                    required: "Please enter last name"
                },
                user_type: {
                    required: "Please select user type"
                },
                email: {
                    required: "Please enter a email address",
                    email: "Please enter a valid email address"
                },
                city_id: {
                   required: "please select city"
                },
                state_id: {
                   required: "please select state"
                },
                dob: {
                   required: "please select date of birth"
                },
                gender: {
                   required: "please select gender"
                },
                status: {
                   required: "please select status"
                },

            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function (form) {
                var form = $('#userForm')[0];
                var data = new FormData(form);
                submitForm(data);
            }
        });
    });

    function submitForm(form) {
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                url: '{{ route('admin.user-create') }}',
                type: 'post',
                processData: false,
                contentType: false,
                data: form,
                beforeSend: function() {
                $(".data-loader").addClass('loading');
                },
                success: function(response) {
                    if (response.status == true) {
                        $(".data-loader").removeClass('loading');
                        toastr.success('{{ config('app.name') }}', response.message)
                        setTimeout(() => {
                            window.location.href = '{{ route('admin.user-list') }}';
                        }, 1000);
                    } else {
                        $(".data-loader").removeClass('loading');
                        toastr.error('{{ config('app.name') }}', response.message)
                    }
                },
                error: function(data) {
                    $(".data-loader").removeClass('loading');
                    errorHandler(data);
                }
            });
    }
    function deleteUser(id)
    {
        Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#5B73E8',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                url:"{{route('admin.user-delete')}}",
                type: 'delete',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {_method: 'Delete', id:id},
                success:function(msg){
                    console.log(msg);
                    if(msg.status == true){
                        Swal.fire("Deleted!",  msg.message, "success");
                        window.LaravelDataTables["user-table"].ajax.reload();
                    }else{
                        Swal.fire("Warning!", msg.message, "warning");
                    }
                },
                error:function(){
                    Swal.fire("Error!", 'Error in delete Record', "error");
                }
                });
            }
        })
    };

    $('#doba').keypress(function(e) {
        e.preventDefault();
    });

    $('#status-filter').on('change', function(e) {

        $("#user-table").on('preXhr.dt', function(e, settings, data) {
            data.status = $("#status-filter").val();
            console.log(data.status)
        });

        window.LaravelDataTables["user-table"].draw()
        e.preventDefault();
    });

    $('#subscription-filter').on('change', function(e) {

    $("#user-table").on('preXhr.dt', function(e, settings, data) {
        data.subscription = $("#subscription-filter").val();
        console.log(data.subscription)
    });

    window.LaravelDataTables["user-table"].draw()
    e.preventDefault();
    });

    $('#state-filter').on('change', function(e) {

    $("#user-table").on('preXhr.dt', function(e, settings, data) {
        data.state = $("#state-filter").val();
        console.log(data.state)
    });

    window.LaravelDataTables["user-table"].draw()
    e.preventDefault();
    });


function getCity(val)
{
    var id = val.value;

    $.ajax({
          url:"{{route('admin.get-city')}}",
          type: 'post',
          headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {id:id},
          success:function(html){
            $("#city_id").html(html);
          },
          error:function(){
            Swal.fire("Error!", 'Error in fetch city record', "error");
          }
        });
}


function activeDeactive(status, id){

    var lable = status == 0 ? 'active' : 'inactive';
    Swal.fire({
        title: 'Are you sure?',
        text: "You wont to be able to "+ lable +" this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#5B73E8',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, change it!'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                url:"{{route('admin.user-enable-disable')}}",
                type: 'post',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {status: status, id:id},
                success:function(msg){
                    console.log(msg);
                    if(msg.status == true){
                        Swal.fire("Change!",  msg.message, "success");
                        window.LaravelDataTables["user-table"].ajax.reload();
                    }else{
                        Swal.fire("Warning!", msg.message, "warning");
                    }
                },
                error:function(){
                    Swal.fire("Error!", 'Error in delete Record', "error");
                }
                });
            }
        })
}

</script>
