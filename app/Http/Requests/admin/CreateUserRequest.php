<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        if ($this->type == "edit") {
            return [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users,email,'.$this->id.',id,deleted_at,NULL',
                'user_type' => 'required',
                'city_id' => 'required',
                'state_id' => 'required',
                'dob' => 'required',
                'gender' => 'required',
                'status' => 'required',
            ];
        } else {
            return [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => ['required',
                'email',
                    Rule::unique(table:'users')->whereNull(column:'deleted_at')],
                'user_type' => 'required',
                'city_id' => 'required',
                'state_id' => 'required',
                'dob' => 'required',
                'gender' => 'required',
                'status' => 'required',
            ];
        }
    }
}
