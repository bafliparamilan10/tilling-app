<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class AdminProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            // 'username' => 'required',
            'email' => "required|email|unique:admins,email,{$this->id},id,deleted_at,NULL",
            'logo.*' => 'nullable|image|mimes:jpg,jpeg,png,gif',
        ];
    }
}
