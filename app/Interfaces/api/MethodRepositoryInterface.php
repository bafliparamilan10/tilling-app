<?php

namespace App\Interfaces\api;

interface MethodRepositoryInterface
{
    public function methodFilterList(array $data);
}
