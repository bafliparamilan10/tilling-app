function errorHandler(data)
{
    console.log(data);
    if (data.status === 422) {
        var errors = $.parseJSON(data.responseText);
        $.each(errors.errors, function(key, value) {
            toastr.error(APPNAME, value)
        });
    }
    else if(data.status === 500){
        var errors = $.parseJSON(data.responseText);
        console.log(errors.message);
        toastr.error(APPNAME, 'Something went wrong. please contact to Administrator')
    }
}

$(function(){

        var url = window.location;
        $('ul.nav .nav-item a[href="'+ url +'"]').addClass('active');
        $('ul.nav .nav-item a').filter(function() {
             return this.href == url;
        }).addClass('active');

        $('ul.nav a[href="'+ url +'"]').parent().closest('.nav').parent().addClass('menu-open');
        $('ul.nav a').filter(function() {
             return this.href == url;
        }).closest('.nav').parent().addClass('menu-open');

        $('ul.nav a[href="'+ url +'"]').closest('.menu-open').children('.nav-link').addClass('active');
        $('ul.nav a').filter(function() {
             return this.href == url;
        }).closest('.menu-open').children('.nav-link').addClass('active');


        // Admin js

        var url = window.location;
        $('ul .nav-item a[href="'+ url +'"]').addClass('active');
        $('ul .nav-item a').filter(function() {
             return this.href == url;
        }).addClass('active');


        $('.slide-menu').on('click', function(e) {
            var adjustTable = $('.adjust-column').DataTable();
            setTimeout(function () {
                adjustTable.columns.adjust().draw();
            }, 400);
        });


});
