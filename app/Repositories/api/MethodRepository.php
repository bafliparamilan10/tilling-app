<?php

namespace App\Repositories\api;

use App\Interfaces\api\MethodRepositoryInterface;
use App\Models\MethodFilter;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;

class MethodRepository implements MethodRepositoryInterface
{
    public function model()
    {
        //return YourModel::class;
    }

    public function methodFilterList($data)
    {
        try {
            DB::beginTransaction();

            $filter_list = MethodFilter::whereNull('parent_id')->get();
            
            DB::commit();
            return response(['message' => 'Method filter list get successfully','data' => $filter_list] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Filter List : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }
}
