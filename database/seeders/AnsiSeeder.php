<?php

namespace Database\Seeders;

use App\Models\Ansi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('ansis')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Ansi::insert([
            [
                'name' => 'A118.1',
            ],
            [
                'name' => 'A118.3',
            ],
            [
                'name' => 'A118.4',
            ],
            [
                'name' => 'A118.5',
            ],
            [
                'name' => 'A118.11',
            ],
            [
                'name' => 'A118.15',
            ],
            [
                'name' => 'A136.1 Type 1',
            ],
            [
                'name' => 'A136.1 Type 2',
            ]
        ]);
    }
}
