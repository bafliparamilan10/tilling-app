<script>
    $(function () {
        $('#cmsPageForm').validate({
            rules: {
                title: {
                    required: true,
                },
                description: {
                    required: true,
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                description: {
                   required: "please enter description"
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function (form) {
                var form = $('#cmsPageForm')[0];
                var data = new FormData(form);
                submitForm(data);
            }
        });

        function submitForm(form) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                url: '{{ route('admin.cms-page-update') }}',
                type: 'post',
                processData: false,
                contentType: false,
                data: form,
                beforeSend: function() {
                    $(".data-loader").addClass('loading');
                },
                success: function(response) {
                    if (response.status == true) {
                        $(".data-loader").removeClass('loading');
                        toastr.success('{{ config('app.name') }}', response.message)
                        setTimeout(() => {
                            window.location.href = '{{ route('admin.cms-page-list') }}';
                        }, 1000);
                    } else {
                        $(".data-loader").removeClass('loading');
                        toastr.error('{{ config('app.name') }}', response.message)
                    }
                },
                error: function(data) {
                    $(".data-loader").removeClass('loading');
                    errorHandler(data);
                }
            });
        }
    });
</script>
