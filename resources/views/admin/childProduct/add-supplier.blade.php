<div class="row multiple-form-boarder mt-3 p-3 supplier-div">

    <div class="form-group col-md-6">
        <label for="email">Supplier Name</label>
        <select id="supplier_ids{{ $index }}"  name="supplier_ids[]" class="form-control allfield">
            <option value="">Select Supplier Name</option>
            @foreach ($suppliers as $suppliersValue)
                <option value="{{ $suppliersValue->id }}">{{ $suppliersValue->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="username">Supplier Product Link (url) </label>
        <input type="url" class="form-control allfield" id="product_link{{ $index }}" name="product_link[]" placeholder="Enter product link">
    </div>

    <div class="form-group col-md-6">
        <label for="username">Supplier Product MDL (#) </label>
        <input type="text" class="form-control allfield" id="product_mdl{{ $index }}" name="product_mdl[]" placeholder="Enter product MDL">
    </div>

    <div class="form-group col-md-6">
        <label for="">Product Package Type</label>
        <input type="text" class="form-control product_package_type_id_text" id="product_package_type_id_text{{ $index }}" name="product_package_type_id_text[]" readonly>
    </div>

    <div class="form-group col-md-6 d-flex added-d-flex product_size_width">
        <div class="w-100">
            <label for="">Product Size (width/ft.) Quantity</label>
            <select id="product_size_width_id{{ $index }}"  name="product_size_width_id[]" class="form-control product_size_width_id allfield" onchange="getProductPrice({{ $index }})">
            <option value="">Select Product Size (width/ft.) Quantity</option>
            @foreach ($productSizeWidthQty as $productSizeWidthQtyValue)
                <option value="{{ $productSizeWidthQtyValue->id }}">{{ $productSizeWidthQtyValue->size }}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('product-size-width-qty');"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div class="form-group col-md-6 d-flex added-d-flex product_size_length">
        <div class="w-100">
            <label for="">Product Size (length/ft.) Quantity</label>
            <select id="product_size_length_id{{ $index }}"  name="product_size_length_id[]" class="form-control product_size_length_id allfield" onchange="getProductPrice({{ $index }})">
            <option value="">Select Product Size (length/ft.) Quantity</option>
            @foreach ($productSizeLengthQty as $productSizeLengthQtyValue)
                <option value="{{ $productSizeLengthQtyValue->id }}">{{ $productSizeLengthQtyValue->size }}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('product-size-length-qty');"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div class="form-group col-md-6 d-flex added-d-flex product_size_lbs">
        <div class="w-100">
            <label for="">Product Size (# of lbs)</label>
            <select id="product_size_lbs_id{{ $index }}" name="product_size_lbs_id[]" class="form-control product_size_lbs_id allfield">
                <option value="">Select Product Size (# of lbs)</option>
                @foreach ($productSizeLbs as $size)
                    <option value="{{ $size->id }}">
                        {{ $size->size }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('product-size-lbs');">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="form-group col-md-6 d-flex added-d-flex product_size_gallon">
        <div class="w-100">
            <label for="">Product Size (# of gallons)</label>
            <select id="product_size_gallon_id{{ $index }}" name="product_size_gallon_id[]" class="form-control product_size_gallon_id allfield">
                <option value="">Select Product Size (# of gallons)</option>
                @foreach ($productSizeGallon as $size)
                    <option value="{{ $size->id }}">
                        {{ $size->size }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('product-size-gallons');">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="form-group col-md-6 d-flex added-d-flex product_size_coverage">
        <div class="w-100">
            <label for="">Product Size (coverage) Quantity</label>
            <select id="product_size_coverage_id{{ $index }}" name="product_size_coverage_id[]" class="form-control product_size_coverage_id allfield" onchange="getProductPrice({{ $index }})">
                <option value="">Select Product Size (coverage) Quantity</option>
                @foreach ($productSizeCoverageQty as $size)
                    <option value="{{ $size->id }}">
                        {{ $size->size }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('product-size-coverage');">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="form-group col-md-6 d-flex added-d-flex product_overage">
        <div class="w-100">
            <label for="">Add an overage % (waste)</label>
            <select id="product_overage_id{{ $index }}" name="product_overage_id[]" class="form-control product_overage_id allfield">
                <option value="">Select overage % (waste) </option>
                @foreach ($productOverage as $overage)
                    <option value="{{ $overage->id }}">
                        {{ $overage->percentage }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('product-overage');">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="form-group col-md-6 d-flex added-d-flex product_color">
        <div class="w-100">
            <label for="">Product Color</label>
            <select id="product_color_id{{ $index }}" name="product_color_id[]" class="form-control product_color_id allfield">
                <option value="">Select Product Color </option>
                @foreach ($product_color as $color)
                    <option value="{{ $color->id }}">
                        {{ $color->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('product-color');">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="col-12 mb-3 mt-3">
        <hr>
        <h4 class="w-100">Specifications (product) :</h4>
        <hr>
    </div>

    <div class="form-group col-md-6 d-flex added-d-flex thin_set_ansis">
        <div class="w-100">
            <label class="mb-3">Spec. - Thin set's ANSIs</label>
            @foreach ($ansi as $key => $value)
                <br>
                <input type="checkbox" name="thin_set_ansis[{{ $ansi_index }}][]" id="thin_set_ansis_{{ $key }}" value="{{ $value['id'] }}">
                <label for="thin_set_ansis_{{ $key }}" class="ml-1">
                    {{ $value['name'] }}
                </label>
            @endforeach
        </div>
    </div>

    <div class="form-group col-md-6 d-flex added-d-flex thin_set_qty_types">
        <div class="w-100">
            <label class="mb-3">Spec. - Thin set's Quality Types</label>
            @foreach ($thin_set_qty_types as $key => $value)
                <br>
                <input type="checkbox" name="thin_set_qty_types[{{ $ansi_index }}][]" id="thin_set_qty_types" value="{{ $value['id'] }}">
                <label for="thin_set_qty_types" class="ml-1">
                    {{ $value['name'] }}
                </label>
            @endforeach
        </div>
    </div>

    <div class="row trowel_field_wrapper{{ $index }} w-100 trowel_hide_show">
        <div class="col-6 trowel mb-3">
            <div class="form-group col-md-12 d-flex added-d-flex">
                <div class="w-100">
                    <div>
                        <label class="mb-3 float-left">Trowel Type</label>
                        <label class="float-right text-success text-bold">
                            <input type="radio" name="trowel_type_default_{{ $index }}[]" class="trowel_type_default" value="1"> Default
                        </label>
                    </div>

                    <select id="trowel_type" name="trowel_type_{{ $index }}[]" class="form-control trowel_type allfield">
                        <option value="">Select Trowel Type </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                    </select>
                </div>
            </div>

            <div class="form-group col-md-12 d-flex added-d-flex">
                <div class="w-100">
                    <label class="mb-3">Trowel Size</label>
                    <select id="trowel_size" name="trowel_size_{{ $index }}[]" class="form-control trowel_size allfield">
                        <option value="">Select Trowel Size </option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                </div>
            </div>

            <div class="form-group col-md-12 d-flex added-d-flex">
                <div class="w-100">
                    <label class="mb-3">Coverage (sq. ft. - quantity)</label>
                    <select id="coverage" name="coverage_{{ $index }}[]" class="form-control coverage allfield">
                        <option value="">Select Coverage </option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                </div>
            </div>
        </div>

        {{-- <div class="col-6 d-flex added-d-flex align-items-end">
            <div class="form-group">
                <button type="button" class="btn btn-primary" onclick="addTrowel(1);">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div> --}}
    </div>

    <div class="row w-100 trowel_hide_show">
        <div class="col-7 trowel-btn">
            <div class="form-group">
                <button type="button" class="btn btn-primary" onclick="addTrowel({{ $index }});">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
    </div>

    <div class="row w-100">

        <div class="col-12">
        </div>

        <div class="form-group col-md-6">
            <label for="username">Product Price ($) </label>
            <input type="number" class="form-control allfield product_price1" id="product_price{{ $index }}" name="product_price[]" placeholder="Enter product price" min="0" onkeyup="getProductPrice({{ $index }});">
        </div>

        <div class="form-group col-md-6">
            <label for="username">Product Price (Per Sq. Ft. in $) </label>
            <input type="number" class="form-control product_price_per_sq" id="product_price_per_sq{{ $index }}" name="product_price_per_sq[]" placeholder="Enter product price" min="0" readonly>
        </div>

        <div class="form-group col-md-6">
            <label for="">Status</label>
            <select id="status{{ $index }}"  name="status[]" class="form-control allfield">
            <option value="">Select Status</option>
            <option value="1">Active</option>
            <option value="0">InActive</option>
            </select>
        </div>
    </div>

    {{-- <a class="btn btn-app remove_button remove-mutiple float-right" ><i class="fas fa-minus"></i></a></div> --}}
</div>

