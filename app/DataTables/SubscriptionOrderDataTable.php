<?php

namespace App\DataTables;

use App\Models\PurchasedSubsciptionPlan;
use App\Models\SubscriptionOrder;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SubscriptionOrderDataTable extends DataTable
{
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
        ->addIndexColumn()
        ->addColumn('action', function ($data) {
            $action = "";
            $action .= '<a href="'.route('admin.subscription-order-view', ['id' => Crypt::encrypt($data->id)]).'" class="btn btn-outline-primary mr-1"><i class="fa fa-eye" title="view"></i></a>';
            return $action;
        })
        ->editColumn('first_name', function (PurchasedSubsciptionPlan $data) {
            return $data->getUserDetails->first_name;
        })       
        ->editColumn('last_name', function (PurchasedSubsciptionPlan $data) {
            return $data->getUserDetails->last_name;
        })       
        ->editColumn('email', function (PurchasedSubsciptionPlan $data) {
            return $data->getUserDetails->email;
        })       
        ->editColumn('plan_name', function (PurchasedSubsciptionPlan $data) {
            return $data->getPlanDetails->name;
        })
        ->rawColumns(['action']);
    }

    public function query(PurchasedSubsciptionPlan $model): QueryBuilder
    {
        return $model->with(['getPlanDetails','getUserDetails'])->newQuery();
    }

    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('subscription-order-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->orderBy(1)
                    ->buttons([]);
    }

    public function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')->title('Sr No.')->searchable(false)->orderable(false),
            Column::make('id')->searchable(false)->exportable(true)->hidden(true),
            Column::make('first_name')->title('First Name'),
            Column::make('last_name')->title('Last Name'),
            Column::make('email')->title('Email'),
            Column::make('plan_name')->title('Subscription Name'),
            Column::make('price')->title('Subscription Price'),
            Column::computed('action')->title('Actions')->exportable(false)->printable(false),
        ];
    }

    protected function filename(): string
    {
        return 'SubscriptionOrder_' . date('YmdHis');
    }
}
