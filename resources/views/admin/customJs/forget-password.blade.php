<script>
    $(function () {
        $.validator.setDefaults({
        submitHandler: function () {
            // alert( "Form successful submitted!" );
        }
    });

    $('#forgetForm').validate({
        rules: {
        email: {
            required: true,
            email: true,
        }
        },
        messages: {
        email: {
            required: "Please enter a email address",
            email: "Please enter a valid email address"
        }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        },
        submitHandler: function () {
            var form = $('#forgetForm')[0];
            var data = new FormData(form);
            forgetForm(data);
        }
    });
    });
    function forgetForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.forget-mail-send') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
               $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    setTimeout(() => {
                        window.location.href = '{{ route('admin.login') }}';
                    }, 2000);
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    };
</script>