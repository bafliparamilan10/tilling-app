@extends('admin.layouts.master')
@push('customCSS')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush
@section('page_title', 'Add Supplier')
@section('content_header')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Add Supplier</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.supplier-list')}}">Supplier Management</a></li>
                    <li class="breadcrumb-item active">Add Supplier</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-11 mx-auto">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"> Add Supplier Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="supplierForm" name="supplierForm" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <img id="preview-image-before-upload" src="{{ '/no_image.jpg' }}"
                                         style="height: 100px; width:150px;">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="username">Image / Logo (optional)</label>
                                    <input type="file" class="form-control" id="logo" name="logo" accept="image/png, image/jpeg, image/jpg">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="username">Supplier </label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter supplier name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Manufacturers (optional)</label>
                                    <select class="select2bs4" id="manufacture_id"  name="manufacture_id" style="width: 100%;">
                                        <option value="">Select Manufacturer</option>
                                        @foreach ($manufacturer as $manufacturerValue)
                                        <option value="{{ $manufacturerValue->id }}">{{ $manufacturerValue->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="website">Location URL (optional)</label>
                                    <input type="text" class="form-control" id="location_url" name="location_url" placeholder="Enter location url">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="status">Status</label>
                                    <select class="select2bs4" id="status"  name="status" style="width: 100%;">
                                        <option value="">Select Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">InActive</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <!-- /.card-body -->
                        <div class="card-footer text-center">
                            <a href="javascript:history.back()" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('customJs')
<!-- jquery-validation -->
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>

<script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
@include('admin.customJs.supplier')
@endpush
