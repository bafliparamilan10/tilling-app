<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductPackageTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('product_package_types')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $productPackageTypes = array(

            array('name' => 'panel/s'),
            array('name' => 'box/es'),
            array('name' => 'gal/s'),
            array('name' => 'roll/s'),
            array('name' => 'roll/s or sheet/s'),
        );

        DB::table('product_package_types')->insert($productPackageTypes);
    }
}
