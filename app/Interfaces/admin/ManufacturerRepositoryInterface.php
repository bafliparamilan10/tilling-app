<?php

namespace App\Interfaces\admin;

interface ManufacturerRepositoryInterface
{
    public function createManufacturer(array $data);
    public function deleteManufacturer($id);
    public function manufacturerEnableDisable(array $data);
}
