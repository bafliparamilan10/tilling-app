<script>
    $(function () {
        $.validator.setDefaults({
            submitHandler: function () {
                // alert( "Form successful submitted!" );
            }
        });

        $('#profile_picture').change(function(){

            let reader = new FileReader();

            reader.onload = (e) => {

              $('#preview-image-before-upload').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);

        });

        $.validator.addMethod("pwcheck", function(value) {
            return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(value)
        });

        $('#userForm').validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name : {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                }

            },
            messages: {
                first_name: {
                    required: "Please enter first name"
                },
                last_name: {
                    required: "Please enter last name"
                },
                email: {
                    required: "Please enter a email address",
                    email: "Please enter a valid email address"
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function () {
                var form = $('#userForm')[0];
                    var data = new FormData(form);
                    updateProfleForm(data);
            }
        });

        $('#changepasswordForm').validate({
            rules: {
                oldpassword: {
                    required: true
                },
                newpassword: {
                    required: true,
                    minlength: 6,
                    maxlength: 12,
                    pwcheck:true
                },
                confirmpassword : {
                    required: true,
                    minlength : 6,
                    maxlength: 12,
                    equalTo : "#newpassword"
                }

            },
            messages: {
                oldpassword: {
                    required: "Please enter a Old password"
                },
                newpassword: {
                    required: "Please enter a password",
                    minlength: "Your password must be at least 6 characters long",
                    pwcheck : "Password must contain at least one capital alphabet, one special character (@, #, $, _, etc.)0 to 9 number and a to z alphabets."
                },
                confirmpassword: {
                    required: "Please enter a confirm password",
                    minlength: "Your password must be at least 6 characters long",
                    equalTo: "Password does not match"
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function () {
                var form = $('#changepasswordForm')[0];
                    var data = new FormData(form);
                    changeForm(data);
            }
        });
    });

    function updateProfleForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.update-profile') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    function changeForm(form) {

        if($("#oldpassword").val() == $("#newpassword").val())
        {
            toastr.error('{{ config('app.name') }}', 'Do not use the same password as before')
            return false;
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.change-password') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('#changepasswordForm')[0].reset();
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }
</script>
