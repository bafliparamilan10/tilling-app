<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];
    protected $appends = ['subsciption_plan_name'];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */

    public function getStateDetails()
    {
        return $this->belongsTo(State::class, 'state_id', 'id');
    }
    public function getCityDetails()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
    public function getPurchasedPlanDetails()
    {
        return $this->hasOne(PurchasedSubsciptionPlan::class, 'user_id', 'id')->orderBy('id', 'DESC');
    }

    public function getSubsciptionPlanNameAttribute()
    {
        $purchase_plan =  PurchasedSubsciptionPlan::where('user_id',$this->id)
                            ->where('status','active')
                            ->first();
               
        if(!empty($purchase_plan)){
            $plan_name = SubsciptionPlan::where('id',$purchase_plan->subscription_plan_id)->value('name');
        }else{
            $plan_name = '';
        }
        
        return($plan_name);
    }
}
