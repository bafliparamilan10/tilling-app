<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;

class SupplierFavUnfavRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'supplier_ids' => 'required',
            'is_favorite' => 'required'
        ];
    }
}
