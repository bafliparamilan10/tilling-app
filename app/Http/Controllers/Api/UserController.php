<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\UserChangePasswordRequest;
use App\Http\Requests\api\UserCityListRequest;
use App\Http\Requests\api\UserForgotPasswordRequest;
use App\Http\Requests\api\UserNotificationRequest;
use App\Http\Requests\api\UserProfileUpdateRequest;
use App\Http\Requests\api\UserResetPasswordRequest;
use App\Http\Requests\api\UserSignupRequest;
use App\Interfaces\api\UserRepositoryInterface;
use Illuminate\Http\Request;
use Laravel\Passport\HasApiTokens;

class UserController extends Controller
{    
    use HasApiTokens;

    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function stateList(Request $request)
    {
        $response = $this->userRepository->stateList($request->all());
        
        return $response;
    }

    public function cityList(UserCityListRequest $request)
    {
        $response = $this->userRepository->cityList($request->all());
        
        return $response;
    }
    
    public function signUp(UserSignupRequest $request)
    {
        $response = $this->userRepository->signUp($request->all());
        
        return $response;
    }

    public function emailExists(Request $request)
    {
        $response = $this->userRepository->emailExists($request->all());
        
        return $response;
    }

    public function login(Request $request)
    {
        $response = $this->userRepository->login($request->all());
        
        return $response;
    }

    public function forgotPassword(UserForgotPasswordRequest $request)
    {
        $response = $this->userRepository->forgotPassword($request->all());
        
        return $response;
    }

    public function resetPassword(UserResetPasswordRequest $request)
    {
        $response = $this->userRepository->resetPassword($request->all());
        
        return $response;
    }

    public function logout(Request $request)
    {
        $response = $this->userRepository->logout($request->all());
        
        return $response;
    }

    public function notificationUpdate(UserNotificationRequest $request)
    {
        $response = $this->userRepository->notificationUpdate($request->all());
        
        return $response;
    }

    public function changePassword(UserChangePasswordRequest $request)
    {
        $response = $this->userRepository->changePassword($request->all());

        return $response;
    }

    public function profileGet(Request $request)
    {
        $response = $this->userRepository->profileGet($request->all());
        
        return $response;
    }

    public function profileUpdate(UserProfileUpdateRequest $request)
    {
        $response = $this->userRepository->profileUpdate($request->all());
        
        return $response;
    }

    public function deleteAccount(Request $request)
    {
        $response = $this->userRepository->deleteAccount($request->all());
        
        return $response;
    }

    
}
