<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParentProductSupplier extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function getSupplierDetails(){
        return $this->belongsTo(Suppliers::class, 'supplier_id', 'id');
    }

    public function getProductSizeWidthQtyData(){
        return $this->belongsTo(ProductSizeWidthQty::class, 'product_size_width_qty_id', 'id');
    }

    public function getProductSizeLengthQtyData(){
        return $this->belongsTo(ProductSizeLengthQty::class, 'product_size_length_qty_id', 'id');
    }

    public function getProductSizeThicknessQtyData(){
        return $this->belongsTo(ProductSizeThickness::class, 'product_size_thickness_id', 'id');
    }

    public function getSpecificationFastnersData(){
        return $this->belongsTo(SpecificationFastners::class, 'specification_fastners_id', 'id');
    }

    public function getSpecificationFastnersScrewData(){
        return $this->belongsTo(SpecificationFastnersScrew::class, 'specification_fastners_screw_id', 'id');
    }

    public function getSpecificationFastnersNailsData(){
        return $this->belongsTo(SpecificationFastnersNail::class, 'specification_fastners_nails_id', 'id');
    }

}
