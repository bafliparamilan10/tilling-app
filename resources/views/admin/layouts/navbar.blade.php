<nav class="main-header navbar navbar-expand navbar-dark navbar-dark-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav ">
        <li class="nav-item">
            <a class="nav-link slide-menu" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->

        <li class="nav-item ">
            <!-- <a class="nav-link" data-toggle="dropdown" href="#"> -->
            <div class=" dropdown nav-link d-flex" data-toggle="dropdown" href="#">
                    <div class="image mr-2">
                        <img src="{{ auth()->user()->profile_picture ? auth()->user()->profile_picture : asset('admin/dist/img/mycpp.png') }}" class="img-circle elevation-2" width="20px" height="20px" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block text-white">{{ auth()->user()->first_name.' '.auth()->user()->last_name }}</a>
                    </div>
                </div>
            </a>
            <div class="dropdown-menu  dropdown-menu-right">
                <a href="{{ route('admin.user-edit') }}"><span class="dropdown-item">Profile</span></a>
                <div class="dropdown-divider"></div>
                <a href="{{route('admin.logout')}}"><span class="dropdown-item">Logout</span></a>
                {{-- <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form> --}}
            </div>
        </li>
        <!-- <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li> -->
        <!-- <li class="nav-item dropdown">
            <a class="nav-link" data-widget="control-sidebar" data-controlsidebar-slide="true" href="#" role="button">
                <i class="fas fa-th-large"></i>
                </a>
        </li> -->
    </ul>
</nav>
