<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MethodFilter extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = ['data'];

    public function getDataAttribute()
    {
        return MethodFilter::where('parent_id',$this->id)->get();
    }
}
