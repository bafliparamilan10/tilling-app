<?php

namespace App\Repositories\api;

use App\Interfaces\api\SubscriptionRepositoryInterface;
use App\Models\PurchasedSubsciptionPlan;
use App\Models\SubsciptionPlan;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;
use PDF;
use Str;


class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    public function model()
    {
        //return YourModel::class;
    }

    public function subscriptionList($data)
    {
        try {
            DB::beginTransaction();

            $subscription_list = SubsciptionPlan::get();

            DB::commit();
            return response(['message' => 'Subscription list get successfully','data' => $subscription_list] ,200);
        }
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Subscription List : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function subscriptionPurchase($data)
    {
        try {
            DB::beginTransaction();

            $user_id = Auth::user()->id;
            $subscription_plan = SubsciptionPlan::where('id',$data['subscription_plan_id'])->first();

            $plan_already_purchased = PurchasedSubsciptionPlan::where('user_id',$user_id)->where('status','active')->first();

            if($plan_already_purchased){
                return response(['message' => 'Subscription alredy purchased', 'data' => new stdClass()] ,422);
            }

            if($subscription_plan->type == 'free')
            {
                $purchased_subscription_plans = new PurchasedSubsciptionPlan();
                $purchased_subscription_plans->user_id = $user_id;
                $purchased_subscription_plans->subscription_plan_id = $data['subscription_plan_id'];
                $purchased_subscription_plans->transaction_id = $data['transaction_id'];
                $purchased_subscription_plans->price = $subscription_plan->price;
                $purchased_subscription_plans->status = 'active';
                $purchased_subscription_plans->start_date = date("Y-m-d");
                $purchased_subscription_plans->end_date = date('Y-m-d', strtotime('+365 days'));

                $pdf_data = [
                    'user_name' => Auth::user()->first_name.''.Auth::user()->last_name,
                    'plan' => 'FREE',
                    'price' => $subscription_plan->price,
                    'start_date' => date("Y-m-d"),
                    'end_date' => date('Y-m-d', strtotime('+365 days')),
                ];

                $pdf_name = Str::random(10).'.pdf';
                PDF::loadView('email.api_subscription_receipt', $pdf_data)->save(public_path('subscription/'.$pdf_name));

                $purchased_subscription_plans->pdf = $pdf_name;
                $purchased_subscription_plans->save();

                DB::commit();
                return response(['message' => 'Subscription purchase successfully','data' => $purchased_subscription_plans] ,200);
            }
            else{

                $purchased = PurchasedSubsciptionPlan::where('user_id',$user_id)->where('subscription_plan_id',2);

                if($purchased->exists()){
                    $end_date = date('Y-m-d', strtotime('+30 days'));
                }else{
                    $end_date = date('Y-m-d', strtotime('+14 days'));
                }

                $purchased_subscription_plans = new PurchasedSubsciptionPlan();
                $purchased_subscription_plans->user_id = $user_id;
                $purchased_subscription_plans->subscription_plan_id = $data['subscription_plan_id'];
                $purchased_subscription_plans->transaction_id = $data['transaction_id'];
                $purchased_subscription_plans->price = $subscription_plan->price;
                $purchased_subscription_plans->status = 'active';
                $purchased_subscription_plans->start_date = date("Y-m-d");
                $purchased_subscription_plans->end_date = $end_date;

                $pdf_data = [
                    'user_name' => Auth::user()->first_name.''.Auth::user()->last_name,
                    'plan' => 'PRO × 10',
                    'price' => $subscription_plan->price,
                    'start_date' => date("Y-m-d"),
                    'end_date' => $end_date,
                ];

                $pdf_name = Str::random(10).'.pdf';
                PDF::loadView('email.api_subscription_receipt', $pdf_data)->save(public_path('subscription/'.$pdf_name));

                $purchased_subscription_plans->pdf = $pdf_name;
                $purchased_subscription_plans->save();

                DB::commit();
                return response(['message' => 'Subscription purchase successfully','data' => $purchased_subscription_plans] ,200);
            }
        }
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Subscription Purchase : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function subscriptionCancel($data)
    {
        try {
            DB::beginTransaction();

            $user_id = Auth::user()->id;
            $plan_already_purchased = PurchasedSubsciptionPlan::where('user_id',$user_id)->first();

            if(!$plan_already_purchased){
                return response(['message' => "You don't have any active subscription plan", 'data' => new stdClass()] ,422);
            }

            $plan_already_purchased->delete();

            DB::commit();
            return response(['message' => 'Subscription cancel successfully','data' => new stdClass()] ,200);
        }
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Subscription Cancel : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function subscriptionReceipts($data)
    {
        try {
            DB::beginTransaction();

            $user_id = Auth::user()->id;
            // $subscription_receipts = PurchasedSubsciptionPlan::where('user_id',$user_id)->get();

            // $array = [];
            // foreach ($subscription_receipts as $key => $receipt) {
            //     $date = date('Y',strtotime($receipt['start_date']));
            //     array_push($array,$date);
            // }

            // $years = array_unique($array);

            $years = PurchasedSubsciptionPlan::where('user_id',$user_id)->selectRaw('YEAR(start_date) as year')->distinct()->pluck('year');
            $newReceipts = [];
            foreach ($years as $key => $year) {
                $newReceipts[$key]['year'] = $year;
                $newReceipts[$key]['yearData'] = PurchasedSubsciptionPlan::where('user_id',$user_id)->whereYear('start_date', $year)->get();;
                // $receipts[$key][$year] = PurchasedSubsciptionPlan::whereYear('start_date', $year)->get();
            }

            DB::commit();
            return response(['message' => 'Subscription receipts get successfully','data' => $newReceipts] ,200);
        }
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Subscription Receipts : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }
}
