<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class ParentProduct extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $appends = [
        'data_sheet',
    ];

    protected function logo(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value ? asset('parent_product/'.$value) : asset('/no_image.jpg'),
        );
    }

    public function getSupplierData()
    {
        return $this->hasMany(ParentProductSupplier::class, 'parent_product_id', 'id');
    }

    public function getManufactureData()
    {
        return $this->belongsTo(Manufacturer::class, 'manufacture_id', 'id');
    }

    public function getAnsiData()
    {
        return $this->belongsTo(Ansi::class, 'ansi_id', 'id');
    }

    public function getParentProdcutPackageTypeData()
    {
        return $this->belongsTo(ProductPackageType::class, 'product_package_type_id', 'id');
    }

    public function getProductTypeDetails()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id', 'id');
    }

    public function getProductTypeMainSubChildDetails()
    {
        return $this->belongsTo(ProductType::class, 'product_type_main_sub_child_id', 'id');
    }
    public function getProductTypeSecondarySubChildDetails()
    {
        return $this->belongsTo(ProductType::class, 'product_type_secondary_sub_child_id', 'id');
    }

    public function getDataSheetAttribute()
    {
        $ids = explode(',',$this->data_sheet_ids);
        return DataSheet::whereIn('id',$ids)->pluck('id','link')->toArray();
    }
}
