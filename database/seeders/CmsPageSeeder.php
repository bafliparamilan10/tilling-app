<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CmsPageSeeder extends Seeder
{
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('cms_pages')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $cms_pages = [
            ['page_name' => 'about_us','title' => 'About Us','description' => 'About Us Description'],
            ['page_name' => 'privacy_policy','title' => 'Privacy Policy','description' => ' Privacy Policy Description'],
            ['page_name' => 'terms_of_use','title' => 'Terms of Use','description' => 'Terms of Use Description'],
        ];

        DB::table('cms_pages')->insert($cms_pages);
    }
}
