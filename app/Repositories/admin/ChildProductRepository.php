<?php

namespace App\Repositories\admin;

use App\Interfaces\admin\ChildProductRepositoryInterface;
use App\Models\ChildProduct;
use App\Models\ChildProductSupplier;
use App\Models\Overage;
use App\Models\ProdctSizeCoverageQty;
use App\Models\ProdctSizeGallon;
use App\Models\ProductColor;
use App\Models\ProductSizeLbs;
use App\Models\Trowel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ChildProductRepository implements ChildProductRepositoryInterface
{
    public function model()
    {
        //return YourModel::class;
    }

    public function storeChildProduct($data)
    {
        try {

            dd($data['trowel_type_default_1']);
            DB::beginTransaction();

            $message = 'Child product added successfully';
            $is_add = 1;
            $childProduct = new ChildProduct();

            if (isset($data['id']) && !empty($data['id'])) {
                $childProduct = ChildProduct::find($data['id']);
                $message = 'Child product update successfully';
                $is_add = 0;

                // delete old supplier
                // if($childProduct->product_type_id != $data['product_type']){
                //     ChildProductSupplier::where('child_product_id',$childProduct->id)->delete();
                // }
            }

            if (isset($data['logo'])) {
                $logo = $data['logo'];
                $extension = $logo->getClientOriginalExtension();
                $logoName = $logo->getFilename();
                $logo->move(public_path('/child_product'), $logoName.'.'.$extension);
                $childProduct->logo = $logo->getFilename().'.'.$extension;
            }

            $childProduct->manufacture_id  = isset($data['manufacture_id']) ? $data['manufacture_id'] : $childProduct->manufacture_id;
            $childProduct->name            = isset($data['name']) ? $data['name'] : $childProduct->name;
            $childProduct->product_area    = isset($data['product_area']) ? $data['product_area'] : $childProduct->product_area;
            $childProduct->ansi_id         = isset($data['ansi_id']) ? $data['ansi_id'] : $childProduct->ansi_id;
            $childProduct->data_sheet_id   = isset($data['data_sheet_ids']) ? $data['data_sheet_ids'][0] : $childProduct->data_sheet_id;
            $childProduct->data_sheet_ids  = isset($data['data_sheet_ids']) ? implode(",",$data['data_sheet_ids']) : $childProduct->data_sheet_ids;
            $childProduct->product_type_id  = isset($data['product_type']) ? $data['product_type'] : $childProduct->product_type;
            $childProduct->product_type_main_sub_child_id = isset($data['product_type_main_sub_child_id']) ? $data['product_type_main_sub_child_id'] : $childProduct->product_type_main_sub_child_id;
            $childProduct->product_type_secondary_sub_child_id  = isset($data['product_type_secondary_sub_child_id']) ? $data['product_type_secondary_sub_child_id'] : $childProduct->product_type_secondary_sub_child_id;
            $childProduct->product_package_type_id = isset($data['get_product_package_type_id']) ? $data['get_product_package_type_id'] : $childProduct->get_product_package_type_id;
            $childProduct->save();

            $child_product_id = $childProduct->id;
            $productLink = $data['product_link'];
            $insertData = array();

            foreach($productLink as $key => $productLinkValue)
            {
                $newKey = $key+1;
                if(!empty($productLinkValue))
                {
                    $childProductSupplierId = isset($data['child_product_supplier_id'][$key]) ? $data['child_product_supplier_id'][$key] : NULL;

                    $newId = ChildProductSupplier::updateOrCreate(
                        [
                            'id' => $childProductSupplierId,
                            'child_product_id' => $child_product_id,
                        ],
                        [
                            'supplier_id' => $data['supplier_ids'][$key],
                            'product_links' => $productLinkValue,
                            'product_mdl' => $data['product_mdl'][$key],
                            'product_size_width_qty_id' => $data['product_size_width_id'][$key],
                            'product_size_length_qty_id' => $data['product_size_length_id'][$key],
                            'product_size_lbs_id' => $data['product_size_lbs_id'][$key],
                            'product_color_id' => $data['product_color_id'][$key],
                            'prodct_size_gallon_id' => $data['product_size_gallon_id'][$key],
                            'prodct_size_coverage_qtie_id' => $data['product_size_coverage_id'][$key],
                            'overage_id' => $data['product_overage_id'][$key],
                            'ansi_ids' => isset($data['thin_set_ansis'][$key]) ? implode(',',$data['thin_set_ansis'][$key]) : '',
                            'thin_set_qty_type_ids' => isset($data['thin_set_qty_types'][$key]) ? implode(',',$data['thin_set_qty_types'][$key]) : '',
                            'product_price' => $data['product_price'][$key],
                            'product_price_per_sq' => $data['product_price_per_sq'][$key],
                            'status' => $data['status'][$key],
                        ]
                    );

                    $child_product_supplier_id = $newId->id;

                    $trowelType = $data['trowel_type_'.$newKey];

                    foreach ($trowelType as $trowelTypeKey => $trowelTypeValue) {

                        $trowel = new Trowel();

                        if (isset($data['trowel_id_'.$newKey][$trowelTypeKey]) && !empty($data['trowel_id_'.$newKey][$trowelTypeKey])) {
                            $trowel = Trowel::find($data['trowel_id_'.$newKey][$trowelTypeKey]);
                        }

                        $trowel->child_product_id = $child_product_id;
                        $trowel->child_product_supplier_id = $child_product_supplier_id;
                        $trowel->is_default = isset($data['trowel_type_default_'.$newKey][$trowelTypeKey]) ? 1 : 0;
                        $trowel->trowel_type = isset($data['trowel_type_'.$newKey][$trowelTypeKey]) ? $data['trowel_type_'.$newKey][$trowelTypeKey] : '';
                        $trowel->trowel_size = isset($data['trowel_size_'.$newKey][$trowelTypeKey]) ? $data['trowel_size_'.$newKey][$trowelTypeKey] : '';
                        $trowel->coverage = isset($data['coverage_'.$newKey][$trowelTypeKey]) ? $data['coverage_'.$newKey][$trowelTypeKey] : '';
                        $trowel->save();

                    }

                }
            }

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('child product store : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function deleteChildProduct($id)
    {
        try {
            DB::beginTransaction();

            ChildProduct::find($id)->delete();

            DB::commit();
            return ['status' => true, 'message' =>  'Child product delete successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('child product delete : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function enableDisableChildProduct($data)
    {
        try {
            DB::beginTransaction();

            $status = $data['status'] ? 0 : 1;
            ChildProduct::where('id', $data['id'])->update(['status' => $status]);

            DB::commit();
            return ['status' => true, 'message' =>  'Child product status updated successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('child product enable-disable : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function createColorChildProduct($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Product color added successfully';
            $is_add = 1;
            $ProductColor = new ProductColor();

            if (isset($data['id']) && !empty($data['id'])) {
                $ProductColor = ProductColor::find($data['id']);
                $message = 'Product color update successfully';
                $is_add = 0;
            }

            $ProductColor->name = isset($data['child_product_color']) ? $data['child_product_color'] : $ProductColor->name;
            $ProductColor->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $ProductColor->id, 'name' => $ProductColor->name]];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('create color child product : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function createProductSizeLbs($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Product size (lbs) added successfully';
            $is_add = 1;
            $ProductSize = new ProductSizeLbs();

            if (isset($data['id']) && !empty($data['id'])) {
                $ProductSize = ProductSizeLbs::find($data['id']);
                $message = 'Product size (lbs) update successfully';
                $is_add = 0;
            }

            $ProductSize->size = isset($data['product_size_lbs']) ? $data['product_size_lbs'] : $ProductSize->size;
            $ProductSize->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $ProductSize->id, 'name' => $ProductSize->size]];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('create child product size lbs : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function createProductSizeGallon($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Product size (gallon) added successfully';
            $is_add = 1;
            $ProductSize = new ProdctSizeGallon();

            if (isset($data['id']) && !empty($data['id'])) {
                $ProductSize = ProdctSizeGallon::find($data['id']);
                $message = 'Product size (gallon) update successfully';
                $is_add = 0;
            }

            $ProductSize->size = isset($data['product_size_gallon']) ? $data['product_size_gallon'] : $ProductSize->size;
            $ProductSize->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $ProductSize->id, 'name' => $ProductSize->size]];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('create product size gallon: ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function createProductSizeCoverage($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Product size coverage added successfully';
            $is_add = 1;
            $ProductSize = new ProdctSizeCoverageQty();

            if (isset($data['id']) && !empty($data['id'])) {
                $ProductSize = ProdctSizeCoverageQty::find($data['id']);
                $message = 'Product size coverage qty update successfully';
                $is_add = 0;
            }

            $ProductSize->size = isset($data['product_size_coverage']) ? $data['product_size_coverage'] : $ProductSize->size;
            $ProductSize->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $ProductSize->id, 'name' => $ProductSize->size]];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('create product size of coverage : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function createProductOverage($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Product overage added successfully';
            $is_add = 1;
            $ProductSize = new Overage();

            if (isset($data['id']) && !empty($data['id'])) {
                $ProductSize = Overage::find($data['id']);
                $message = 'Product overage update successfully';
                $is_add = 0;
            }

            $ProductSize->percentage = isset($data['child_product_overage']) ? $data['child_product_overage'] : $ProductSize->percentage;
            $ProductSize->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $ProductSize->id, 'name' => $ProductSize->percentage]];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('create product overage : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function updateDefaultDataSheet($data)
    {
        try {
            DB::beginTransaction();

            ChildProduct::where('id', $data['id'])->update(['data_sheet_id' => $data['data_sheet_id']]);

            DB::commit();
            return ['status' => true, 'message' =>  'Default datasheet updated successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('Default datasheet : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }
}
