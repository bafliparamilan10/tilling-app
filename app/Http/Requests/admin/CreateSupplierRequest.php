<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'logo' => 'nullable|image|mimes:jpg,jpeg,png,gif',
            'name'=> "required|unique:suppliers,name,{$this->id},id,deleted_at,NULL",
            'status' => "required",
            'location_url' => 'nullable|url'
        ];
    }
}
