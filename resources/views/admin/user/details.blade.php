@extends('admin.layouts.master')
@push('customCSS')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush
@section('page_title', 'View User')
@section('content_header')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                {{-- <h1 class="m-0">View User</h1> --}}
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.user-list')}}">User Management</a></li>
                    <li class="breadcrumb-item active">User Edit</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="col-md-11 mx-auto">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header text-center">
                    <h3 class="card-title float-none"> View User </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="card-body box-profile">
                            <ul class="list-group list-group-unbordered mb-3">
                              <li class="list-group-item">
                                <b>First Name : </b> <span class="m-3">{{ $userData->first_name }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Email Address : </b> <span class="m-3">{{$userData->email }}</span>
                              </li>
                              {{-- <li class="list-group-item">
                                <b>Features : </b> <span class="m-3"></span>
                              </li> --}}
                              <li class="list-group-item">
                                <b>City : </b> <span class="m-3">{{ $userData->getCityDetails->name }}</span>
                              </li>
                              <li class="list-group-item">
                                  <b>Date Of Birth : </b> <span class="m-3">{{ date('M d, Y', strtotime($userData->date_of_birth) )  }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Registration Date & Time : </b> <span class="m-3">{{ date('M d, Y', strtotime($userData->created_at) ) }} | {{ date('h:i A', strtotime($userData->created_at) ) }}</span>
                              </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-body box-profile">
                            <ul class="list-group list-group-unbordered mb-3">
                              <li class="list-group-item">
                                <b>Last Name : </b> <span class="m-3">{{ $userData->last_name }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>User Type : </b> <span class="m-3">{{ $userData->user_type }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>State : </b> <span class="m-3">{{ $userData->getStateDetails->name }}</span>
                              </li>
                              <li class="list-group-item">
                                  <b>Gender : </b> <span class="m-3">{{ Str::ucfirst($userData->gender) }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Status : </b>
                                @if($userData->status)
                                <span class="m-3 text-success">Active</span>
                                @else
                                <span class="m-3 text-danger">Inactive</span>
                                @endif
                              </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center">
                    <h3 class="card-title float-none"> Subscription Plan Details </h3>
                </div>
                @if(isset($userData->getPurchasedPlanDetails))
                <div class="row">
                    <div class="col-md-6">
                        <div class="card-body box-profile">
                            <ul class="list-group list-group-unbordered mb-3">
                              <li class="list-group-item">
                                <b>Subscription Plan : </b> <span class="m-3">{{ $userData->getPurchasedPlanDetails ? ($userData->getPurchasedPlanDetails->getPlanDetails ? Str::ucfirst($userData->getPurchasedPlanDetails->getPlanDetails->type) : '-') : '-' }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Price : </b> <span class="m-3">${{$userData->getPurchasedPlanDetails->price}}</span>
                              </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-body box-profile">
                            <ul class="list-group list-group-unbordered mb-3">
                              <li class="list-group-item">
                                <b>Date & Time : </b> <span class="m-3">{{ date('M d, Y', strtotime($userData->getPurchasedPlanDetails->start_date) ) }} | {{ date('h:i A', strtotime($userData->getPurchasedPlanDetails->start_date) ) }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Expiration : </b> <span class="m-3">{{ date('M d, Y', strtotime($userData->getPurchasedPlanDetails->end_date) )  }}</span>
                              </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @else
                <div class="text-center">
                    <h4>Subscription plan has been not purchased</h4>
                </div>
                @endif
            </div>
            <div class="mt-5 text-center">
                <a href="javascript:history.back()" class="btn btn-secondary">Back</a>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
@push('customJs')
<!-- jquery-validation -->
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
@include('admin.customJs.user')
@endpush
