@extends('admin.layouts.master')
@push('customCSS')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush
@section('page_title', 'Parent Product Management')
@section('content_header')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Parent Product Management</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.parent-product-list')}}">Parent Product Management</a></li>
                    <li class="breadcrumb-item active">Parent Product Details</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="col-md-11 mx-auto">
            <!-- general form elements -->
            <div class="card card-primary">

                <div class="card-header">
                    <h3 class="card-title"> View Parent Product</h3>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-body box-profile">
                            <input type="hidden" id="id" name="id" value="{{ $parentProduct->id }}">
                            <ul class="list-group list-group-unbordered mb-3">
                              <li class="list-group-item">
                                <b>Image / Logo : </b> <span class="float-right"> <img src="{{ $parentProduct->logo }}"
                                    style="height: 100px; width:150px;"></span>
                              </li>
                              <li class="list-group-item">
                                <b>Product Name : </b> <span class="float-right">{{$parentProduct->name }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Product Manufacturer : </b> <span class="float-right">{{$parentProduct->getManufactureData->name }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Product Area : </b> <span class="float-right">{{$parentProduct->product_area }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Product ANSI : </b> <span class="float-right">{{$parentProduct->getAnsiData->name }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Product Type : </b> 
                                <span class="float-right">
                                    {{ isset($parentProduct->getProductTypeDetails->name) ? $parentProduct->getProductTypeDetails->name : '' }} / {{ isset($parentProduct->getProductTypeMainSubChildDetails->name) ? $parentProduct->getProductTypeMainSubChildDetails->name : "" }} / {{ isset($parentProduct->getProductTypeSecondarySubChildDetails->name) ? $parentProduct->getProductTypeSecondarySubChildDetails->name : "" }}
                                </span>
                              </li>
                              <li class="list-group-item">
                                <b>Data Sheet (url) : </b>
                                <span class="float-right">
                                    @foreach ($parentProduct->data_sheet as $key => $dataSheetValue)
                                    <input type="radio" name="dataSheetId"  value="{{ $dataSheetValue }}" @checked($dataSheetValue == $parentProduct->data_sheet_id)>
                                    <a href="{{ $dataSheetValue }}">{{ $key }}</a>
                                    <br>
                                    @endforeach
                                </span>
                              </li>
                              <li class="list-group-item">

                                <b>Supplier Name : </b>

                                <select class="form-control getSupplier float-right" style="width: 40%">
                                    @foreach ($parentProduct->getSupplierData as $getSupplierDataValue)
                                    <option value="{{ $getSupplierDataValue->id }}">{{ $getSupplierDataValue->getSupplierDetails->name }}</option>
                                    @endforeach
                                </select>

                              </li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card-header">
                    <h3 class="card-title"> Other Details</h3>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-body box-profile">
                            <ul class="list-group list-group-unbordered mb-3">

                                <li class="list-group-item">
                                <b>Supplier Product Link (url) : </b> <span class="float-right product_links">{{$parentProduct->name }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Supplier Product MDL # : </b> <span class="float-right product_mdl">{{$parentProduct->getManufactureData->name }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Product Package Type : </b> <span class="float-right">{{isset($parentProduct->getParentProdcutPackageTypeData->name) ?? '' }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Product Size (width/ft.) Quantity : </b> <span class="float-right product_size_width_qty">{{$parentProduct->getAnsiData->name }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Product Size (length/ft.) Quantity  : </b> <span class="float-right get_product_size_length_qty_data">{{isset($parentProduct->getProductTypeDetails->name) ?? "" .' / '.$parentProduct->getProductTypeMainSubChildDetails->name.' / '.$parentProduct->getProductTypeSecondarySubChildDetails->name }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Product Size (thickness/inch.) : </b> <span class="float-right get_product_size_thickness_qty_data">{{$parentProduct->name }}</span>
                              </li>

                            </ul>
                            </br>
                            <h4>Specifications (product)</h4>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Spec.- # of Fastners (per sq. ft.) req. : </b> <span class="float-right get_specification_fastners_data">{{$parentProduct->name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Spec.- Fastners screw (length/inch.) req. : </b> <span class="float-right get_specification_fastners_screw_data">{{$parentProduct->name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Fastners nail (length/inch.) req. : </b> <span class="float-right get_specification_fastners_nails_data">{{$parentProduct->name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Product Price ($) : </b> <span class="float-right product_price">{{$parentProduct->name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Product Price (Per Sq. Ft. in $) : </b> <span class="float-right product_price_per_sq">{{$parentProduct->name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Status : </b> <span class="float-right status">{{ $parentProduct->name }}</span>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>

            </div>
            <div class="mt-5 text-center">
                <a href="javascript:history.back()" class="btn btn-secondary">Back</a>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
@push('customJs')
<!-- jquery-validation -->
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script>

    getPerticularSupplierDetails();

    $(".getSupplier").change(function(e){
        e.preventDefault();

        getPerticularSupplierDetails();

        });

        function getPerticularSupplierDetails(){

            var id = $(".getSupplier").val();

            $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    url: '{{ route('admin.get-supplier-view-data') }}',
                    type: 'post',
                    data: {id: id},
                    beforeSend: function() {
                        $(".data-loader").addClass('loading');
                    },
                    success: function(response) {
                        console.log(response);
                        $(".product_links").text(response.product_links);
                        $(".product_mdl").text(response.product_mdl);
                        $(".product_size_width_qty").text(response.get_product_size_width_qty_data.size);
                        $(".get_product_size_length_qty_data").text(response.get_product_size_length_qty_data.size);
                        $(".get_product_size_thickness_qty_data").text(response.get_product_size_thickness_qty_data.size);
                        $(".get_specification_fastners_data").text(response.get_specification_fastners_data.squer_foot);
                        $(".get_specification_fastners_screw_data").text(response.get_specification_fastners_screw_data.length_inch);
                        $(".get_specification_fastners_nails_data").text(response.get_specification_fastners_nails_data.length_inch);
                        $(".product_price").text(response.product_price);
                        $(".product_price_per_sq").text(response.product_price_per_sq);
                        var status = response.status ? '<span class="text-success">Active</span>' : '<span class="text-danger">DeActive</span>';
                        $(".status").html(status);
                        $(".data-loader").removeClass('loading');
                    },
                    error: function(data) {
                        $(".data-loader").removeClass('loading');
                        errorHandler(data);
                    }
            });

        }

        $('input[type=radio][name=dataSheetId]').change(function() {

            var id = $("#id").val();
            var data_sheet_id = $("input[name='dataSheetId']:checked").val();

            $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    url: '{{ route('admin.update-default-data-sheet') }}',
                    type: 'post',
                    data: {id: id, data_sheet_id: data_sheet_id},
                    beforeSend: function() {
                        // $(".data-loader").addClass('loading');
                    },
                    success: function(response) {
                        console.log(response);
                    },
                    error: function(data) {
                        // $(".data-loader").removeClass('loading');
                        errorHandler(data);
                    }
            });

        });
</script>
@endpush
