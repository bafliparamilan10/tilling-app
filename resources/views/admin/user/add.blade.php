@extends('admin.layouts.master')
@push('customCSS')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush
@section('page_title', 'Add User')
@section('content_header')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Add User</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.user-list')}}">User Management</a></li>
                    <li class="breadcrumb-item active">Add User</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-11 mx-auto">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"> Add User Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="userForm" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="type" value="create">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="username">First Name </label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="username">Last Name </label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email Address</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email Address">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">User Type</label>
                                    <select class="form-control" id="user_type"  name="user_type" style="width: 100%;" >
                                        <option value="Tile Professional">Tile Professional</option>
                                        <option value="Tile DIYer" >Tile DIYer</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="email">State</label>
                                    <select class="select2bs4" id="state_id"  name="state_id" style="width: 100%;" onchange="getCity(this);">
                                        <option value="">Select State</option>
                                        @if(count($state) > 0)
                                            @foreach ($state as $s)
                                             <option value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="email">City</label>
                                    <select class="select2bs4" id="city_id"  name="city_id" style="width: 100%;">
                                        <option value="">Select City</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="username">Date of Birth</label>
                                    <div class="input-group date" id="dob" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" id="doba"  name="dob"
                                            data-target="#dob" onblur="" data-toggle="datetimepicker"/>
                                        <div class="input-group-append" data-target="#dob" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Gender</label>
                                    <select class="form-control" id="gender"  name="gender" style="width: 100%;" >
                                        <option value="male">Male</option>
                                        <option value="female" >Female</option>
                                        <option value="non_binary" >Non-binary</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Status</label>
                                    <select class="form-control" id="status"  name="status" style="width: 100%;" >
                                        <option value="1">Active</option>
                                        <option value="0" >Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- /.card-body -->
                        <div class="card-footer text-center">
                            <a href="javascript:history.back()" class="btn btn-secondary">Cancel</a>
                            <button type="submit" id='usersubmit' class="btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('customJs')
<!-- jquery-validation -->
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>

<script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
@include('admin.customJs.user')
@endpush
