<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\api\MethodRepositoryInterface;
use Illuminate\Http\Request;
use Laravel\Passport\HasApiTokens;

class MethodController extends Controller
{
    use HasApiTokens;

    private MethodRepositoryInterface $MethodRepository;

    public function __construct(MethodRepositoryInterface $MethodRepository)
    {
        $this->MethodRepository = $MethodRepository;
    }
    
    public function methodFilterList(Request $request)
    {
        $response = $this->MethodRepository->methodFilterList($request->all());
        
        return $response;
    }
}
