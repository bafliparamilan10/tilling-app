<?php

namespace App\Http\Controllers\admin;

use App\DataTables\ParentProductDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\DataSheetRequest;
use App\Http\Requests\admin\ProductSizeLengthQtyRequest;
use App\Http\Requests\admin\ProductSizeThicknessQtyRequest;
use App\Http\Requests\admin\ProductSizeWidthQtyRequest;
use App\Http\Requests\admin\SpecFastenerNailRequest;
use App\Http\Requests\admin\SpecFastenerRequest;
use App\Http\Requests\admin\SpecFastenerScrewRequest;
use App\Interfaces\admin\ParentProductRepositoryInterface;
use App\Models\Ansi;
use App\Models\DataSheet;
use App\Models\Manufacturer;
use App\Models\ParentProduct;
use App\Models\ParentProductSupplier;
use App\Models\ProductPackageType;
use App\Models\ProductSizeLengthQty;
use App\Models\ProductSizeThickness;
use App\Models\ProductSizeWidthQty;
use App\Models\ProductType;
use App\Models\SpecificationFastners;
use App\Models\SpecificationFastnersNail;
use App\Models\SpecificationFastnersScrew;
use App\Models\Suppliers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;

class ParentProductController extends Controller
{
    private ParentProductRepositoryInterface $parentProductRepositoryInterface;

    public function __construct(ParentProductRepositoryInterface $parentProductRepositoryInterface)
    {
        $this->parentProductRepositoryInterface = $parentProductRepositoryInterface;
    }

    public function list(ParentProductDataTable $dataTable)
    {
        // $redis = Redis::connection();
        // $redis->set('name', 'Martin');
        // $name = $redis->get('name');

        return $dataTable->render('admin.parentProduct.list');
    }

    public function addParentProduct()
    {
        $manufacturer = Manufacturer::where('status',1)->get();
        $ansi = Ansi::where('status',1)->get();
        $dataSheets = DataSheet::where('status',1)->get();
        $suppliers = Suppliers::where('status',1)->get();
        $productPackageType = ProductPackageType::where('status',1)->get();
        $productType = ProductType::where(['status' => 1, 'is_parent' => 1])->whereNull('parent_id')->get();
        $productSizeWidthQty = ProductSizeWidthQty::all();
        $productSizeLengthQty = ProductSizeLengthQty::all();
        $productSizeThickness = ProductSizeThickness::all();
        $specificationFasteners = SpecificationFastners::all();
        $specificationFastenersScrew = SpecificationFastnersScrew::all();
        $specificationFastenersNail = SpecificationFastnersNail::all();

        return view('admin.parentProduct.add',compact('manufacturer', 'ansi', 'dataSheets', 'suppliers', 'productPackageType', 'productType', 'productSizeWidthQty', 'productSizeLengthQty', 'productSizeThickness', 'specificationFasteners', 'specificationFastenersScrew', 'specificationFastenersNail'));
    }

    public function parentProductEdit($id)
    {
        $id = Crypt::decrypt($id);
        $manufacturer = Manufacturer::where('status',1)->get();
        $ansi = Ansi::where('status',1)->get();
        $dataSheets = DataSheet::where('status',1)->get();
        $suppliers = Suppliers::where('status',1)->get();
        $productPackageType = ProductPackageType::where('status',1)->get();
        $productType = ProductType::where(['status' => 1, 'is_parent' => 1])->whereNull('parent_id')->get();
        $productSizeWidthQty = ProductSizeWidthQty::all();
        $productSizeLengthQty = ProductSizeLengthQty::all();
        $productSizeThickness = ProductSizeThickness::all();
        $specificationFasteners = SpecificationFastners::all();
        $specificationFastenersScrew = SpecificationFastnersScrew::all();
        $specificationFastenersNail = SpecificationFastnersNail::all();
        $parentProduct = ParentProduct::with(['getSupplierData','getParentProdcutPackageTypeData','getProductTypeDetails','getProductTypeMainSubChildDetails','getProductTypeSecondarySubChildDetails'])->where('id', $id)->first();

        return view('admin.parentProduct.edit',compact('manufacturer', 'ansi', 'dataSheets', 'suppliers', 'productPackageType', 'productType', 'productSizeWidthQty', 'productSizeLengthQty', 'productSizeThickness', 'specificationFasteners', 'specificationFastenersScrew', 'specificationFastenersNail', 'parentProduct'));
    }

    public function addDataSheet(DataSheetRequest $request)
    {
        $data =  $this->parentProductRepositoryInterface->addDataSheet($request->all());
        return $data;
    }

    public function addProductSizeWidthQty(ProductSizeWidthQtyRequest $request)
    {
        $data =  $this->parentProductRepositoryInterface->addProductSizeWidthQty($request->all());
        return $data;
    }

    public function addProductSizeLengthQty(ProductSizeLengthQtyRequest $request)
    {
        $data =  $this->parentProductRepositoryInterface->addProductSizeLengthQty($request->all());
        return $data;
    }

    public function addProductSizeThicknessQty(ProductSizeThicknessQtyRequest $request)
    {
        $data =  $this->parentProductRepositoryInterface->addProductSizeThicknessQty($request->all());
        return $data;
    }

    public function addSpecFastener(SpecFastenerRequest $request)
    {
        $data =  $this->parentProductRepositoryInterface->addSpecFastener($request->all());
        return $data;
    }

    public function addSpecFastenerScrew(SpecFastenerScrewRequest $request)
    {
        $data =  $this->parentProductRepositoryInterface->addSpecFastenerScrew($request->all());
        return $data;
    }

    public function addSpecFastenerNail(SpecFastenerNailRequest $request)
    {
        $data =  $this->parentProductRepositoryInterface->addSpecFastenerNail($request->all());
        return $data;
    }

    public function parentProductTypeCreate(Request $request)
    {
        $data =  $this->parentProductRepositoryInterface->parentProductTypeCreate($request->all());
        return $data;
    }

    public function parentProductPackageTypeCreate(Request $request)
    {
        $data =  $this->parentProductRepositoryInterface->parentProductPackageTypeCreate($request->all());
        return $data;
    }

    public function mainSubParentProductTypeCreate(Request $request)
    {
        $data =  $this->parentProductRepositoryInterface->mainSubParentProductTypeCreate($request->all());
        return $data;
    }

    public function secondarySubParentProductTypeCreate(Request $request)
    {
        $data =  $this->parentProductRepositoryInterface->secondarySubParentProductTypeCreate($request->all());
        return $data;
    }

    public function parentProductCreate(Request $request)
    {
        $data =  $this->parentProductRepositoryInterface->parentProductCreate($request->all());
        return $data;
    }

    public function getProductTypeData(Request $request)
    {
        return ProductType::with('getMainSubChildData')->where('id',$request->id)->first();
    }

    public function addParentProductSupplier(Request $request)
    {
        $index = $request->index;
        $suppliers = Suppliers::where('status',1)->get();
        $productPackageType = ProductPackageType::where('status',1)->get();
        $productSizeWidthQty = ProductSizeWidthQty::all();
        $productSizeLengthQty = ProductSizeLengthQty::all();
        $productSizeThickness = ProductSizeThickness::all();
        $specificationFasteners = SpecificationFastners::all();
        $specificationFastenersScrew = SpecificationFastnersScrew::all();
        $specificationFastenersNail = SpecificationFastnersNail::all();

        return view('admin.parentProduct.add-supplier',compact('suppliers', 'productPackageType', 'productSizeWidthQty', 'productSizeLengthQty', 'productSizeThickness', 'specificationFasteners', 'specificationFastenersScrew', 'specificationFastenersNail', 'index'));
    }

    public function getSupplierData(Request $request)
    {
        return ParentProductSupplier::where('id',$request->supplier_id)->first();
    }

    public function parentProductEnableDisable(Request $request)
    {
        $response = $this->parentProductRepositoryInterface->parentProductEnableDisable($request->all());
        return  $response;
    }

    public function deleteParentProduct(Request $request)
    {
        $data =  $this->parentProductRepositoryInterface->deleteParentProduct($request['id']);
        return $data;
    }

    public function parentProductView($id)
    {
        $id = Crypt::decrypt($id);
        $parentProduct = ParentProduct::with('getAnsiData','getProductTypeDetails','getProductTypeMainSubChildDetails','getProductTypeSecondarySubChildDetails','getSupplierData','getSupplierData.getProductSizeWidthQtyData','getSupplierData.getProductSizeLengthQtyData','getSupplierData.getProductSizeThicknessQtyData','getSupplierData.getSpecificationFastnersData','getSupplierData.getSpecificationFastnersScrewData','getSupplierData.getSpecificationFastnersNailsData')->where(['id' => $id])->orderBy('id', 'DESC')->first();
        return view('admin.parentProduct.view',compact('parentProduct'));
    }

    public function getSupplierViewData(Request $request)
    {
        return ParentProductSupplier::with('getProductSizeWidthQtyData','getProductSizeLengthQtyData','getProductSizeThicknessQtyData','getSpecificationFastnersData','getSpecificationFastnersScrewData','getSpecificationFastnersNailsData')->where('id',$request->id)->first();
    }

    public function updateDefaultDataSheet(Request $request)
    {
        $data =  $this->parentProductRepositoryInterface->updateDefaultDataSheet($request->all());
        return $data;
    }
}
