<?php

namespace App\Repositories\admin;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Interfaces\admin\ManufacturerRepositoryInterface;
use App\Models\Manufacturer;
use App\Models\Suppliers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

//use Your Model

/**
 * Class MenufacturerRepository.
 */
class MenufacturerRepository implements ManufacturerRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        //return YourModel::class;
    }
    public function createManufacturer($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Manufacturer added successfully';
            $is_add = 1;
            $manufacturerData = new Manufacturer();
            if (isset($data['id']) && !empty($data['id'])) {
                $manufacturerData = Manufacturer::find($data['id']);
                $message = 'Manufacturer update successfully';
                $is_add = 0;
            }

            if (isset($data['logo'])) {
                $logo = $data['logo'];
                $extension = $logo->getClientOriginalExtension();
                $logoName = $logo->getFilename();
                $logo->move(public_path('/manufacturer'), $logoName.'.'.$extension);
                $manufacturerData->logo = $logo->getFilename().'.'.$extension;
            }

            $manufacturerData->name         = isset($data['name']) ? $data['name'] : $manufacturerData->name;
            $manufacturerData->ticker         = isset($data['ticker']) ? $data['ticker'] : $manufacturerData->ticker;
            $manufacturerData->website         = isset($data['website']) ? $data['website'] : $manufacturerData->website;
            $manufacturerData->suppliers_ids         = isset($data['suppliers_ids']) ? implode(",",$data['suppliers_ids']) : $manufacturerData->suppliers_ids;
            $manufacturerData->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Manufacturer Store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function deleteManufacturer($id)
    {
        try {
            DB::beginTransaction();

            Suppliers::where('manufacture_id',$id)->delete();
            Manufacturer::find($id)->delete();

            DB::commit();
            return ['status' => true, 'message' =>  'Manufacturer delete successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error(' Manufacturer Delete : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function manufacturerEnableDisable($data)
    {
        try {
            DB::beginTransaction();

            $status = $data['status'] ? 0 : 1;
            Manufacturer::where('id', $data['id'])->update(['status' => $status]);

            DB::commit();
            return ['status' => true, 'message' =>  'Manufacturer status updated successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('Manufacturer enable disable : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }
}
