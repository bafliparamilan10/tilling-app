<?php

namespace App\Interfaces\api;

interface UserRepositoryInterface
{
    public function stateList(array $data);
    public function cityList(array $data);
    public function signUp(array $data);
    public function emailExists(array $data);
    public function login(array $data);
    public function logout(array $data);
    public function notificationUpdate(array $data);
    public function forgotPassword(array $data);
    public function resetPassword(array $data);
    public function changePassword(array $data);
    public function profileGet(array $data);
    public function profileUpdate(array $data);
    public function deleteAccount(array $data);
}
