<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;


class Manufacturer extends Model
{
    use HasFactory,SoftDeletes;

    protected $guarded = [];

    protected function logo(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value ? asset('manufacturer/'.$value) : asset('/no_image.jpg'),
        );
    }
}
