<script>
    $(function () {
        $.validator.setDefaults({
        submitHandler: function () {

            var form = $('#loginForm')[0];
            loginForm(form);
        }
    });
    $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
        input.attr("type", "text");
        } else {
        input.attr("type", "password");
        }
    });

    $.validator.addMethod("pwcheck", function(value) {
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(value)
    });

    $('#loginForm').validate({
        rules: {
        email: {
            required: true,
            email: true,
        },
        password: {
            required: true,
            minlength: 6,
            maxlength: 18,
            },
        },
        messages: {
            
            email: {
                required: "Please enter a email address",
                email: "Please enter a valid email address"
            },
            password: {
                required: "Please enter a password",
                minlength: "Your password must be at least 6 characters long",
                maxlength: "Your password max length is 18 characters long",
                pwcheck : "Password must contain at least one capital alphabet, one special character (@, #, $, _, etc.)0 to 9 number and a to z alphabets."
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        }
    });
    });

    function loginForm(form) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        url: '{{ route('admin.custom-login') }}',
        type: 'post',
        processData: false,
        contentType: false,
        data: new FormData(form),
        beforeSend: function() {
            $(".data-loader").addClass('loading');
        },
        success: function(response) {
            console.log(response);

            $(".data-loader").removeClass('loading');
            if (response.status == true) {
                toastr.success('{{ config('app.name') }}', response.message)
                window.location.href = '{{ route('admin.dashboard') }}';
            } else {
                $(".data-loader").removeClass('loading');
                toastr.error('{{ config('app.name') }}', response.message)
            }

        },
        error: function(data) {
            $(".data-loader").removeClass('loading');
            errorHandler(data);
        }
    });
}
</script>