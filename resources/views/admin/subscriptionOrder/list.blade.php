@extends('admin.layouts.master')
@section('page_title', 'Subscription Orders')
@section('content_header')

@push('customCSS')
<link  href="{{ asset('admin/dist/css/datatables/jquery.dataTables.min.css') }}" rel="stylesheet">
<link  href="{{ asset('admin/dist/css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link  href="{{ asset('admin/dist/css/datatables/buttons.dataTables.min.css') }}" rel="stylesheet">
<link  href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}" rel="stylesheet">
@endpush

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Subscription Orders</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Subscription Orders</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Subscription Orders</h3>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  {{$dataTable->table(['class' => 'table table-bordered table-striped ', 'id' => 'subscription-order-table'], true)}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</section>
@endsection

@push('scripts')
  <script src="{{ asset('admin/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('admin/dist/js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('admin/dist/js/datatables/bootstrap.min.js') }}"></script>
  <script src="{{ asset('admin/dist/js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('admin/dist/js/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables/buttons.server-side.js')}}"></script>

  {{$dataTable->scripts()}}
@endpush

@push('customJs')
  <script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
  {{-- @include('admin.customJs.supplier') --}}
@endpush
