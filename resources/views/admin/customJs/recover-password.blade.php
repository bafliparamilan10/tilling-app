<script>
    $(function () {
        $.validator.setDefaults({
        submitHandler: function () {
            // alert( "Form successful submitted!" );
        }
    });
    
    $.validator.addMethod("pwcheck", function(value) {
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(value)
    });
    
    $('#recoverForm').validate({
        rules: {
            password: {
            required: true,
            minlength: 6,
            maxlength: 12,
            pwcheck:true
            },
            confirm_password : {
                required: true,
                minlength : 6,
                maxlength: 12,
                equalTo : "#password"
            }
    
        },
        messages: {
            password: {
            required: "Please enter a password",
            minlength: "Your password must be at least 6 characters long",
            pwcheck : "Password must contain at least one capital alphabet, one special character (@, #, $, _, etc.)0 to 9 number and a to z alphabets."
            },
            confirm_password: {
            required: "Please enter a confirm password",
            minlength: "Your password must be at least 6 characters long",
            equalTo: "Password does not match"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function () {
            var form = $('#recoverForm')[0];
            var data = new FormData(form);
            recoverForm(data);
        }
    });
});

function recoverForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.recover-password-store') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    setTimeout(() => {
                        window.location.href = '{{ route('admin.login') }}';
                    }, 2000);
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    };
</script>