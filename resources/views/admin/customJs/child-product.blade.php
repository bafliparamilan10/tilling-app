<script>
    $('.select2').select2();
    $('.select2bs4').select2({
        theme: 'bootstrap4',
        placeholder: "Select Data Sheet ( URL )",
    });

    function managePopup(popupId) {
        $("#" + popupId).modal('show');
    }

    function productTypeManage(val, type) {
        if (type == 'secondary_sub_child') {
            var id = val.value;
        } else {
            var id = $("#product_type").val();
        }

        if($("#product_type").val() == '')
        {
            $(".main-sub-parent-product-type-btn").attr('disabled',true)
            $(".secondary-sub-parent-product-type-btn").attr('disabled',true)
        }
        else{
            $(".main-sub-parent-product-type-btn").removeAttr('disabled')
            $(".secondary-sub-parent-product-type-btn").removeAttr('disabled')
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.get-product-type-data') }}',
            type: 'post',
            data: {
                id: id
            },
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {

                $(".data-loader").removeClass('loading');

                $("#product_package_type_id option[value='" + response.product_package_type_id + "']").attr(
                    "selected", "selected");


                if (type == 'secondary_sub_child') {
                    $('.secondary_sub_child').children().remove().end();
                    $('.secondary_sub_child').append('<option value="">Select sub parent product type</option>');
                    response.get_main_sub_child_data.forEach(element => {
                        $('.secondary_sub_child').append('<option value="' + element.id + '">' +
                            element.name + '</option>');
                    });
                } else {
                    $('.main_sub_child').children().remove().end();
                    $('.secondary_sub_child').children().remove().end();
                    $('.main_sub_child').append(
                        '<option value="">Select Main Sub Parent Product Type </option>');

                    $(".product-type-name").val(response.name);

                    if(!response.name){
                        $(".product-type-name").removeAttr("readonly");
                    }
                    else{
                        $(".product-type-name").attr("readonly", true);
                    }

                    response.get_main_sub_child_data.forEach(element => {
                        $('.main_sub_child').append('<option value="' + element.id + '">' + element
                            .name + '</option>');
                    });
                }

                console.log(response);
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#logo').change(function() {

        let reader = new FileReader();

        reader.onload = (e) => {

            $('#preview-image-before-upload').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);

    });

    $('#productColorForm').validate({
        rules: {
            child_product_color: {
                required: true,
            }
        },
        messages: {
            child_product_color: {
                required: "Please enter product color"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productColorForm')[0];
            var data = new FormData(form);
            productColorForm(data);
        }
    });

    function productColorForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.child-product-color-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.product_color_id').append('<option value="' + response.data.id + '">' + response.data.name + '</option>');
                    $("#productColorForm")[0].reset();
                    $("#product-color").modal('hide');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#productSizeLbsForm').validate({
        rules: {
            product_size_lbs: {
                required: true,
                number : true
            }
        },
        messages: {
            product_size_lbs: {
                required: "Please enter product size of lbs"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productSizeLbsForm')[0];
            var data = new FormData(form);
            productSizeLbsForm(data);
        }
    });

    function productSizeLbsForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.child-product-size-lbs-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.product_size_lbs_id').append('<option value="' + response.data.id + '">' + response.data.name + '</option>');
                    $("#productSizeLbsForm")[0].reset();
                    $("#product-size-lbs").modal('hide');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#productSizeGallonForm').validate({
        rules: {
            product_size_gallon: {
                required: true,
                number : true
            }
        },
        messages: {
            product_size_gallon: {
                required: "Please enter product size of gallon"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productSizeGallonForm')[0];
            var data = new FormData(form);
            productSizeGallonForm(data);
        }
    });

    function productSizeGallonForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.child-product-size-gallon-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.product_size_gallon_id').append('<option value="' + response.data.id + '">' + response.data.name + '</option>');
                    $("#productSizeGallonForm")[0].reset();
                    $("#product-size-gallons").modal('hide');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#productSizeCoverageForm').validate({
        rules: {
            product_size_coverage: {
                required: true,
                number : true
            }
        },
        messages: {
            product_size_coverage: {
                required: "Please enter product size of coverage"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productSizeCoverageForm')[0];
            var data = new FormData(form);
            productSizeCoverageForm(data);
        }
    });

    function productSizeCoverageForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.child-product-size-coverage-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.product_size_coverage_id').append('<option value="' + response.data.id + '">' + response.data.name + '</option>');
                    $("#productSizeCoverageForm")[0].reset();
                    $("#product-size-coverage").modal('hide');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#productOverageForm').validate({
        rules: {
            child_product_overage: {
                required: true,
                number : true
            }
        },
        messages: {
            child_product_overage: {
                required: "Please enter product overage"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productOverageForm')[0];
            var data = new FormData(form);
            productOverageForm(data);
        }
    });

    function productOverageForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.child-product-overage-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.product_overage_id').append('<option value="' + response.data.id + '">' + response.data.name + '</option>');
                    $("#productOverageForm")[0].reset();
                    $("#product-overage").modal('hide');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#dataSheetForm').validate({
        rules: {
            link: {
                required: true,
            }
        },
        messages: {
            link: {
                required: "Please enter data sheet link"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#dataSheetForm')[0];
            var data = new FormData(form);
            dataSheetForm(data);
        }
    });

    function dataSheetForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.data-sheet-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    // getDataSheetData();
                    $('#data_sheet_ids').append('<option value="' + response.data.id + '">' + response.data
                        .link + '</option>');
                    $("#dataSheetForm")[0].reset();
                    $("#data-sheet").modal('hide');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#productSizeWidthQtyForm').validate({
        rules: {
            size: {
                required: true,
            }
        },
        messages: {
            size: {
                required: "Please enter size"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productSizeWidthQtyForm')[0];
            var data = new FormData(form);
            productSizeWidthQtyForm(data);
        }
    });

    function productSizeWidthQtyForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.product-size-width-qty-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.product_size_width_id').append('<option value="' + response.data.id + '">' +
                        response.data.size + '</option>');
                    $("#product-size-width-qty").modal('hide');
                    $(".size").val('');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#productSizeLengthQtyForm').validate({
        rules: {
            size: {
                required: true,
            }
        },
        messages: {
            size: {
                required: "Please enter size"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productSizeLengthQtyForm')[0];
            var data = new FormData(form);
            productSizeLengthQtyForm(data);
        }
    });

    function productSizeLengthQtyForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.product-size-length-qty-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.product_size_length_id').append('<option value="' + response.data.id + '">' +
                        response.data.size + '</option>');
                    $("#product-size-length-qty").modal('hide');
                    $(".size").val('');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#productTypeForm').validate({
        rules: {
            product_type_name: {
                required: true,
            },
            product_package_type_id: {
                required: true,
            },
        },
        messages: {
            product_type_name: {
                required: "Please enter product type name "
            },
            product_package_type_id: {
                required: "Please select package product type "
            },

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productTypeForm')[0];
            var data = new FormData(form);
            productTypeForm(data);
        }
    });

    function productTypeForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.parent-product-type-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {

                    $("#product_type_main_sub_child_id").val($("#main_sub_child").val());
                    $("#product_type_secondary_sub_child_id").val($("#secondary_sub_child").val());
                    $(".product_package_type_id_text").val($("#product_package_type_id option:selected").text());
                    $("#get_product_package_type_text").val($("#product_package_type_id option:selected").text());
                    $("#get_product_package_type_id").val($("#product_package_type_id").val());

                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)

                    if($('#product_type_name').attr('readonly') != 'readonly'){
                        $('#product_type').append('<option value="' + response.data.id +'">' + response.data.product_type_name + '</option>');
                    }

                    let disData = response.data.product_type_name;
                    if(response.data.main_sub_child_name){
                        disData += ' / '+response.data.main_sub_child_name;
                    }

                    if(response.data.secondary_sub_child_name){
                        disData += ' / '+response.data.secondary_sub_child_name;
                    }

                    $(".dis-product-type").html('<div class="callout callout-success">'+disData+'</div>');
                    $("#product-type").modal('hide');
                    $("#productTypeForm")[0].reset();
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#productPackageTypeForm').validate({
        rules: {
            parent_product_package_type: {
                required: true,
            }
        },
        messages: {
            parent_product_package_type: {
                required: "Please enter parent product type name "
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productPackageTypeForm')[0];
            var data = new FormData(form);
            productPackageTypeForm(data);
        }
    });

    function productPackageTypeForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.parent-product-package-type-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {

                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)

                    $('#product_package_type_id').append('<option value="' + response.data.id +
                        '">' + response.data.parent_product_package_type + '</option>');

                    $("#product-package-type").modal('hide');
                    $("#productPackageTypeForm")[0].reset();
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#mainSubParentProductTypeForm').validate({
        rules: {
            main_sub_parent_product_type: {
                required: true,
            }
        },
        messages: {
            main_sub_parent_product_type: {
                required: "Please enter main sub parent product type "
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#mainSubParentProductTypeForm')[0];
            var data = new FormData(form);
            mainSubParentProductTypeForm(data);
        }
    });

    function mainSubParentProductTypeForm(form) {

        var product_type = $("#product_type").val();
        form.append('parent_id',product_type);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.main-sub-parent-product-type-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {

                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)

                    $('#main_sub_child').append('<option value="' + response.data.id +
                        '">' + response.data.main_sub_parent_product_type + '</option>');

                    $("#main-sub-parent-product-type").modal('hide');
                    $("#productTypeForm")[0].reset();
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#secondarySubParentProductTypeForm').validate({
        rules: {
            secondary_sub_parent_product_type: {
                required: true,
            }
        },
        messages: {
            secondary_sub_parent_product_type: {
                required: "Please enter secondary sub parent product type "
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#secondarySubParentProductTypeForm')[0];
            var data = new FormData(form);
            secondarySubParentProductTypeForm(data);
        }
    });

    function secondarySubParentProductTypeForm(form) {

        var main_sub_child = $("#main_sub_child").val();
        if(main_sub_child == '')
        {
            toastr.error('{{ config('app.name') }}', 'Please select first main sub parent product type');
            return false;
        }

        form.append('main_sub_child',main_sub_child);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.secondary-sub-parent-product-type-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {

                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)

                    $('#secondary_sub_child').append('<option value="' + response.data.id +
                        '">' + response.data.secondary_sub_parent_product_type + '</option>');

                    $("#secondary-sub-parent-product-type").modal('hide');
                    $("#secondarySubParentProductTypeForm")[0].reset();
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    function getProductPrice(index){

        var product_type = $("#product_type").val();
        let product_size_width_id;
        let product_size_length_id;
        let product_size_coverage_id;
        var product_price = $("#product_price"+index).val();

        if(product_type == 21 || product_type == 24){

            if($("#product_size_width_id"+index).val()){
                product_size_width_id = $("#product_size_width_id"+index+" option:selected").text();
            }

            if($("#product_size_length_id"+index).val()){
                product_size_length_id = $("#product_size_length_id"+index+" option:selected").text();
            }

            if($("#product_size_width_id"+index).val() != '' && $("#product_size_length_id"+index).val() != '' && product_price != '')
            {
                var multiplication = product_size_width_id*product_size_length_id;
                $("#product_price_per_sq"+index).val((product_price / multiplication).toFixed(2));
            }

        }
        else if(product_type == 27){
            $("#product_price_per_sq"+index).val(0);
        }
        else if(product_type == 28){
            product_size_coverage_id = $("#product_size_coverage_id"+index+" option:selected").text();

            if($("#product_size_coverage_id"+index).val() != '' && product_price != '')
            {
                $("#product_price_per_sq"+index).val((product_price / product_size_coverage_id).toFixed(2));
            }
        }
    }

    function getSupplierDetails(id){

        var supplier_id = $("#select_supplier"+id).val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.child-product-get-supplier-data') }}',
            type: 'post',
            data: {index : id, supplier_id: supplier_id},
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                $(".data-loader").removeClass('loading');
                $("#price_per_sq_id"+id).text(response.product_price_per_sq);
                $("#price_id"+id).text(response.product_price);
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    function activeDeactive(status, id){

        var lable = status == 0 ? 'active' : 'inactive';
        Swal.fire({
            title: 'Are you sure?',
            text: "You wont to be able to "+ lable +" this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#5B73E8',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, change it!'
        })
        .then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url:"{{route('admin.child-product-enable-disable')}}",
                    type: 'post',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {status: status, id:id},
                    success:function(msg){
                        if(msg.status == true){
                            Swal.fire("Change!",  msg.message, "success");
                            window.LaravelDataTables["child-product-table"].ajax.reload();
                        }else{
                            Swal.fire("Warning!", msg.message, "warning");
                        }
                    },
                    error:function(){
                        Swal.fire("Error!", 'Error in delete Record', "error");
                    }
                });
            }
        })
    }

    function deleteChildProduct(id)
    {
        Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#5B73E8',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                url:"{{route('admin.child-product-delete')}}",
                type: 'delete',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {_method: 'Delete', id:id},
                success:function(msg){
                    console.log(msg);
                    if(msg.status == true){
                        Swal.fire("Deleted!",  msg.message, "success");
                        window.LaravelDataTables["child-product-table"].ajax.reload();
                    }else{
                        Swal.fire("Warning!", msg.message, "warning");
                    }
                },
                error:function(){
                    Swal.fire("Error!", 'Error in delete Record', "error");
                }
                });
            }
        })
    };

    $('.supllierHideShow').addClass("d-none");

    function hideShow(){
        // $(".product_price_per_sq").val('');
        // $(".product_price").val('');
        var product_type = $("#product_type").val();

        $("div").removeClass("d-none");
        $(".added-d-flex").addClass("d-flex");
        $('.supllierHideShow').removeClass("d-none");

        if(product_type == 21){

            $(".product_size_lbs").removeClass('d-flex');
            $(".product_size_lbs").addClass('d-none');

            $(".product_size_gallon").removeClass('d-flex');
            $(".product_size_gallon").addClass('d-none');

            $(".product_size_coverage").removeClass('d-flex');
            $(".product_size_coverage").addClass('d-none');

            $(".product_overage").removeClass('d-flex');
            $(".product_overage").addClass('d-none');

            $(".product_color").removeClass('d-flex');
            $(".product_color").addClass('d-none');

            $(".thin_set_ansis").removeClass('d-flex');
            $(".thin_set_ansis").addClass('d-none');

            $(".thin_set_qty_types").removeClass('d-flex');
            $(".thin_set_qty_types").addClass('d-none');

            $(".trowel_hide_show").removeClass('d-flex');
            $(".trowel_hide_show").addClass('d-none');

            // Remove class for validation
            $(".product_size_lbs_id").removeClass('allfield');
            $(".product_size_gallon_id").removeClass('allfield');
            $(".product_size_coverage_id").removeClass('allfield');
            $(".product_overage_id").removeClass('allfield');
            $(".product_color_id").removeClass('allfield');
            $(".thin_set_ansis_id").removeClass('allfield');
            $(".thin_set_qty_types_id").removeClass('allfield');

            // Remove value on change
            $(".product_size_lbs_id").val("");
            $(".product_size_gallon_id").val("");
            $(".product_size_coverage_id").val("");
            $(".product_overage_id").val("");
            $(".product_color_id").val("");
            $(".thin_set_ansis_id").val();
            $(".thin_set_qty_types_id").val("");

            // Add class for validation
            $(".product_size_width_id").addClass('allfield');
            $(".product_size_length_id").addClass('allfield');
            $(".thin_set_qty_types_id").addClass('allfield');

        }
        else if(product_type == 24){

            $(".product_size_lbs").removeClass('d-flex');
            $(".product_size_lbs").addClass('d-none');

            $(".product_size_gallon").removeClass('d-flex');
            $(".product_size_gallon").addClass('d-none');

            $(".product_size_coverage").removeClass('d-flex');
            $(".product_size_coverage").addClass('d-none');

            $(".product_color").removeClass('d-flex');
            $(".product_color").addClass('d-none');

            $(".thin_set_ansis").removeClass('d-flex');
            $(".thin_set_ansis").addClass('d-none');

            $(".thin_set_qty_types").removeClass('d-flex');
            $(".thin_set_qty_types").addClass('d-none');

            $(".trowel_hide_show").removeClass('d-flex');
            $(".trowel_hide_show").addClass('d-none');

            // Remove class for validation
            $(".product_size_lbs_id").removeClass('allfield');
            $(".product_size_gallon_id").removeClass('allfield');
            $(".product_size_coverage_id").removeClass('allfield');
            $(".product_color_id").removeClass('allfield');
            $(".thin_set_ansis_id").removeClass('allfield');
            $(".thin_set_qty_types_id").removeClass('allfield');

            // Remove value on change
            $(".product_size_lbs_id").val("");
            $(".product_size_gallon_id").val("");
            $(".product_size_coverage_id").val("");
            $(".product_color_id").val("");
            $(".thin_set_ansis_id").val();
            $(".thin_set_qty_types_id").val("");

            // Add class for validation
            $(".product_overage_id").addClass('allfield');
            $(".product_size_width_id").addClass('allfield');
            $(".product_size_length_id").addClass('allfield');
        }
        else if(product_type == 27){

            $(".product_size_width").removeClass('d-flex');
            $(".product_size_width").addClass('d-none');

            $(".product_size_gallon").removeClass('d-flex');
            $(".product_size_gallon").addClass('d-none');

            $(".product_size_coverage").removeClass('d-flex');
            $(".product_size_coverage").addClass('d-none');

            // Remove class for validation
            $(".product_size_width_id").removeClass('allfield');
            $(".product_size_gallon_id").removeClass('allfield');
            $(".product_size_coverage_id").removeClass('allfield');

            // Remove value on change
            $(".product_size_width_id").val("");
            $(".product_size_gallon_id").val("");
            $(".product_size_coverage_id").val("");

            // Add class for validation
            $(".product_size_lbs_id").addClass('allfield');
            $(".product_overage_id").addClass('allfield');
            $(".product_color_id").addClass('allfield');
            $(".thin_set_ansis_id").addClass('allfield');
            $(".thin_set_qty_types_id").addClass('allfield');
            $(".product_size_length_id").addClass('allfield');
            $(".trowel_type").addClass('allfield');
            $(".trowel_size").addClass('allfield');
            $(".coverage").addClass('allfield');
            // $(".product_size_length_id").attr('disabled', true)
        }
        else if(product_type == 28){

            $(".product_size_width").removeClass('d-flex');
            $(".product_size_width").addClass('d-none');

            $(".product_size_length").removeClass('d-flex');
            $(".product_size_length").addClass('d-none');

            $(".product_size_lbs").removeClass('d-flex');
            $(".product_size_lbs").addClass('d-none');

            $(".product_overage").removeClass('d-flex');
            $(".product_overage").addClass('d-none');

            $(".product_color").removeClass('d-flex');
            $(".product_color").addClass('d-none');

            $(".thin_set_ansis").removeClass('d-flex');
            $(".thin_set_ansis").addClass('d-none');

            $(".thin_set_qty_types").removeClass('d-flex');
            $(".thin_set_qty_types").addClass('d-none');

            $(".trowel_hide_show").removeClass('d-flex');
            $(".trowel_hide_show").addClass('d-none');

            // Remove class for validation
            $(".product_size_width_id").removeClass('allfield');
            $(".product_size_length_id").removeClass('allfield');
            $(".product_size_lbs_id").removeClass('allfield');
            $(".product_overage_id").removeClass('allfield');
            $(".product_color_id").removeClass('allfield');
            $(".thin_set_ansis_id").removeClass('allfield');
            $(".thin_set_qty_types_id").removeClass('allfield');

            // Remove value on change
            $(".product_size_width_id").val("");
            $(".product_size_length_id").val("");
            $(".product_size_lbs_id").val("");
            $(".product_overage_id").val("");
            $(".product_color_id").val("");
            $(".thin_set_ansis_id").val();
            $(".thin_set_qty_types_id").val("");

            // Add class for validation
            $(".product_size_gallon_id").addClass('allfield');
            $(".product_size_coverage_id").addClass('allfield');
        }
    }

    $('#childProductForm').validate({
        rules: {
            manufacture_id: {
                required: true,
            },
            name: {
                required: true,
            },
            product_area: {
                required: true,
            },
            product_type: {
                required: true,
            },
            'data_sheet_ids[]': {
                required: true,
            }
        },
        messages: {
            manufacture_id: {
                required: "Please select product manufacturer"
            },
            name: {
                required: "Please enter product name"
            },
            product_area: {
                required: "Please select product area"
            },
            product_type: {
                required: "Please select product type"
            },
            'data_sheet_ids[]': {
                required: "Please data sheet ids"
            },
        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var isValid = true;

            $(".allfield").each(function() {
                if($.trim($(this).val()) === '' && $(this).val().length < 1) {
                    $(this).addClass('is-invalid');
                    isValid = false;
                }else {
                    $(this).removeClass('is-invalid');
                    isValid = true;
                }
            });

            $(".is-invalid:first").focus();

            if(isValid) {
                var form = $('#childProductForm')[0];
                var data = new FormData(form);
                childProductForm(data);
            }

            // $(".allfield").each(function() {
            //     if($.trim($(this).val()) === '' && $(this).val().length < 1) {
            //         $(this).addClass('is-invalid');
            //         isValid = false;
            //     } else {
            //         $(this).removeClass('is-invalid');
            //         isValid = true;
            //     }

            //     if (!$('.thin_set_ansis_id').is(':checked')) {
            //         isValid = false;
            //         $(this).addClass('is-invalid');
            //         $('.thin_set_ansis_error').text('Please select at least one checkbox').css('color', 'red');;
            //     }else {
            //         $('.thin_set_ansis_error').text('');
            //         $(this).removeClass('is-invalid');
            //         isValid = true;
            //     }

            //     if (!$('.thin_set_qty_types_id').is(':checked')) {
            //         isValid = false;
            //         $(this).addClass('is-invalid');
            //         $('.thin_set_qty_error').text('Please select at least one checkbox').css('color', 'red');;
            //     }else {
            //         $('.thin_set_qty_error').text('');
            //         $(this).removeClass('is-invalid');
            //         isValid = true;
            //     }
            // });

            // $(".is-invalid:first").focus();

            // if(isValid) {
            //     var form = $('#childProductForm')[0];
            //     var data = new FormData(form);
            //     childProductForm(data);
            // }
        }
    });

    function childProductForm(form)
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.child-product-store') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    setTimeout(() => {
                        window.location.href = '{{ route('admin.child-product-list') }}';
                    }, 1000);
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $("#product_type").on('change',function(){
        $(".dis-product-type").html('');
    });

    // $(document).ready(function() {
    //     var product_type = $("#product_type").val();

    //     if(product_type == 21){

    //         $(".product_size_lbs").removeClass('d-flex');
    //         $(".product_size_lbs").addClass('d-none');

    //         $(".product_size_gallon").removeClass('d-flex');
    //         $(".product_size_gallon").addClass('d-none');

    //         $(".product_size_coverage").removeClass('d-flex');
    //         $(".product_size_coverage").addClass('d-none');

    //         $(".product_overage").removeClass('d-flex');
    //         $(".product_overage").addClass('d-none');

    //         $(".product_color").removeClass('d-flex');
    //         $(".product_color").addClass('d-none');

    //         $(".thin_set_ansis").removeClass('d-flex');
    //         $(".thin_set_ansis").addClass('d-none');

    //         $(".thin_set_qty_types").removeClass('d-flex');
    //         $(".thin_set_qty_types").addClass('d-none');

    //         $(".trowel_hide_show").removeClass('d-flex');
    //         $(".trowel_hide_show").addClass('d-none');

    //         // Remove class for validation
    //         $(".product_size_lbs_id").removeClass('allfield');
    //         $(".product_size_gallon_id").removeClass('allfield');
    //         $(".product_size_coverage_id").removeClass('allfield');
    //         $(".product_overage_id").removeClass('allfield');
    //         $(".product_color_id").removeClass('allfield');
    //         $(".thin_set_ansis_id").removeClass('allfield');
    //         $(".thin_set_qty_types_id").removeClass('allfield');

    //         // Remove value on change
    //         $(".product_size_lbs_id").val("");
    //         $(".product_size_gallon_id").val("");
    //         $(".product_size_coverage_id").val("");
    //         $(".product_overage_id").val("");
    //         $(".product_color_id").val("");
    //         $(".thin_set_ansis_id").val();
    //         $(".thin_set_qty_types_id").val("");

    //         // Add class for validation
    //         $(".product_size_width_id").addClass('allfield');
    //         $(".product_size_length_id").addClass('allfield');
    //         $(".thin_set_qty_types_id").addClass('allfield');

    //     }
    //     else if(product_type == 24){

    //         $(".product_size_lbs").removeClass('d-flex');
    //         $(".product_size_lbs").addClass('d-none');

    //         $(".product_size_gallon").removeClass('d-flex');
    //         $(".product_size_gallon").addClass('d-none');

    //         $(".product_size_coverage").removeClass('d-flex');
    //         $(".product_size_coverage").addClass('d-none');

    //         $(".product_color").removeClass('d-flex');
    //         $(".product_color").addClass('d-none');

    //         $(".thin_set_ansis").removeClass('d-flex');
    //         $(".thin_set_ansis").addClass('d-none');

    //         $(".thin_set_qty_types").removeClass('d-flex');
    //         $(".thin_set_qty_types").addClass('d-none');

    //         $(".trowel_hide_show").removeClass('d-flex');
    //         $(".trowel_hide_show").addClass('d-none');

    //         // Remove class for validation
    //         $(".product_size_lbs_id").removeClass('allfield');
    //         $(".product_size_gallon_id").removeClass('allfield');
    //         $(".product_size_coverage_id").removeClass('allfield');
    //         $(".product_color_id").removeClass('allfield');
    //         $(".thin_set_ansis_id").removeClass('allfield');
    //         $(".thin_set_qty_types_id").removeClass('allfield');

    //         // Remove value on change
    //         $(".product_size_lbs_id").val("");
    //         $(".product_size_gallon_id").val("");
    //         $(".product_size_coverage_id").val("");
    //         $(".product_color_id").val("");
    //         $(".thin_set_ansis_id").val();
    //         $(".thin_set_qty_types_id").val("");

    //         // Add class for validation
    //         $(".product_overage_id").addClass('allfield');
    //         $(".product_size_width_id").addClass('allfield');
    //         $(".product_size_length_id").addClass('allfield');
    //     }
    //     else if(product_type == 27){

    //         $(".product_size_width").removeClass('d-flex');
    //         $(".product_size_width").addClass('d-none');

    //         $(".product_size_gallon").removeClass('d-flex');
    //         $(".product_size_gallon").addClass('d-none');

    //         $(".product_size_coverage").removeClass('d-flex');
    //         $(".product_size_coverage").addClass('d-none');

    //         // Remove class for validation
    //         $(".product_size_width_id").removeClass('allfield');
    //         $(".product_size_gallon_id").removeClass('allfield');
    //         $(".product_size_coverage_id").removeClass('allfield');

    //         // Remove value on change
    //         $(".product_size_width_id").val("");
    //         $(".product_size_gallon_id").val("");
    //         $(".product_size_coverage_id").val("");

    //         // Add class for validation
    //         $(".product_size_lbs_id").addClass('allfield');
    //         $(".product_overage_id").addClass('allfield');
    //         $(".product_color_id").addClass('allfield');
    //         $(".thin_set_ansis_id").addClass('allfield');
    //         $(".thin_set_qty_types_id").addClass('allfield');
    //         $(".product_size_length_id").addClass('allfield');
    //         $(".trowel_type").addClass('allfield');
    //         $(".trowel_size").addClass('allfield');
    //         $(".coverage").addClass('allfield');
    //         // $(".product_size_length_id").attr('disabled', true)
    //     }
    //     else if(product_type == 28){

    //         $(".product_size_width").removeClass('d-flex');
    //         $(".product_size_width").addClass('d-none');

    //         $(".product_size_length").removeClass('d-flex');
    //         $(".product_size_length").addClass('d-none');

    //         $(".product_size_lbs").removeClass('d-flex');
    //         $(".product_size_lbs").addClass('d-none');

    //         $(".product_overage").removeClass('d-flex');
    //         $(".product_overage").addClass('d-none');

    //         $(".product_color").removeClass('d-flex');
    //         $(".product_color").addClass('d-none');

    //         $(".thin_set_ansis").removeClass('d-flex');
    //         $(".thin_set_ansis").addClass('d-none');

    //         $(".thin_set_qty_types").removeClass('d-flex');
    //         $(".thin_set_qty_types").addClass('d-none');

    //         $(".trowel_hide_show").removeClass('d-flex');
    //         $(".trowel_hide_show").addClass('d-none');

    //         // Remove class for validation
    //         $(".product_size_width_id").removeClass('allfield');
    //         $(".product_size_length_id").removeClass('allfield');
    //         $(".product_size_lbs_id").removeClass('allfield');
    //         $(".product_overage_id").removeClass('allfield');
    //         $(".product_color_id").removeClass('allfield');
    //         $(".thin_set_ansis_id").removeClass('allfield');
    //         $(".thin_set_qty_types_id").removeClass('allfield');

    //         // Remove value on change
    //         $(".product_size_width_id").val("");
    //         $(".product_size_length_id").val("");
    //         $(".product_size_lbs_id").val("");
    //         $(".product_overage_id").val("");
    //         $(".product_color_id").val("");
    //         $(".thin_set_ansis_id").val();
    //         $(".thin_set_qty_types_id").val("");

    //         // Add class for validation
    //         $(".product_size_gallon_id").addClass('allfield');
    //         $(".product_size_coverage_id").addClass('allfield');
    //     }
    // });

</script>
