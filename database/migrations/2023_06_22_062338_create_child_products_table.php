<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('child_products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('manufacture_id')->nullable();
            $table->foreign('manufacture_id')->references('id')->on('manufacturers')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name', 255)->nullable();
            $table->string('logo', 255)->nullable();
            $table->enum('product_area', ['Substrate', 'Underlayment']);
            $table->unsignedBigInteger('ansi_id')->nullable();
            $table->foreign('ansi_id')->references('id')->on('ansis')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('data_sheet_id')->nullable();
            $table->foreign('data_sheet_id')->references('id')->on('data_sheets')->onDelete('cascade')->onUpdate('cascade');
            $table->string('data_sheet_ids', 255)->nullable()->comment('Added multiple sheet ids in sepreted comma');
            $table->unsignedBigInteger('product_type_id')->nullable();
            $table->foreign('product_type_id')->references('id')->on('product_types')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('product_type_main_sub_child_id')->nullable();
            $table->foreign('product_type_main_sub_child_id')->references('id')->on('product_types')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('product_type_secondary_sub_child_id')->nullable();
            $table->foreign('product_type_secondary_sub_child_id')->references('id')->on('product_types')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('product_package_type_id')->nullable();
            $table->foreign('product_package_type_id')->references('id')->on('product_package_types')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('status')->default(1)->comment('1 = active, 0 = deactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('child_products');
    }
};
