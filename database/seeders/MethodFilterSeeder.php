<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MethodFilterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('method_filters')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $productTypes = [
            [
                'name' => 'Install Location Types',
                'is_lable' => 1,
                'is_checkbox' => 0,
                'is_lock' => 0,
                'is_default_filter' => 1,
                'type' => 'exposure',
                'parent_id' => null,
                'child_data' =>[
                    [
                        'name' => 'Exposure',
                        'is_lable' => 1,
                        'is_checkbox' => 0,
                        'is_lock' => 0,
                        'is_default_filter' => 1,
                        'type' => 'exposure',
                        'child_data' => [
                            [
                                'name' => 'Interior',
                                'is_lable' => 1,
                                'is_checkbox' => 0,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'exposure',
                                'child_data' => [
                                    [
                                        'name' => 'Backsplashes',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 1,
                                        'is_default_filter' => 1,
                                        'type' => 'exposure',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Bath Showers',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 1,
                                        'is_default_filter' => 1,
                                        'type' => 'exposure',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Bath Tubs',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 1,
                                        'is_default_filter' => 1,
                                        'type' => 'exposure',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Ceilings',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 1,
                                        'is_default_filter' => 1,
                                        'type' => 'exposure',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Countertops',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 1,
                                        'is_default_filter' => 1,
                                        'type' => 'exposure',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Floors',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'exposure',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Stairs',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 1,
                                        'is_default_filter' => 1,
                                        'type' => 'exposure',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Walls',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 1,
                                        'is_default_filter' => 1,
                                        'type' => 'exposure',
                                        'child_data' => []
                                    ]
                                ]
                            ],
                            [
                                'name' => 'Exterior',
                                'is_lable' => 1,
                                'is_checkbox' => 0,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'exposure',
                                'child_data' => []
                            ]
                        ]
                    ],
                    [
                        'name' => 'Substrate',
                        'is_lable' => 1,
                        'is_checkbox' => 0,
                        'is_lock' => 0,
                        'is_default_filter' => 1,
                        'type' => 'substrate',
                        'child_data' => [
                            [
                                'name' => 'Playwood Substrates',
                                'is_lable' => 1,
                                'is_checkbox' => 0,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'substrate',
                                'child_data' => [
                                    [
                                        'name' => 'Joist Spacing',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'substrate',
                                        'child_data' => [
                                            [
                                                'name' => '16" 0.c.',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'substrate',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => '19.2" 0.c.',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'substrate',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => '24" 0.c.',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'substrate',
                                                'child_data' => []
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'name' => 'Concrete Substrates',
                                'is_lable' => 1,
                                'is_checkbox' => 0,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'substrate',
                                'child_data' => [
                                    [
                                        'name' => 'On-ground',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'substrate',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Above-ground',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'substrate',
                                        'child_data' => []
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'name' => 'Underlayments',
                        'is_lable' => 1,
                        'is_checkbox' => 0,
                        'is_lock' => 0,
                        'is_default_filter' => 1,
                        'type' => 'underlayments',
                        'child_data' => [
                            [
                                'name' => 'Backer Boards',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'underlayments',
                                'child_data' => [
                                    [
                                        'name' => 'Cement Backers',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Fiber Cement Backers',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Foam Backers',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => []
                                    ]
                                ]
                            ],
                            [
                                'name' => 'Fire Rated',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'underlayments',
                                'child_data' => []
                            ],
                            [
                                'name' => 'Lightweights',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'underlayments',
                                'child_data' => []
                            ],
                            [
                                'name' => 'Membranes',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'underlayments',
                                'child_data' => [
                                    [
                                        'name' => 'Cleavage',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => [
                                            [
                                                'name' => 'Cleavage (bonded)',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'Cleavage (unbonded)',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ]
                                        ]
                                    ],
                                    [
                                        'name' => 'Crack Isolation',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Sound',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Uncoupling',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => [
                                            [
                                                'name' => 'Thin set (bonded)',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'Adhesive (bonded)',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'Unbonded',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ]
                                        ]
                                    ],
                                    [
                                        'name' => 'Waterproofing',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => [
                                            [
                                                'name' => 'Liquid',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'Fabric',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ]
                                        ]
                                    ],
                                    [
                                        'name' => 'Mortar Beds',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => [
                                            [
                                                'name' => 'Bonded',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'Unbonded',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ]
                                        ]
                                    ],
                                    [
                                        'name' => 'Movement Joints',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => [
                                            [
                                                'name' => 'Perimeter Joints',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'In-Field Joints',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ]
                                        ]
                                    ],
                                    [
                                        'name' => 'MyWay',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Profiles',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => [
                                            [
                                                'name' => 'Floors',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'Walls & Counters',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ]
                                        ]
                                    ],
                                    [
                                        'name' => 'Radiant Heats',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => [
                                            [
                                                'name' => 'Radiant Heat – Electric',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'Radiant Heat - Hydronic',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ]
                                        ]
                                    ],
                                    [
                                        'name' => 'SLUs',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Structural Planks',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => []
                                    ],
                                    [
                                        'name' => 'Unique Methods',
                                        'is_lable' => 0,
                                        'is_checkbox' => 1,
                                        'is_lock' => 0,
                                        'is_default_filter' => 1,
                                        'type' => 'underlayments',
                                        'child_data' => [
                                            [
                                                'name' => 'Over Contaminated Floors',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'Over Cracked Concrete',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'Over Existing Vinyl',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ],
                                            [
                                                'name' => 'Over Existing Tile',
                                                'is_lable' => 0,
                                                'is_checkbox' => 1,
                                                'is_lock' => 0,
                                                'is_default_filter' => 1,
                                                'type' => 'underlayments',
                                                'child_data' => []
                                            ]
                                        ]
                                    ],

                                ]
                            ],
                        ]
                    ],
                    [
                        'name' => 'Tile Types',
                        'is_lable' => 1,
                        'is_checkbox' => 0,
                        'is_lock' => 0,
                        'is_default_filter' => 1,
                        'type' => 'tile',
                        'parent_id' => null,
                        'child_data' => [
                            [
                                'name' => 'Tile (Ceramic)',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'tile',
                                'child_data' => []
                            ],
                            [
                                'name' => 'Tile (Porcelain Tile)',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'tile',
                                'child_data' => []
                            ],
                            [
                                'name' => 'Tile (Stone Tile)',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'tile',
                                'child_data' => []
                            ]
                        ]
                    ],
                    [

                        'name' => 'Price per Sq. Ft.',
                        'is_lable' => 1,
                        'is_checkbox' => 0,
                        'is_lock' => 0,
                        'is_default_filter' => 1,
                        'type' => 'price',
                        'parent_id' => null,
                        'child_data' => [
                            [
                                'name' => 'Under $2',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'price',
                                'child_data' => []
                            ],
                            [
                                'name' => 'Up t0 $3',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'price',
                                'child_data' => []
                            ],
                            [
                                'name' => '$3.01 to $4',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'price',
                                'child_data' => []
                            ],
                            [
                                'name' => '$4.01 to $5',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'price',
                                'child_data' => []
                            ],
                            [
                                'name' => 'Over $5.01',
                                'is_lable' => 0,
                                'is_checkbox' => 1,
                                'is_lock' => 0,
                                'is_default_filter' => 1,
                                'type' => 'price',
                                'child_data' => []
                            ]
                        ]
                    ]


                ]

            ]
        ];

        foreach ($productTypes as $productType) {
            $this->insertProductType($productType, null);
        }
    }

    private function insertProductType(array $productType, ?int $parentId)
    {
        $data = [
            'name' => $productType['name'],
            'is_lable' => $productType['is_lable'],
            'is_checkbox' => $productType['is_checkbox'],
            'is_lock' => $productType['is_lock'],
            'is_default_filter' => $productType['is_default_filter'],
            'type' => $productType['type'],
            'parent_id' => array_key_exists('parent_id', $productType) ? null : $parentId,
        ];

        $productId = DB::table('method_filters')->insertGetId($data);

        if (!empty($productType['child_data'])) {
            foreach ($productType['child_data'] as $childProductType) {
                $this->insertProductType($childProductType, $productId);
            }
        }

        return $productId;
    }
}
