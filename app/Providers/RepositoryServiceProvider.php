<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        // Admin Repository Interface List
        $this->app->bind('App\Interfaces\admin\LoginRepositoryInterface', 'App\Repositories\admin\LoginRepository');
        $this->app->bind('App\Interfaces\admin\UserRepositoryInterface', 'App\Repositories\admin\UserRepository');
        $this->app->bind('App\Interfaces\admin\ManufacturerRepositoryInterface','App\Repositories\admin\MenufacturerRepository');
        $this->app->bind('App\Interfaces\admin\SupplierRepositoryInterface','App\Repositories\admin\SupplierRepository');
        $this->app->bind('App\Interfaces\admin\ParentProductRepositoryInterface','App\Repositories\admin\ParentProductRepository');
        $this->app->bind('App\Interfaces\admin\ChildProductRepositoryInterface','App\Repositories\admin\ChildProductRepository');
        $this->app->bind('App\Interfaces\admin\CmsPageRepositoryInterface','App\Repositories\admin\CmsPageRepository');
        
        // API Repository Interface List
        $this->app->bind('App\Interfaces\api\UserRepositoryInterface', 'App\Repositories\api\UserRepository');
        $this->app->bind('App\Interfaces\api\SupplierRepositoryInterface', 'App\Repositories\api\SupplierRepository');
        $this->app->bind('App\Interfaces\api\SubscriptionRepositoryInterface', 'App\Repositories\api\SubscriptionRepository');
        $this->app->bind('App\Interfaces\api\MethodRepositoryInterface', 'App\Repositories\api\MethodRepository');
    }

    public function boot()
    {
        //
    }
}
