<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Admin extends Authenticatable
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];

    public function profilePicture(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value ? asset('super_admin/images/'.$value) : asset('no_image.jpg'),
            set: fn ($value) => $value,
        );
    }
}
