<?php

namespace App\Repositories\admin;

use App\Interfaces\admin\CmsPageRepositoryInterface;
use App\Models\CmsPage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CmsPageRepository implements CmsPageRepositoryInterface
{
    public function model()
    {
        //return YourModel::class;
    }

    public function CmsUpdate($data)
    {
        try {
            DB::beginTransaction();

            $page = [
                'title' => $data['title'],
                'description' => $data['description'],
            ];
            
            CmsPage::where('id', $data['id'])->update($page);

            DB::commit();
            return ['status' => true, 'message' =>  'Cms page updated successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('cms-page update : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }
}
