<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('child_product_suppliers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('child_product_id')->nullable();
            $table->foreign('child_product_id')->references('id')->on('child_products')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('supplier_id')->nullable();
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade')->onUpdate('cascade');
            $table->string('product_links', 255)->nullable();
            $table->string('product_mdl', 255)->nullable();
            $table->unsignedBigInteger('product_size_width_qty_id')->nullable();
            $table->foreign('product_size_width_qty_id')->references('id')->on('product_size_width_qties')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('product_size_length_qty_id')->nullable();
            $table->foreign('product_size_length_qty_id')->references('id')->on('product_size_length_qties')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('product_size_lbs_id')->comment('used for thin set')->nullable();
            $table->foreign('product_size_lbs_id')->references('id')->on('product_size_lbs')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('product_color_id')->comment('used for thin set')->nullable();
            $table->foreign('product_color_id')->references('id')->on('product_colors')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('prodct_size_gallon_id')->comment('used for waterproofing liquid')->nullable();
            $table->foreign('prodct_size_gallon_id')->references('id')->on('prodct_size_gallons')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('prodct_size_coverage_qtie_id')->comment('used for waterproofing liquid')->nullable();
            $table->foreign('prodct_size_coverage_qtie_id')->references('id')->on('prodct_size_coverage_qties')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('overage_id')->nullable();
            $table->foreign('overage_id')->references('id')->on('overages')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('trowel_id')->nullable();
            $table->foreign('trowel_id')->references('id')->on('trowels')->onDelete('cascade')->onUpdate('cascade');

            $table->string('ansi_ids', 255)->nullable();
            $table->string('thin_set_qty_type_ids', 255)->nullable();

            $table->decimal('product_price', $precision = 8, $scale = 2)->default(0);
            $table->decimal('product_price_per_sq', $precision = 8, $scale = 2)->default(0);
            $table->boolean('status')->default(1)->comment('1 = active, 0 = deactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('child_product_suppliers');
    }
};
