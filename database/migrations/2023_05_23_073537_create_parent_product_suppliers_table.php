<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('parent_product_suppliers', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('parent_product_id')->nullable();
            $table->foreign('parent_product_id')->references('id')->on('parent_products')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('supplier_id')->nullable();
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade')->onUpdate('cascade');
            $table->string('product_links', 255)->nullable();
            $table->string('product_mdl', 255)->nullable();
            $table->unsignedBigInteger('product_size_width_qty_id')->nullable();
            $table->foreign('product_size_width_qty_id')->references('id')->on('product_size_width_qties')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('product_size_length_qty_id')->nullable();
            $table->foreign('product_size_length_qty_id')->references('id')->on('product_size_length_qties')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('product_size_thickness_id')->nullable();
            $table->foreign('product_size_thickness_id')->references('id')->on('product_size_thicknesses')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('specification_fastners_id')->nullable();
            $table->foreign('specification_fastners_id')->references('id')->on('specification_fastners')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('specification_fastners_screw_id')->nullable();
            $table->foreign('specification_fastners_screw_id')->references('id')->on('specification_fastners_screws')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('specification_fastners_nails_id')->nullable();
            $table->foreign('specification_fastners_nails_id')->references('id')->on('specification_fastners_nails')->onDelete('cascade')->onUpdate('cascade');
            $table->decimal('product_price', $precision = 8, $scale = 2)->default(0);
            $table->decimal('product_price_per_sq', $precision = 8, $scale = 2)->default(0);
            $table->boolean('status')->default(1)->comment('1 = active, 0 = deactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('parent_product_suppliers');
    }
};
