<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>TMW | Forget Password</title>
        <link rel="icon" href="{{ asset('admin/dist/img/icon.png') }}" type="image/png">
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- icheck bootstrap -->
        <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('admin/dist/css/adminlte.min.css') }}">
        <!-- Toastr -->
        <link rel="stylesheet" href="{{ asset('admin/plugins/toastr/toastr.min.css') }}">
        <!-- custom -->
        <link rel="stylesheet" href="{{ asset('admin/dist/css/custom.css') }}">
    </head>
    <body class="hold-transition login-page">
        <div class="data-loader"></div>
        <div class="login-box">
            <div class="card card-outline card-primary">
                <div class="card-header text-center">
                    {{-- <a href="#" class="h1"><b>MY</b>CPP</a> --}}
                    <img width="200px" src="{{ asset('admin/dist/img/TilingMyWay.png') }}" >
                </div>
                <div class="card-body">
                    <h2 class="login-box-msg">Forget Password</h2>
                    <p class="text-center">Enter your account email address to receive a link allowing you to create a new password</p>
                    <form action="#" id="forgetForm" method="post">
                        <div class="input-group form-group mb-3">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary btn-block">Request new password</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                     <p class="mt-3 mb-1 d-flex justify-content-between">
                        <a href="{{route('admin.login')}}">Back to Login</a>
                        {{-- <a href="login.html">Resend link</a> --}}
                    </p>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- /.login-box -->
        <!-- jQuery -->
        <script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap 4 -->
        <script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('admin/dist/js/adminlte.min.js') }}"></script>
        <!-- jquery-validation -->
        <script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
        <!-- Toastr -->
        <script src="{{ asset('admin/plugins/toastr/toastr.min.js') }}"></script>
        <!-- Custom -->
        <script src="{{ asset('admin/dist/js/custom.js') }}"></script>

        @include('admin.customJs.forget-password')
    </body>
</html>