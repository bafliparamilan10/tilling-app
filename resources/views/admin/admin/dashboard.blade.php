@extends('admin.layouts.master')

@section('page_title', 'Dashboard')
@section('content_header')
 <!-- Content Header (Page header) -->
 <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Admin Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  @endsection
  @section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row mb-2">
        <a href="{{ route('admin.user-list') }}" >
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner p-2">
                <h3>{{ $totalUsers }}</h3>

                <p>Total Number of Registered Users</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-contact"></i>
              </div>
              <a href="{{ route('admin.user-list') }}" class="small-box-footer">View More</a>
            </div>
          </div>
      </a>
        <!-- ./col -->
        <a href="{{ route('admin.user-list') }}" >
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner p-2">
                <h3>{{ $totalSubscribeUsers }}</h3>

                <p>Total Number of Subscribed Users</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-person-add"></i>
              </div>
              <a href="{{ route('admin.user-list') }}" class="small-box-footer">View More</a>
            </div>
          </div>
        </a>
        <!-- ./col -->
        <a href="{{ route('admin.manufacturer-list') }}" >
          <div class="col-lg-4 col-6">
            <div class="small-box bg-warning">
              <div class="inner p-2">
                <h3>{{ $totalManufacturer }}</h3>

                <p>Total Number of Manufacturers</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-briefcase"></i>
              </div>
              <a href="{{ route('admin.manufacturer-list') }}" class="small-box-footer">View More</a>
            </div>
          </div>
        </a>

        <a href="javascript:void(0);" >
            <div class="col-lg-6 col-6">
              <div class="small-box bg-danger">
                <div class="inner p-2">
                  <h3>0</h3>

                  <p>Total Number of Methods</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-folder"></i>
                </div>
                <a href="javascript:void(0);" class="small-box-footer">View More</a>
              </div>
            </div>
          </a>

          <a href="{{ route('admin.parent-product-list') }}" >
            <div class="col-lg-6 col-6">
              <div class="small-box bg-dark">
                <div class="inner p-2">
                  <h3>{{ $parentProduct }}</h3>

                  <p>Total Number of Parent Products</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-list"></i>
                </div>
                <a href="{{ route('admin.parent-product-list') }}" class="small-box-footer">View More</a>
              </div>
            </div>
          </a>

          <a href="javascript:void(0);" >
            <div class="col-lg-6 col-6">
              <div class="small-box bg-primary">
                <div class="inner p-2">
                  <h3>0</h3>

                  <p>Total Number of Child Products</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-list-outline"></i>
                </div>
                <a href="javascript:void(0);" class="small-box-footer">View More</a>
              </div>
            </div>
          </a>

          <a href="{{ route('admin.supplier-list') }}" >
            <div class="col-lg-6 col-6">
              <div class="small-box bg-success">
                <div class="inner p-2">
                  <h3>{{ $totalSupplier }}</h3>

                  <p>Total Number of Suppliers</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-people"></i>
                </div>
                <a href="{{ route('admin.supplier-list') }}" class="small-box-footer">View More</a>
              </div>
            </div>
          </a>
      </div>
      <!-- /.row -->

      <div class="row">


        <div class="col-lg-6 col-6 border-right border-3">
            <hr>
            <div class="row">
                <div class="col-2">
                    <label for="">Methods</label>
                </div>
                <div class="col-5">
                    <select name="" id="" class="form-control" >
                        <option value="">Select Manufacturer</option>
                        @foreach ($manufacturerData as $manufacturerDataValue)
                            <option value="{{ $manufacturerDataValue->id }}">{{ $manufacturerDataValue->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-5">
                    <input type="text" class="form-control manufactureData">
                </div>
            </div>

            <hr>

            <canvas id="methodChart"></canvas>
        </div>
        <div class="col-lg-6 col-6">
            <hr>
            <div class="row">
                <div class="col-2">
                    <label for="">Suppliers</label>
                </div>
                <div class="col-5">
                    {{-- <select name="" id="" class="form-control" >
                        <option value="">Select Supplier</option>
                        @foreach ($supplierData as $supplierDataValue)
                            <option value="{{ $supplierDataValue->id }}">{{ $supplierDataValue->name }}</option>
                        @endforeach
                    </select> --}}
                </div>
                <div class="col-5">
                    <input type="text" class="form-control supplierData">
                </div>
            </div>

            <hr>

            <canvas id="supplierChart"></canvas>
        </div>
      </div>

    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  @endsection

@push('customJs')

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>

    //used for method chart

    let allData = [12, 19, 3, 5, 2, 3];



    const ctx = document.getElementById('methodChart');

    var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
      datasets: [{
        label: '# of Votes',
        data: allData,
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });

  //used for supplier chart


  const ctx1 = document.getElementById('supplierChart');

  new Chart(ctx1, {
    type: 'bar',
    data: {
      labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
      datasets: [{
        label: '# of Votes',
        data: allData,
        borderWidth: 1,
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });

  function updateData()
  {
        myChart.data.datasets[0].data = [10, 19, 3, 5, 2, 3];
        myChart.update();
  }

  $('.manufactureData, .supplierData').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "alwaysShowCalendars": true,
        "startDate": new Date(),
        "endDate": new Date()
    }, function(start, end, label) {
        console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    });

</script>
@endpush
