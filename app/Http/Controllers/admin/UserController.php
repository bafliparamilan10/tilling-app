<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DataTables\UserDataTable;
use App\Interfaces\admin\UserRepositoryInterface;
use App\Http\Requests\admin\CreateUserRequest;
use App\Models\City;
use App\Models\State;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;

class UserController extends Controller
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public function list(UserDataTable $dataTable)
    {
        $state = State::where('country_id', 233)->orderBy('name', 'ASC')->get();
        return $dataTable->render('admin.user.list', compact('state'));
    }
    public function addUser()
    {
        $state = State::where('country_id', 233)->orderBy('name', 'ASC')->get();
        return view('admin.user.add', compact('state'));
    }
    public function deleteUser(Request $request)
    {
        $data =  $this->userRepository->deleteUser($request['id']);
        return $data;
    }
    public function createUser(CreateUserRequest $request)
    {
        $data =  $this->userRepository->createUser($request->all());
        return $data;
    }

    public function userEditPage($ids)
    {
        $id = Crypt::decrypt($ids);
        $userData = User::find($id);
        $userData->date_of_birth = Carbon::parse($userData->date_of_birth)->format('d-m-Y');
        $city = City::where('country_id', 233)->where('state_id',$userData->state_id)->orderBy('name', 'ASC')->get();
        $state = State::where('country_id', 233)->orderBy('name', 'ASC')->get();

        return view('admin.user.edit', compact('city', 'state', 'userData'));
    }

    public function userDetails($id)
    {
        $id = Crypt::decrypt($id);
        $userData = User::with(['getCityDetails','getStateDetails','getPurchasedPlanDetails'])->where('id', $id)->first();
        return view('admin.user.details', compact('userData'));
    }

    public function getCity(Request $request)
    {
        $response = $this->userRepository->getCity($request->id);
        return  $response;
    }

    public function userEnableDisable(Request $request)
    {
        $response = $this->userRepository->userEnableDisable($request->all());
        return  $response;
    }

    public function test()
    {
        // $state = State::get();
        // foreach ($state as $key => $value) {
        //     $state_id = $value->id;
        //     $city_id = City::where('state_id',$state_id)->count();
        //     if($city_id == 0)
        //     {
        //         $supplierData = new City();
        //         $supplierData->state_id         = $state_id;
        //         $supplierData->name         = $value->name;
        //         $supplierData->country_id         = $value->country_id;
        //         $supplierData->save();
        //         print_r($supplierData);
        //         print_r("</br>");
        //     }
        // }
    }
}
