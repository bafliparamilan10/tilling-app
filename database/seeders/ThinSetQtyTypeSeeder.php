<?php

namespace Database\Seeders;

use App\Models\ThinSetQtyType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ThinSetQtyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('thin_set_qty_types')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        ThinSetQtyType::insert([
            [
                'name' => 'Extended',
            ],
            [
                'name' => 'Fast Settings',
            ],
            [
                'name' => 'Heavy',
            ],
            [
                'name' => 'Thixotropic',
            ],
            [
                'name' => 'LFT',
            ],
            [
                'name' => 'LHT',
            ]
        ]);
    }
}
