<script>
    $('.select2').select2();
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });




    function managePopup(popupId) {
        $("#" + popupId).modal('show');
    }

    function productTypeManage(val, type) {
        if (type == 'secondary_sub_child') {
            var id = val.value;
        } else {
            var id = $("#product_type").val();
        }

        if($("#product_type").val() == '')
        {
            $(".main-sub-parent-product-type-btn").attr('disabled',true)
            $(".secondary-sub-parent-product-type-btn").attr('disabled',true)
        }
        else{
            $(".main-sub-parent-product-type-btn").removeAttr('disabled')
            $(".secondary-sub-parent-product-type-btn").removeAttr('disabled')
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.get-product-type-data') }}',
            type: 'post',
            data: {
                id: id
            },
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {

                $(".data-loader").removeClass('loading');

                $("#product_package_type_id option[value='" + response.product_package_type_id + "']").attr(
                    "selected", "selected");


                if (type == 'secondary_sub_child') {
                    $('.secondary_sub_child').children().remove().end();
                    $('.secondary_sub_child').append('<option value="">Select sub parent product type</option>');
                    response.get_main_sub_child_data.forEach(element => {
                        $('.secondary_sub_child').append('<option value="' + element.id + '">' +
                            element.name + '</option>');
                    });
                } else {
                    $('.main_sub_child').children().remove().end();
                    $('.secondary_sub_child').children().remove().end();
                    $('.main_sub_child').append(
                        '<option value="">Select Main Sub Parent Product Type </option>');

                    $(".product-type-name").val(response.name);

                    if(!response.name){
                        $(".product-type-name").removeAttr("readonly");
                    }
                    else{
                        $(".product-type-name").attr("readonly", true);
                    }

                    response.get_main_sub_child_data.forEach(element => {
                        $('.main_sub_child').append('<option value="' + element.id + '">' + element
                            .name + '</option>');
                    });
                }

                console.log(response);
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#logo').change(function() {

        let reader = new FileReader();

        reader.onload = (e) => {

            $('#preview-image-before-upload').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);

    });

    $('#dataSheetForm').validate({
        rules: {
            link: {
                required: true,
            }
        },
        messages: {
            link: {
                required: "Please enter data sheet link"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#dataSheetForm')[0];
            var data = new FormData(form);
            dataSheetForm(data);
        }
    });



    function dataSheetForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.data-sheet-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    // getDataSheetData();
                    $('#data_sheet_ids').append('<option value="' + response.data.id + '">' + response.data
                        .link + '</option>');
                    $("#dataSheetForm")[0].reset();
                    $("#data-sheet").modal('hide');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }



    $('#productSizeWidthQtyForm').validate({
        rules: {
            size: {
                required: true,
            }
        },
        messages: {
            size: {
                required: "Please enter size"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productSizeWidthQtyForm')[0];
            var data = new FormData(form);
            productSizeWidthQtyForm(data);
        }
    });

    function productSizeWidthQtyForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.product-size-width-qty-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.product_size_width_id').append('<option value="' + response.data.id + '">' +
                        response.data.size + '</option>');
                    $("#product-size-width-qty").modal('hide');
                    $(".size").val('');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }


    $('#productSizeLengthQtyForm').validate({
        rules: {
            size: {
                required: true,
            }
        },
        messages: {
            size: {
                required: "Please enter size"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productSizeLengthQtyForm')[0];
            var data = new FormData(form);
            productSizeLengthQtyForm(data);
        }
    });

    function productSizeLengthQtyForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.product-size-length-qty-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.product_size_length_id').append('<option value="' + response.data.id + '">' +
                        response.data.size + '</option>');
                    $("#product-size-length-qty").modal('hide');
                    $(".size").val('');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }


    $('#productSizeThicknessQtyForm').validate({
        rules: {
            size: {
                required: true,
            }
        },
        messages: {
            size: {
                required: "Please enter size"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productSizeThicknessQtyForm')[0];
            var data = new FormData(form);
            productSizeThicknessQtyForm(data);
        }
    });

    function productSizeThicknessQtyForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.product-size-thickness-qty-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.product_size_thickness_id').append('<option value="' + response.data.id + '">' +
                        response.data.size + '</option>');
                    $("#product-size-thickness-qty").modal('hide');
                    $(".size").val('');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#specFastenerForm').validate({
        rules: {
            squer_foot: {
                required: true,
            }
        },
        messages: {
            squer_foot: {
                required: "Please enter squer foot"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#specFastenerForm')[0];
            var data = new FormData(form);
            specFastenerForm(data);
        }
    });

    function specFastenerForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.spec-fastener-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.specification_fastener_id').append('<option value="' + response.data.id + '">' +
                        response.data.squer_foot + '</option>');
                    $("#spec-fastener").modal('hide');
                    $(".squer_foot").val('');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#specFastenerScrewForm').validate({
        rules: {
            length_inch: {
                required: true,
            }
        },
        messages: {
            length_inch: {
                required: "Please enter length inch"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#specFastenerScrewForm')[0];
            var data = new FormData(form);
            specFastenerScrewForm(data);
        }
    });

    function specFastenerScrewForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.spec-fastener-screw-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.specification_fastener_screw_id').append('<option value="' + response.data.id +
                        '">' + response.data.length_inch + '</option>');
                    $("#spec-fastener-screw").modal('hide');
                    $(".length_inch").val('');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }


    $('#specFastenerNailForm').validate({
        rules: {
            length_inch: {
                required: true,
            }
        },
        messages: {
            length_inch: {
                required: "Please enter length inch"
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#specFastenerNailForm')[0];
            var data = new FormData(form);
            specFastenerNailForm(data);
        }
    });

    function specFastenerNailForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.spec-fastener-nail-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    $('.specification_fastener_nail_id').append('<option value="' + response.data.id +
                        '">' + response.data.length_inch + '</option>');
                    $("#spec-fastener-nail").modal('hide');
                    $(".length_inch").val('');
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }


    $('#productTypeForm').validate({
        rules: {
            product_type_name: {
                required: true,
            },
            product_package_type_id: {
                required: true,
            },
        },
        messages: {
            product_type_name: {
                required: "Please enter product type name "
            },
            product_package_type_id: {
                required: "Please select package product type "
            },

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productTypeForm')[0];
            var data = new FormData(form);
            productTypeForm(data);
        }
    });


    function productTypeForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.parent-product-type-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {

                console.log(response.data);
                if (response.status == true) {

                    $("#product_type_main_sub_child_id").val($("#main_sub_child").val());
                    $("#product_type_secondary_sub_child_id").val($("#secondary_sub_child").val());

                    $(".product_package_type_id_text").val($("#product_package_type_id option:selected").text());
                    $("#get_product_package_type_text").val($("#product_package_type_id option:selected").text());
                    $("#get_product_package_type_id").val($("#product_package_type_id").val());

                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)

                    if($('#product_type_name').attr('readonly') != 'readonly'){
                        $('#product_type').append('<option value="' + response.data.id +
                        '">' + response.data.product_type_name + '</option>');
                    }


                    let disData = response.data.product_type_name;
                    if(response.data.main_sub_child_name){
                        disData += ' / '+response.data.main_sub_child_name;
                    }

                    if(response.data.secondary_sub_child_name){
                        disData += ' / '+response.data.secondary_sub_child_name;
                    }

                    $(".dis-product-type").html('<div class="callout callout-success">'+disData+'</div>');
                    $("#product-type").modal('hide');
                    $("#productTypeForm")[0].reset();
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#productPackageTypeForm').validate({
        rules: {
            parent_product_package_type: {
                required: true,
            }
        },
        messages: {
            parent_product_package_type: {
                required: "Please enter parent product type name "
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#productPackageTypeForm')[0];
            var data = new FormData(form);
            productPackageTypeForm(data);
        }
    });


    function productPackageTypeForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.parent-product-package-type-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {

                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)

                    $('#product_package_type_id').append('<option value="' + response.data.id +
                        '">' + response.data.parent_product_package_type + '</option>');

                    $("#product-package-type").modal('hide');
                    $("#productPackageTypeForm")[0].reset();
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#mainSubParentProductTypeForm').validate({
        rules: {
            main_sub_parent_product_type: {
                required: true,
            }
        },
        messages: {
            main_sub_parent_product_type: {
                required: "Please enter main sub parent product type "
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#mainSubParentProductTypeForm')[0];
            var data = new FormData(form);
            mainSubParentProductTypeForm(data);
        }
    });


    function mainSubParentProductTypeForm(form) {

        var product_type = $("#product_type").val();
        form.append('parent_id',product_type);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.main-sub-parent-product-type-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {

                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)

                    $('#main_sub_child').append('<option value="' + response.data.id +
                        '">' + response.data.main_sub_parent_product_type + '</option>');

                    $("#main-sub-parent-product-type").modal('hide');
                    $("#productTypeForm")[0].reset();
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#secondarySubParentProductTypeForm').validate({
        rules: {
            secondary_sub_parent_product_type: {
                required: true,
            }
        },
        messages: {
            secondary_sub_parent_product_type: {
                required: "Please enter secondary sub parent product type "
            }

        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {
            var form = $('#secondarySubParentProductTypeForm')[0];
            var data = new FormData(form);
            secondarySubParentProductTypeForm(data);
        }
    });


    function secondarySubParentProductTypeForm(form) {

        var main_sub_child = $("#main_sub_child").val();
        if(main_sub_child == '')
        {
            toastr.error('{{ config('app.name') }}', 'Please select first main sub parent product type');
            return false;
        }

        form.append('main_sub_child',main_sub_child);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.secondary-sub-parent-product-type-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {

                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)

                    $('#secondary_sub_child').append('<option value="' + response.data.id +
                        '">' + response.data.secondary_sub_parent_product_type + '</option>');

                    $("#secondary-sub-parent-product-type").modal('hide');
                    $("#secondarySubParentProductTypeForm")[0].reset();
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    $('#parentProductForm').validate({
        rules: {
            manufacture_id: {
                required: true,
            },
            name: {
                required: true,
            },
            product_area: {
                required: true,
            },
            product_type: {
                required: true,
            },
            'data_sheet_ids[]': {
                required: true,
            }
        },
        messages: {
            manufacture_id: {
                required: "Please select product manufacturer"
            },
            name: {
                required: "Please enter product name"
            },
            product_area: {
                required: "Please select product area"
            },
            product_type: {
                required: "Please select product type"
            },
            'data_sheet_ids[]': {
                required: "Please data sheet ids"
            },
        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function(form) {

            var isValid = true;

            $(".allfield").each(function() {
                if($(this).val() == "" && $(this).val().length < 1) {
                    $(this).addClass('is-invalid');
                    isValid = false;
                } else {
                    $(this).removeClass('is-invalid');
                }
            });

            $(".is-invalid:first").focus();

            if(isValid) {
                var form = $('#parentProductForm')[0];
                var data = new FormData(form);
                parentProductForm(data);
            }

        }
    });


    function parentProductForm(form) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.parent-product-create') }}',
            type: 'post',
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {

                if (response.status == true) {
                    $(".data-loader").removeClass('loading');
                    toastr.success('{{ config('app.name') }}', response.message)
                    setTimeout(() => {
                            window.location.href = '{{ route('admin.parent-product-list') }}';
                        }, 1000);
                } else {
                    $(".data-loader").removeClass('loading');
                    toastr.error('{{ config('app.name') }}', response.message)
                }
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    function getProductPrice(index){

        let product_size_width_id;
        let product_size_length_id;

        if($("#product_size_width_id"+index).val()){
            product_size_width_id = $("#product_size_width_id"+index+" option:selected").text();
            console.log("product_size_width_id : ",product_size_width_id);
        }

        if($("#product_size_length_id"+index).val()){
            product_size_length_id = $("#product_size_length_id"+index+" option:selected").text();
            console.log(" product_size_length_id : ",product_size_length_id);
        }

        var product_price = $("#product_price"+index).val();
        console.log(" product_price: ",product_price);

        if($("#product_size_width_id"+index).val() != '' && $("#product_size_length_id"+index).val() != '' && product_price != '')
        {
            var multiplication = product_size_width_id*product_size_length_id;
            console.log("multiplication : ", multiplication);
            $("#product_price_per_sq"+index).val((product_price / multiplication).toFixed(2));
        }
    }

    function getSupplierDetails(id){

        var supplier_id = $("#select_supplier"+id).val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.get-supplier-data') }}',
            type: 'post',
            data: {index : id, supplier_id: supplier_id},
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                $(".data-loader").removeClass('loading');
                $("#price_per_sq_id"+id).text(response.product_price_per_sq);
                $("#price_id"+id).text(response.product_price);
            },
            error: function(data) {
                $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    }

    function activeDeactive(status, id){

        var lable = status == 0 ? 'active' : 'inactive';
        Swal.fire({
            title: 'Are you sure?',
            text: "You wont to be able to "+ lable +" this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#5B73E8',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, change it!'
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                    url:"{{route('admin.parent-product-enable-disable')}}",
                    type: 'post',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {status: status, id:id},
                    success:function(msg){
                        console.log(msg);
                        if(msg.status == true){
                            Swal.fire("Change!",  msg.message, "success");
                            window.LaravelDataTables["parent-product"].ajax.reload();
                        }else{
                            Swal.fire("Warning!", msg.message, "warning");
                        }
                    },
                    error:function(){
                        Swal.fire("Error!", 'Error in delete Record', "error");
                    }
                    });
                }
            })
        }

        function deleteParentProduct(id){
            
            Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#5B73E8',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                    url:"{{route('admin.parent-product-delete')}}",
                    type: 'delete',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {_method: 'Delete', id:id},
                    success:function(msg){
                        console.log(msg);
                        if(msg.status == true){
                            Swal.fire("Deleted!",  msg.message, "success");
                            window.LaravelDataTables["parent-product"].ajax.reload();
                        }else{
                            Swal.fire("Warning!", msg.message, "warning");
                        }
                    },
                    error:function(){
                        Swal.fire("Error!", 'Error in delete Record', "error");
                    }
                    });
                }
            })
        };

        $("#product_type").on('change',function(){
            $(".dis-product-type").html('');
        });


</script>
