<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('trowels', function (Blueprint $table) {
            $table->unsignedBigInteger('child_product_supplier_id')->nullable()->after('child_product_id');
            $table->foreign('child_product_supplier_id')->references('id')->on('child_product_suppliers')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('is_default')->default(0)->after('coverage');

        });
    }

    public function down(): void
    {
        Schema::table('trowels', function (Blueprint $table) {
            $table->dropColumn('child_product_supplier_id');
            $table->dropColumn('is_default');
        });
    }
};
