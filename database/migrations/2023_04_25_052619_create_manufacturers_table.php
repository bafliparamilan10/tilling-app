<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacturers', function (Blueprint $table) {
            $table->id();
            $table->string('logo', 255)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('ticker', 255)->nullable();
            $table->string('suppliers_ids', 255)->nullable();
            $table->string('website', 255)->nullable();
            $table->boolean('status')->default(1)->comment('1 = active, 0 = deactive');
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacturers');
    }
};
