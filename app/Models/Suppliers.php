<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Auth;

class Suppliers extends Model
{
    use HasFactory,SoftDeletes;

    protected $guarded = [];

    protected $appends = ['is_favorite'];

    protected function logo(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value ? asset('supplier/'.$value) : asset('/no_image.jpg'),
        );
    }

    public function getIsFavoriteAttribute()
    {
        return SupplierFavorite::where('supplier_id',$this->id)->where('user_id',Auth::user()->id)->exists();
    }
}
