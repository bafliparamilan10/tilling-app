<?php

namespace App\Repositories\admin;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Interfaces\admin\UserRepositoryInterface;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use App\Models\City;
use App\Models\State;
use App\Models\Subscription;
use Str;
use App\Mail\GeneratePasswordMail;
use App\Mail\UpdateUserMail;
use Illuminate\Support\Facades\Hash;

//use Your Model

/**
 * Class UserRepository.
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        //return YourModel::class;
    }

    public function deleteUser($id)
    {
        try {
            DB::beginTransaction();

            User::find($id)->delete();

            DB::commit();
            return ['status' => true, 'message' =>  'user delete successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error(' user Delete : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function createUser($data)
    {
        try {
            DB::beginTransaction();
            $dob = Carbon::parse($data['dob']);
            $user = [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'user_type' => $data['user_type'],
                'city_id' => $data['city_id'],
                'state_id' => $data['state_id'],
                'date_of_birth' => $dob->format('Y-m-d'),
                'gender' => $data['gender'],
                'status' => $data['status'],
            ];
            if ($data['type'] == 'edit') {
                $last_id = User::where('id', $data['id'])->update($user);

                $city = City::where('id', $data['city_id'])->first();
                $state = State::where('id', $data['state_id'])->first();
                $mailData = [
                    'name' => $data["first_name"].' '.$data["last_name"],
                    'email' => $data['email'],
                    'user_type' => $data['user_type'],
                    'city' => $city->name,
                    'state' => $state->name,
                    'date_of_birth' => $dob->format('Y-m-d'),
                    'gender' => $data['gender'],
                    'status' => $data['status'] == '1' ? 'Active' : "Inactive",
                ];
                Mail::to($data['email'])->send(new UpdateUserMail($mailData));
                DB::commit();
                return ['status' => true, 'message' =>  'User updated successfully'];
            } else {
                $mailPassword = Str::random(8);
                $password =  Hash::make($mailPassword);
                $user['password'] = $password;
                $last_id = User::create($user);

                $mailData = [
                    'name' => $data["first_name"].' '.$data["last_name"],
                    'email' => $data['email'],
                    'password' => $mailPassword,
                ];

                Mail::to($data['email'])->send(new GeneratePasswordMail($mailData));
                DB::commit();
                return ['status' => true, 'message' =>  'User created successfully'];
            }
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error(' user create : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function getCity($id)
    {
        $city =  City::where(['state_id' => $id])->get();
        echo '<option value="">Select City</option>';
        foreach($city as $cityValue)
        {
            echo '<option value="'.$cityValue->id.'">'.$cityValue->name.'</option>';
        }
    }

    public function userEnableDisable($data)
    {
        try {
            DB::beginTransaction();

            $status = $data['status'] ? 0 : 1;
            User::where('id', $data['id'])->update(['status' => $status]);

            DB::commit();
            return ['status' => true, 'message' =>  'User status updated successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error(' user Delete : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }
}
