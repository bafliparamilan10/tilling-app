@extends('admin.layouts.master')
@push('customCSS')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush
@section('page_title', 'Manufacture Management')
@section('content_header')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Manufacture Management</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.supplier-list')}}">Supplier Management</a></li>
                    <li class="breadcrumb-item active">Supplier Details</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="col-md-11 mx-auto">
            <!-- general form elements -->
            <div class="card card-primary">

                <div class="row">
                    <div class="col-md-6">
                        <div class="card-body box-profile">
                            <ul class="list-group list-group-unbordered mb-3">
                              <li class="list-group-item">
                                <b>Image / Logo : </b> <span class="ml-3"> <img src="{{ $manufactureData->logo }}"
                                    style="height: 100px; width:150px;"></span>
                              </li>
                              <li class="list-group-item">
                                <b>Manufacturer : </b> <span class="ml-3">{{$manufactureData->name }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Website : </b> <span class="ml-3">{{ $manufactureData->website }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Ticker : </b> <span class="ml-3">{{ $manufactureData->ticker }}</span>
                              </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-body box-profile">
                            <ul class="list-group list-group-unbordered mb-3">
                              <li class="list-group-item">
                                <b>Total # of Suppliers : </b> <span class="ml-3">{{ $totalSuppliers }}</span>
                              </li>
                              <li class="list-group-item">
                                <b>Total # of Methods : </b> <span class="ml-3">0</span>
                              </li>
                              <li class="list-group-item">
                                  <b>Total # of Parent Products : </b> <span class="ml-3">0</span>
                              </li>
                              <li class="list-group-item">
                                <b>Status : </b>
                                @if($manufactureData->status)
                                <span class="ml-3 text-success">Active</span>
                                @else
                                <span class="ml-3 text-danger">Inactive</span>
                                @endif
                              </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-5 text-center">
                <a href="javascript:history.back()" class="btn btn-secondary">Back</a>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
@push('customJs')
<!-- jquery-validation -->
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
@include('admin.customJs.user')
@endpush
