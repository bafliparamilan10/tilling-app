<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class UserSignupRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required',
            'email',
                Rule::unique(table:'users')->whereNull(column:'deleted_at')],
            'user_type' => 'required',
            'password' => 'required|between:6,25',
            'city_id' => 'required',
            'state_id' => 'required',
            // 'date_of_birth' => 'required',
            'gender' => 'required',
            'status' => 'required',
        ];
    }
}
