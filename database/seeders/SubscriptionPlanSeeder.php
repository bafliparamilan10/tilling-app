<?php

namespace Database\Seeders;

use App\Models\SubsciptionPlan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('subsciption_plans')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        SubsciptionPlan::insert([
            [
                'name' => 'FREE',
                'description' => 'Unlimited Methods, Method cost by the Sq. ft., Favorite Methods, MyWay Experience, Save More!',
                'type' => 'free',
                'price' => '00',
                'button_description' => 'FREE 365 Days a Year',
            ],
            [
                'name' => 'PRO × 10',
                'description' => 'Unlimited Methods, Method cost by the Sq. ft., Favorite Methods, Price Methods, MyWay Experience, Save More!',
                'type' => 'pro',
                'price' => '29',
                'button_description' => '$29/mo. After 14 FREE Days',
            ]
        ]
    );
    }
}
