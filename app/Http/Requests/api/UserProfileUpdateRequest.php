<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserProfileUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $id = Auth::user()->id;

        return [
            
            // 'email' => [
            //     'required',
            //     'email',Rule::unique(table:'users')->whereNull(column:'deleted_at')->where('id',$id)
            // ],
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => "required|email|unique:users,email,{$id},id,deleted_at,NULL",
            'city_id' => 'required',
            'state_id' => 'required',
            // 'date_of_birth' => 'required',
        ];
    }
}
