<?php

namespace App\Repositories\api;

use App\Interfaces\api\SupplierRepositoryInterface;
use App\Models\SupplierFavorite;
use App\Models\Suppliers;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;

class SupplierRepository implements SupplierRepositoryInterface
{
    public function model()
    {
        //return YourModel::class;
    }

    public function supplierList($data)
    {
        try {
            DB::beginTransaction();

            $suppliers = Suppliers::where('status',1)->get();
            $auth_id = Auth::user()->id;

            // $data = [];
            // foreach ($suppliers as $key => $value) {

            //     $supplier_fav = SupplierFavorite::where('supplier_id',$value['id'])->where('user_id',$auth_id)->first();

            //     if($supplier_fav){
            //         $data[$key] = $value;
            //         $data[$key]['is_favorite'] = 1;
            //     }else{
            //         $data[$key] = $value;
            //         $data[$key]['is_favorite'] = 0;
            //     }
                
            // }
            
            DB::commit();
            return response(['message' => 'Suppllier list get successfully','data' => $suppliers] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Supplier List : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function supplierFavUnfav($data)
    {
        try {
            DB::beginTransaction();

            $user_id = Auth::user()->id;
            $is_favorite = $data['is_favorite'];
            $suppliers = explode(',',$data['supplier_ids']);
            SupplierFavorite::where('user_id',$user_id)->delete();

            foreach($suppliers as $key => $supplier_id)
            {                
                $supplier_fav = SupplierFavorite::where('supplier_id',$supplier_id)->where('user_id',$user_id)->first();
                
                if(empty($supplier_fav)){
                    $supplier_fav = new SupplierFavorite();
                    $supplier_fav->supplier_id = $supplier_id;
                    $supplier_fav->user_id = $user_id;
                    $supplier_fav->save();
                }
            }
            DB::commit();
            return response(['message' => 'Supplier Favorite successfully','data' => new stdClass()] ,200);

            // if($is_favorite == 0){
                
            //     foreach($suppliers as $key => $supplier_id){
                    
            //         $supplier_fav = SupplierFavorite::where('supplier_id',$supplier_id)
            //                         ->where('user_id',$user_id)
            //                         ->delete();
            //     }
            //     DB::commit();
            //     return response(['message' => 'Supplier Unfavorite successfully','data' => new stdClass()] ,200);

            // }else{
            //     foreach($suppliers as $key => $supplier_id){
                    
            //         $supplier_fav = SupplierFavorite::where('supplier_id',$supplier_id)->where('user_id',$user_id)->first();

            //         if(empty($supplier_fav)){
            //             $supplier_fav = new SupplierFavorite();
            //             $supplier_fav->supplier_id = $supplier_id;
            //             $supplier_fav->user_id = $user_id;
            //             $supplier_fav->save();
            //         }
            //     }
            //     DB::commit();
            //     return response(['message' => 'Supplier Favorite successfully','data' => new stdClass()] ,200);
            // }
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Suppllier FavUnfav : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function supplierFavList($data)
    {
        try {
            DB::beginTransaction();

            $user_id = Auth::user()->id;

            $supplier_fav = SupplierFavorite::with('getSupplier')->where('user_id',$user_id)->get();

            DB::commit();
            return response(['message' => 'Favorite supplier get successfully','data' => $supplier_fav] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Suppllier FavList : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    
}
