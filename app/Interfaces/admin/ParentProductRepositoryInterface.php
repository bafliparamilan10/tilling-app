<?php

namespace App\Interfaces\admin;

interface ParentProductRepositoryInterface
{
    public function addDataSheet(array $data);
    public function addProductSizeWidthQty(array $data);
    public function addProductSizeLengthQty(array $data);
    public function addProductSizeThicknessQty(array $data);
    public function addSpecFastener(array $data);
    public function addSpecFastenerScrew(array $data);
    public function addSpecFastenerNail(array $data);
    public function parentProductTypeCreate(array $data);
    public function parentProductPackageTypeCreate(array $data);
    public function mainSubParentProductTypeCreate(array $data);
    public function secondarySubParentProductTypeCreate(array $data);
    public function parentProductCreate(array $data);
    public function parentProductEnableDisable(array $data);
    public function deleteParentProduct($id);
    public function updateDefaultDataSheet(array $data);
}
