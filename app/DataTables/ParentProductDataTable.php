<?php

namespace App\DataTables;

use App\Models\ParentProduct;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ParentProductDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
        ->addIndexColumn()
        ->editColumn('manufacture_id', function ($data) {
            return isset($data->getManufactureData) ? $data->getManufactureData->name : '';
        })
        ->editColumn('suppliers', function ($data) {

            $getSupplier = '<select id="select_supplier'.$data->id.'" class="form-control" onchange="getSupplierDetails('.$data->id.');">';
            foreach ($data->getSupplierData as $key => $getSupplierDataValue) {
                $getSupplier .= '<option value="'.$getSupplierDataValue->id.'">'.$getSupplierDataValue->getSupplierDetails->name.'</option>';
            }
            $getSupplier .= '</select>';
            return $getSupplier;
        })
        ->editColumn('supplier_id', function ($data) {
            return isset($data->getSupplierData) ? sizeof($data->getSupplierData) : '';
        })
        ->editColumn('price_per_sq', function ($data) {

            $pricePerData = isset($data->getSupplierData) ? $data->getSupplierData[0]->product_price_per_sq : '-';
            $price_per_sq = '<div>$<span id="price_per_sq_id'.$data->id.'">'.$pricePerData.'</span></div>';
             return $price_per_sq;
            })
        ->editColumn('price', function ($data) {
            $priceData = isset($data->getSupplierData) ? $data->getSupplierData[0]->product_price : '';
            return '<div>$<span id="price_id'.$data->id.'">'.$priceData.'</span></div>';
        })
        ->editColumn('status', function ($data) {
            $status = "";
            if ($data->status == 1) {
                $status =  '<button type="button" class="btn btn-outline-success btn-block" onclick="activeDeactive('.$data->status.','.$data->id.')">Active</button>';
            } else {
                $status =  '<button type="button" class="btn btn-outline-danger btn-block" onclick="activeDeactive('.$data->status.','.$data->id.')">InActive</button>';
            }
            return $status;
        })
        ->addColumn('action', function ($data) {
            $action = "";
            $action .= '<a href="'.route('admin.parent-product-view', ['id' => Crypt::encrypt($data->id)]).'" class="btn btn-outline-primary mr-1 mb-1"><i class="fa fa-eye" title="view"></i></a>';
            $action .= '<a href="'.route('admin.parent-product-edit', ['id' => Crypt::encrypt($data->id)]).'" class="btn btn-outline-success mr-1 mb-1"><i class="fa fa-edit" title="Edit"></i></a>';
            $action .= '<a  onclick="deleteParentProduct('.$data->id.')" class="btn btn-outline-danger mr-1"><i class="fa fa-trash" title="Delete"></i></a>';
            return $action;
        })->rawColumns(['action', 'status', 'suppliers', 'price_per_sq','price']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ParentProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ParentProduct $model): QueryBuilder
    {
        return $model->with(['getManufactureData','getSupplierData','getSupplierData.getSupplierDetails'])->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                ->setTableId('parent-product')
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->dom('Blfrtip')
                ->orderBy(1)
                ->buttons([
                ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')
            ->title('Sr No.')->searchable(false)->orderable(false),
            Column::make('id')->searchable(false)->exportable(true)->hidden(true),
            Column::make('name')->title('Parent Product Name'),
            Column::make('manufacture_id')->title('Manufacturers')->searchable(false)->orderable(false),
            Column::make('suppliers')->title('Suppliers')->searchable(false)->orderable(false),
            Column::make('supplier_id')->title('Total no. of Suppliers')->searchable(false)->orderable(false),
            Column::make('price')->title('Price')->searchable(false)->orderable(false),
            Column::make('price_per_sq')->title('Price Per SQ. FT.')->searchable(false)->orderable(false),
            Column::make('status')->title('Status')->searchable(false)->orderable(false),
            Column::computed('action')->title('Actions')->exportable(false)->printable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'ParentProduct_' . date('YmdHis');
    }
}
