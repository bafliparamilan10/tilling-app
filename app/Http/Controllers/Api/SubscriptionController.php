<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\api\SubscriptionRepositoryInterface;
use Illuminate\Http\Request;
use Laravel\Passport\HasApiTokens;

class SubscriptionController extends Controller
{
    use HasApiTokens;

    private SubscriptionRepositoryInterface $supplierRepository;

    public function __construct(SubscriptionRepositoryInterface $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }
    
    public function subscriptionList(Request $request)
    {
        $response = $this->supplierRepository->subscriptionList($request->all());
        
        return $response;
    }

    public function subscriptionPurchase(Request $request)
    {
        $response = $this->supplierRepository->subscriptionPurchase($request->all());
        
        return $response;
    }

    public function subscriptionCancel(Request $request)
    {
        $response = $this->supplierRepository->subscriptionCancel($request->all());
        
        return $response;
    }

    public function subscriptionReceipts(Request $request)
    {
        $response = $this->supplierRepository->subscriptionReceipts($request->all());
        
        return $response;
    }
}
