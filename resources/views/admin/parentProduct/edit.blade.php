@extends('admin.layouts.master')
@push('customCSS')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush
@section('page_title', 'Update Parent Product')
@section('content_header')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Update Parent Product</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.parent-product-list')}}">Parent Product Management</a></li>
                    <li class="breadcrumb-item active">Update Parent Product</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-11 mx-auto">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"> Update Parent Product Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="parentProductForm" name="parentProductForm" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $parentProduct->id }}">
                        <input type="hidden" name="product_type_main_sub_child_id" id="product_type_main_sub_child_id" value="{{ $parentProduct->product_type_main_sub_child_id }}">
                        <input type="hidden" name="product_type_secondary_sub_child_id" id="product_type_secondary_sub_child_id" value="{{ $parentProduct->product_type_secondary_sub_child_id }}">
                        <input type="hidden" name="get_product_package_type_id" id="get_product_package_type_id" value="{{ $parentProduct->product_package_type_id }}">
                        <input type="hidden" name="get_product_package_type_text" id="get_product_package_type_text" value="{{ isset($parentProduct->getParentProdcutPackageTypeData->name) ?? '' }}">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <img id="preview-image-before-upload" src="{{ $parentProduct->logo }}" style="height: 150px; width:250px;">
                                </div>
                                {{-- <div class="form-group col-md-6">
                                    <label for="username">Image (optional)</label>
                                    <input type="file" class="form-control" id="logo" name="logo" accept="image/png, image/jpeg, image/jpg">
                                </div> --}}

                                <div class="form-group col-md-6">
                                    <label for="exampleInputFile">Image (optional)</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                          <input type="file" class="custom-file-input" id="logo" name="logo" accept="image/png, image/jpeg, image/jpg">
                                          <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label for="email">Product Manufacturer</label>
                                    <select id="manufacture_id"  name="manufacture_id" class="form-control">
                                      <option value="">Select Product Manufacturer</option>
                                      @foreach ($manufacturer as $manufacturerValue)
                                        <option @selected($parentProduct->manufacture_id == $manufacturerValue->id)  value="{{ $manufacturerValue->id }}">{{ $manufacturerValue->name }}</option>
                                      @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="username">Product Name </label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter product name" value="{{ $parentProduct->name }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Product Area ( Substrate, Underlayment )</label>
                                    <select id="product_area"  name="product_area" class="form-control">
                                      <option value="">Select Product Area</option>
                                      <option @selected($parentProduct->product_area == 'Substrate')  value="Substrate">Substrate</option>
                                      <option @selected($parentProduct->product_area == 'Underlayment') value="Underlayment">Underlayment</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Product ANSI ( Optional )</label>
                                    <select id="ansi_id"  name="ansi_id" class="form-control">
                                      <option value="">Select Product ANSI</option>
                                      @foreach ($ansi as $ansiVlaue)
                                        <option @selected($parentProduct->ansi_id == $ansiVlaue->id) value="{{ $ansiVlaue->id }}">{{ $ansiVlaue->name }}</option>
                                      @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6 d-flex">
                                    <div class="w-100">
                                        <label for="email">Product Type</label>
                                        <select id="product_type"  name="product_type" class="form-control">
                                        <option value="">Select Product Type</option>
                                        @foreach ($productType as $productTypeValue)
                                            <option @selected($parentProduct->product_type_id == $productTypeValue->id) value="{{ $productTypeValue->id }}">{{ $productTypeValue->name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group plus-btn-div">
                                        <button type="button" class="btn btn-primary" onclick="managePopup('product-type'); productTypeManage('','main_sub_child');"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>

                                <div class="form-group col-md-6 d-flex">
                                    <div class="w-100">
                                        <label for="email">Data Sheet ( URL )</label>
                                        <div class="new-c-arrow">
                                        <select id="data_sheet_ids" name="data_sheet_ids[]" class="form-control select2bs4" multiple>
                                        @foreach ($dataSheets as $dataSheetsValue)
                                            <option  @selected(in_array($dataSheetsValue->id, explode(",",$parentProduct->data_sheet_ids)))  value="{{ $dataSheetsValue->id }}">{{ $dataSheetsValue->link }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    </div>
                                    <div class="form-group plus-btn-div">
                                        <button type="button" class="btn btn-primary" onclick="managePopup('data-sheet');"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>



                                <div class="form-group col-md-6 dis-product-type">
                                    <div class="callout callout-success">
                                        {{ $parentProduct->getProductTypeDetails->name  }} / {{ isset($parentProduct->getProductTypeMainSubChildDetails->name) ? $parentProduct->getProductTypeMainSubChildDetails->name : ''}} / {{ isset($parentProduct->getProductTypeSecondarySubChildDetails->name) ?  $parentProduct->getProductTypeSecondarySubChildDetails->name : '' }}
                                    </div>
                                </div>

                            </div>

                            <div class="field_wrapper form-horizontal">

                                @foreach ($parentProduct->getSupplierData as $getSupplierDataValue)

                                    <input type="hidden" id="parent_product_supplier_id" name="parent_product_supplier_id[]" value="{{ $getSupplierDataValue->id }}">

                                    <div class="row multiple-form-boarder mt-3 p-3 supplier-div">

                                        <div class="form-group col-md-6">
                                            <label for="email">Supplier Name</label>
                                            <select id="supplier_ids1"  name="supplier_ids[]" class="form-control allfield">
                                            <option value="">Select Supplier Name</option>
                                            @foreach ($suppliers as $suppliersValue)
                                                <option @selected($getSupplierDataValue->supplier_id == $suppliersValue->id) value="{{ $suppliersValue->id }}">{{ $suppliersValue->name }}</option>
                                            @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="username">Supplier Product Link (url) </label>
                                            <input type="url" class="form-control allfield" id="product_link1" name="product_link[]" placeholder="Enter product link" value="{{ $getSupplierDataValue->product_links }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="username">Supplier Product MDL (#) </label>
                                            <input type="text" class="form-control allfield" id="product_mdl1" name="product_mdl[]" placeholder="Enter product MDL" value="{{ $getSupplierDataValue->product_mdl }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="">Product Package Type</label>
                                            <input type="text" class="form-control product_package_type_id_text" id="product_package_type_id_text1" name="product_package_type_id_text" value="{{ isset($parentProduct->getParentProdcutPackageTypeData->name) ?? '' }}" readonly>
                                        </div>

                                        <div class="form-group col-md-6 d-flex">
                                            <div class="w-100">
                                                <label for="">Product Size (width/ft.) Quantity</label>
                                                <select id="product_size_width_id1"  name="product_size_width_id[]" class="form-control product_size_width_id allfield" onchange="getProductPrice(1)">
                                                <option value="">Select Product Size (width/ft.) Quantity</option>
                                                @foreach ($productSizeWidthQty as $productSizeWidthQtyValue)
                                                    <option @selected($getSupplierDataValue->product_size_width_qty_id == $productSizeWidthQtyValue->id) value="{{ $productSizeWidthQtyValue->id }}">{{ $productSizeWidthQtyValue->size }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group plus-btn-div">
                                                <button type="button" class="btn btn-primary" onclick="managePopup('product-size-width-qty');"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6 d-flex">
                                            <div class="w-100">
                                                <label for="">Product Size (length/ft.) Quantity</label>
                                                <select id="product_size_length_id1"  name="product_size_length_id[]" class="form-control product_size_length_id allfield" onchange="getProductPrice(1)">
                                                <option value="">Select Product Size (length/ft.) Quantity</option>
                                                @foreach ($productSizeLengthQty as $productSizeLengthQtyValue)
                                                    <option @selected($getSupplierDataValue->product_size_length_qty_id == $productSizeLengthQtyValue->id) value="{{ $productSizeLengthQtyValue->id }}">{{ $productSizeLengthQtyValue->size }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group plus-btn-div">
                                                <button type="button" class="btn btn-primary" onclick="managePopup('product-size-length-qty');"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6 d-flex">
                                            <div class="w-100">
                                                <label for="">Product Size (thickness/inch.)</label>
                                                <select id="product_size_thickness_id1"  name="product_size_thickness_id[]" class="form-control product_size_thickness_id allfield">
                                                <option value="">Select Product Size (thickness/inch.)</option>
                                                @foreach ($productSizeThickness as $productSizeThicknessValue)
                                                    <option @selected($getSupplierDataValue->product_size_thickness_id == $productSizeThicknessValue->id) value="{{ $productSizeThicknessValue->id }}">{{ $productSizeThicknessValue->size }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group plus-btn-div">
                                                <button type="button" class="btn btn-primary" onclick="managePopup('product-size-thickness-qty');"><i class="fa fa-plus"></i></button>
                                            </div>

                                        </div>

                                        <div class="col-12">
                                            <h4 class="m-4">Specifications (product) :</h4>
                                        </div>

                                        <div class="form-group col-md-6 d-flex">
                                            <div class="w-100">
                                                <label for="">Spec. - # Fasteners (per sq. ft.) req.</label>
                                                <select id="specification_fastener_id1"  name="specification_fastener_id[]" class="form-control specification_fastener_id allfield">
                                                <option value="">Select Fasteners (per sq. ft.)</option>
                                                @foreach ($specificationFasteners as $specificationFastenersValue)
                                                    <option @selected($getSupplierDataValue->specification_fastners_id == $specificationFastenersValue->id)  value="{{ $specificationFastenersValue->id }}">{{ $specificationFastenersValue->squer_foot }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group plus-btn-div">
                                                <button type="button" class="btn btn-primary" onclick="managePopup('spec-fastener');"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>


                                        <div class="form-group col-md-6 d-flex">
                                            <div class="w-100">
                                                <label for="">Spec. - # Fasteners Screw (length/inch.) req.</label>
                                                <select id="specification_fastener_screw_id1"  name="specification_fastener_screw_id[]" class="form-control specification_fastener_screw_id allfield">
                                                <option value="">Select Fasteners Screw (length/inch.)</option>
                                                @foreach ($specificationFastenersScrew as $specificationFastenersScrewValue)
                                                    <option @selected($getSupplierDataValue->specification_fastners_screw_id == $specificationFastenersScrewValue->id) value="{{ $specificationFastenersScrewValue->id }}">{{ $specificationFastenersScrewValue->length_inch }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group plus-btn-div">
                                                <button type="button" class="btn btn-primary" onclick="managePopup('spec-fastener-screw');"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>


                                        <div class="form-group col-md-6 d-flex">
                                            <div class="w-100">
                                                <label for="">Spec. - # Fasteners Nail (length/inch.) req.</label>
                                                <select id="specification_fastener_nail_id1"  name="specification_fastener_nail_id[]" class="form-control specification_fastener_nail_id allfield">
                                                <option value="">Select Fasteners Nail (length/inch.)</option>
                                                @foreach ($specificationFastenersNail as $specificationFastenersNailValue)
                                                    <option @selected($getSupplierDataValue->specification_fastners_nails_id == $specificationFastenersNailValue->id) value="{{ $specificationFastenersNailValue->id }}">{{ $specificationFastenersNailValue->length_inch }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group plus-btn-div">
                                                <button type="button" class="btn btn-primary" onclick="managePopup('spec-fastener-nail');"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>


                                        <div class="col-12 m-3">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="username">Product Price ($) </label>
                                            <input type="number" class="form-control allfield" id="product_price1" name="product_price[]" placeholder="Enter product price" min="0" onkeyup="getProductPrice(1);" value="{{ $getSupplierDataValue->product_price }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="username">Product Price (Per Sq. Ft. in $) </label>
                                            <input type="number" class="form-control allfield" id="product_price_per_sq1" name="product_price_per_sq[]" placeholder="Enter product price" min="0" readonly value="{{ $getSupplierDataValue->product_price_per_sq }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="">Status</label>
                                            <select id="status1"  name="status[]" class="form-control allfield">
                                            <option value="">Select Status</option>
                                            <option @selected($getSupplierDataValue->status == 1) value="1">Active</option>
                                            <option @selected($getSupplierDataValue->status == 0) value="0">InActive</option>
                                            </select>
                                        </div>
                                    </div>

                                @endforeach

                            </div>

                            <button type="button"  onclick="addSuppliers();" class="btn btn-primary m-2">Add New Supplier</button>

                        </div>

                        <div class="card-footer text-center">
                            <a href="javascript:history.back()" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add New Product Type</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">

            <div class="row">

                <div class="form-group col-md-12">
                    <label for="username">Product Price ($) </label>
                    <input type="text" class="form-control" id="product_price" name="product_price" placeholder="Enter product price">
                </div>

            </div>

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Add</button>
        </div>
      </div>

    </div>

</div>

<div class="modal fade" id="data-sheet" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="dataSheetForm" name="dataSheetForm">
            <div class="modal-header">
            <h4 class="modal-title">Data Sheet</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Data Sheet ( URL ) </label>
                        <input type="text" class="form-control" id="link" name="link" placeholder="Enter data sheet url">
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>

<div class="modal fade" id="product-type" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="productTypeForm" name="productTypeForm" method="POST">
            <div class="modal-header">
            <h4 class="modal-title">Add New Product Type</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Parent Product Type Name </label><br><span> (ex.Backer Board,Membranes,etc)</span>
                        <input type="text" class="form-control product-type-name" id="product_type_name" name="product_type_name" placeholder="Enter name">
                    </div>

                    <div class="form-group col-md-12 d-flex">
                        <div class="w-100">
                            <label for="">Main Sub Parent Product Type (Optional)</label><br><span>(ex.Cement Backer,Foam Backers,etc)</span>
                            <select id="main_sub_child"  name="main_sub_child" class="form-control main_sub_child" onchange="productTypeManage(this,'secondary_sub_child');">
                            <option value="">Main Sub Parent Product Type</option>
                            </select>
                        </div>
                        <div class="form-group plus-btn-div">
                            <button type="button" class="btn btn-primary main-sub-parent-product-type-btn" onclick="managePopup('main-sub-parent-product-type');"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>


                    <div class="form-group col-md-12 d-flex">
                        <div class="w-100">
                            <label for="">Secondary Sub Parent Product Type (Optional)</label><br><span>(ex.Thin set, Andhesive,etc)</span>
                            <select id="secondary_sub_child"  name="secondary_sub_child" class="form-control secondary_sub_child">
                            <option value="">Secondary Sub Parent Product Type</option>
                            </select>
                        </div>
                        <div class="form-group plus-btn-div">
                            <button type="button" class="btn btn-primary secondary-sub-parent-product-type-btn" onclick="managePopup('secondary-sub-parent-product-type');"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                    <div class="form-group col-md-12 d-flex">
                        <div class="w-100">
                            <label for="">Parent Product Package Type</label><br><span>(ex.Panels,etc)</span>
                            <select id="product_package_type_id"  name="product_package_type_id" class="form-control product_package_type_id">
                              <option value="">Parent Product Package Type</option>
                              @foreach ($productPackageType as $productPackageTypeValue)
                                <option value="{{ $productPackageTypeValue->id }}">{{ $productPackageTypeValue->name }}</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="form-group plus-btn-div">
                            <button type="button" class="btn btn-primary" onclick="managePopup('product-package-type');"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>


<div class="modal fade" id="product-size-width-qty" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="productSizeWidthQtyForm" name="productSizeWidthQtyForm">
            <div class="modal-header">
            <h4 class="modal-title">Product Size Width Qty.</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Product Size Width Qty. </label>
                        <input type="number" class="form-control size" id="size" name="size" placeholder="Enter size" min="0">
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>

<div class="modal fade" id="product-size-length-qty" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="productSizeLengthQtyForm" name="productSizeLengthQtyForm">
            <div class="modal-header">
            <h4 class="modal-title">Product Size Length Qty.</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Product Size Length Qty. </label>
                        <input type="number" class="form-control size" id="size" name="size" placeholder="Enter size" min="0">
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>


<div class="modal fade" id="product-size-thickness-qty" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="productSizeThicknessQtyForm" name="productSizeThicknessQtyForm">
            <div class="modal-header">
            <h4 class="modal-title">Product Size Thickness Qty.</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Product Size Thickness Qty. </label>
                        <input type="text" class="form-control size" id="size" name="size" placeholder="Enter size" >
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>


<div class="modal fade" id="spec-fastener" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="specFastenerForm" name="specFastenerForm">
            <div class="modal-header">
            <h4 class="modal-title">Spec. Fastener</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Spec. Fastener. </label>
                        <input type="number" class="form-control squer_foot" id="squer_foot" name="squer_foot" placeholder="Enter squer foot" min="0">
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>



<div class="modal fade" id="spec-fastener-screw" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="specFastenerScrewForm" name="specFastenerScrewForm">
            <div class="modal-header">
            <h4 class="modal-title">Spec. Fastener Screw</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Spec. Fastener. Screw</label>
                        <input type="text" class="form-control length_inch" id="length_inch" name="length_inch" placeholder="Enter length ">
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>


<div class="modal fade" id="spec-fastener-nail" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="specFastenerNailForm" name="specFastenerNailForm">
            <div class="modal-header">
            <h4 class="modal-title">Spec. Fastener Nail</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Spec. Fastener. Nail</label>
                        <input type="text" class="form-control length_inch" id="length_inch" name="length_inch" placeholder="Enter length ">
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>


<div class="modal fade" id="product-package-type" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="productPackageTypeForm" name="productPackageTypeForm" method="POST">
            <div class="modal-header">
            <h4 class="modal-title">Parent Product Package Type</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Parent Product Package Type</label>
                        <input type="text" class="form-control" id="parent_product_package_type" name="parent_product_package_type" placeholder="Enter parent product package type ">
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>



<div class="modal fade" id="main-sub-parent-product-type" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="mainSubParentProductTypeForm" name="mainSubParentProductTypeForm" method="POST">
            <div class="modal-header">
            <h4 class="modal-title">Main Sub Parent Product Type</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Main Sub Parent Product Type</label>
                        <input type="text" class="form-control" id="main_sub_parent_product_type" name="main_sub_parent_product_type" placeholder="Enter main sub parent product type">
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>


<div class="modal fade" id="secondary-sub-parent-product-type" aria-modal="true" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <form id="secondarySubParentProductTypeForm" name="secondarySubParentProductTypeForm" method="POST">
            <div class="modal-header">
            <h4 class="modal-title">Secondary Sub Parent Product Type</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="form-group col-md-12">
                        <label for="username">Secondary Sub Parent Product Type</label>
                        <input type="text" class="form-control" id="secondary_sub_parent_product_type" name="secondary_sub_parent_product_type" placeholder="Enter secondary sub parent product type">
                    </div>

                </div>

            </div>
            <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
      </div>

    </div>

</div>


@endsection
@push('customJs')
<!-- jquery-validation -->
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>

<script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
@include('admin.customJs.parent-product')

<script>
      $('.field_wrapper').on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove();
    });


    let i = 1;
    function addSuppliers() {

        i++;

        $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        url: '{{ route('admin.parent-product-add-supplier') }}',
        type: 'post',
        // processData: false,
        // contentType: false,
        data: {index : i},
        beforeSend: function() {
        $(".data-loader").addClass('loading');
        },
        success: function(response) {
            $(".data-loader").removeClass('loading');
            $( ".field_wrapper" ).append( $( response ) );
            $(".product_package_type_id_text").val($("#get_product_package_type_text").val());

        },
        error: function(data) {
            $(".data-loader").removeClass('loading');
            errorHandler(data);
        }
    });

}
</script>
@endpush
