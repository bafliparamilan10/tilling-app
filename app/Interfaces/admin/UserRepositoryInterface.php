<?php

namespace App\Interfaces\admin;

interface UserRepositoryInterface
{
    public function deleteUser($userId);
    public function createUser(array $data);
    public function getCity($id);
    public function userEnableDisable(array $data);
}
