<?php

namespace App\DataTables;

use App\Models\Manufacturer;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ManufacturerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
        ->addIndexColumn()
        ->editColumn('status', function (Manufacturer $data) {
            $status = "";
            if ($data->status == 1) {
                $status =  '<button type="button" class="btn btn-outline-success btn-block" onclick="activeDeactive('.$data->status.','.$data->id.')">Active</button>';
            } else {
                $status =  '<button type="button" class="btn btn-outline-danger btn-block" onclick="activeDeactive('.$data->status.','.$data->id.')">InActive</button>';
            }
            return $status;
        })
        ->addColumn('action', function ($data) {
            $action = "";
            $action .= '<a href="'.route('admin.manufacturer-view', ['id' => Crypt::encrypt($data->id)]).'" class="btn btn-outline-primary mr-1"><i class="fa fa-eye" title="view"></i></a>';
            $action .= '<a href="'.route('admin.manufacturer-edit', ['id' => Crypt::encrypt($data->id)]).'" class="btn btn-outline-success mr-1"><i class="fa fa-edit" title="Edit"></i></a>';
            $action .= '<a  onclick="deleteManufacturer('.$data->id.')" class="btn btn-outline-danger mr-1"><i class="fa fa-trash" title="Delete"></i></a>';
            return $action;
        })->rawColumns(['action','status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Manufacturer $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Manufacturer $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                ->setTableId('manufacture-table')
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->dom('Blfrtip')
                ->orderBy(1)
                ->buttons([
                    // Button::make('excel'),
                    // Button::make('csv'),
                    // Button::make('pdf'),
                ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')
            ->title('Sr No.')->searchable(false)->orderable(false),
            Column::make('id')->searchable(false)->exportable(true)->hidden(true),
            Column::make('name')->title('Manufacturers'),
            Column::make('ticker')->title('Tickers')->orderable(false),
            Column::make('status')->title('Status')->searchable(false)->orderable(false),
            Column::computed('action')->title('Actions')->exportable(false)->printable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Manufacturer_' . date('YmdHis');
    }
}
