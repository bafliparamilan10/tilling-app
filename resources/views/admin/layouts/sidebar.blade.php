<aside class="main-sidebar elevation-4 sidebar-dark-light">
    <!-- Brand Logo -->
    <a href="{{route('admin.dashboard')}}" class="brand-link text-center navbar-light">
    <img src="{{ asset('admin/dist/img/icon.png') }}" alt="AdminLTE Logo" class="brand-image  elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-bold text-dark-full"> {{ auth()->user()->first_name.' '.auth()->user()->last_name }}  </span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column font-weight-bold text-white" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{route('admin.dashboard')}}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('admin.user-list')}}" class="nav-link">
                        <i class="nav-icon fas fa-solid fa-user"></i>
                        <p>User Management</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.manufacturer-list')}}" class="nav-link">
                        <i class="nav-icon fas fa-briefcase"></i>
                        <p>Manufacturer Management</p>
                    </a>
                </li>

                  <li class="nav-item">
                    <a href="{{route('admin.supplier-list')}}" class="nav-link">
                        <i class="nav-icon fas fa-solid fa-truck"></i>
                        <p>Supplier Management</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.parent-product-list') }}" class="nav-link">
                        <i class="nav-icon fas fa-solid fa-list"></i>
                        <p>Parent Product Management</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.child-product-list') }}" class="nav-link">
                        <i class="nav-icon fas fa-solid fa-bars"></i>
                        <p>Child Product Management</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.cms-page-list') }}" class="nav-link">
                        <i class="nav-icon fas fa-solid fa-book"></i>
                        <p>CMS Mangement</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.subscription-order-list') }}" class="nav-link">
                        <i class="nav-icon fas fa-light fa-crown"></i>
                        <p>Subscription Orders</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
