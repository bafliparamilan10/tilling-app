<?php

namespace App\DataTables;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addIndexColumn()
            ->editColumn('status', function (User $data) {
                $status = "";
                if ($data->status == 1) {
                    $status =  '<button type="button" class="btn btn-outline-success btn-block" onclick="activeDeactive('.$data->status.','.$data->id.')">Active</button>';
                } else {
                    $status =  '<button type="button" class="btn btn-outline-danger btn-block" onclick="activeDeactive('.$data->status.','.$data->id.')">InActive</button>';
                }
                return $status;
            })
            ->editColumn('state_id', function (User $data) {

                return $data->getStateDetails->name;
            })
            ->editColumn('city_id', function (User $data) {

                return $data->getCityDetails->name;
            })
            ->editColumn('subscription_plan', function (User $data) {

                return $data->getPurchasedPlanDetails ? ($data->getPurchasedPlanDetails->getPlanDetails ? $data->getPurchasedPlanDetails->getPlanDetails->name : '-') : '-';
            })
            ->addColumn('action', function ($data) {
                $action = "";
                $action .= '<a href="'.route('admin.user-view', ['id' => Crypt::encrypt($data->id)]).'" class="btn btn-outline-primary mr-1"><i class="fa fa-eye" title="view"></i></a>';
                $action .= '<a href="'.route('admin.user-edit-page', ['id' => Crypt::encrypt($data->id)]).'" class="btn btn-outline-success mr-1"><i class="fa fa-edit" title="Edit"></i></a>';
                return $action;
            })->rawColumns(['action','status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model, Request $request): QueryBuilder
    {
        $subscription = $request->subscription;
        $model = User::with(['getStateDetails', 'getCityDetails', 'getPurchasedPlanDetails','getPurchasedPlanDetails.getPlanDetails']);

        if (!empty($request->status) || strlen($request->status)) {
            $model = $model->where('status', $request->status);
        }
        if (!empty($request->subscription) || strlen($request->subscription)) {
            $model = $model->whereHas('getPurchasedPlanDetails.getPlanDetails', function ($query) use ($subscription) {
                $query->where('type', $subscription);
            });
        }
        if (!empty($request->state) || strlen($request->state)) {
            $model = $model->where('state_id', $request->state);
        }
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('user-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->orderBy(1,'Desc')
                    ->buttons([
                        // Button::make('excel'),
                        // Button::make('csv'),
                        // Button::make('pdf'),
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')
            ->title('Sr No.')->searchable(false)->orderable(false),
            Column::make('first_name')->title('First Name')->orderable(true),
            Column::make('last_name')->title('Last Name')->orderable(true),
            Column::make('email')->title('Email Address')->orderable(false),
            Column::make('city_id')->name('getCityDetails.name')->title('City')->orderable(false)->searchable(false),
            Column::make('state_id')->name('getStateDetails.name')->title('State')->orderable(false)->searchable(false),
            Column::make('subscription_plan')->title('Subscription')->orderable(false)->searchable(false),
            Column::make('status')->title('Status')->searchable(false)->orderable(false),
            Column::computed('action')->title('Actions')->exportable(false)->printable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'User_' . date('YmdHis');
    }
}
