<?php

use App\Http\Controllers\Api\MethodController;
use App\Http\Controllers\Api\SubscriptionController;
use App\Http\Controllers\Api\SupplierController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;
use Mockery\Generator\Method;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('state-list',[UserController::class, 'stateList'])->name('state-list');
Route::post('city-list',[UserController::class, 'cityList'])->name('city-list');
Route::post('sign-up',[UserController::class, 'signUp'])->name('sign-up');
Route::post('email-exists',[UserController::class, 'emailExists'])->name('email-exists');
Route::post('login', [UserController::class, 'login'])->name('login');
Route::post('forgot-password', [UserController::class, 'forgotPassword'])->name('forgot-password');
Route::post('reset-password', [UserController::class, 'resetPassword'])->name('reset-password');
Route::get('subscription-list', [SubscriptionController::class, 'subscriptionList']);

Route::middleware('auth:api')->group(function () {
    
    Route::controller(UserController::class)->group(function () {
        Route::post('notification-update','notificationUpdate');
        Route::get('/logout','logout');
        Route::post('/change-password','changePassword');
        Route::post('/profile-get','profileGet');
        Route::post('/profile-update','profileUpdate');
        Route::post('/delete-account','deleteAccount');
    });

    Route::controller(SupplierController::class)->group(function () {
        Route::get('supplier-list','supplierList');
        Route::post('supplier-fav-unfav','supplierFavUnfav');
        Route::post('supplier-fav-list','supplierFavList');
    });

    Route::controller(SubscriptionController::class)->group(function () {
        // Route::get('subscription-list','subscriptionList');
        Route::post('subscription-purchase','subscriptionPurchase');
        Route::post('subscription-cancel','subscriptionCancel');
        Route::get('subscription-receipts','subscriptionReceipts');
    });

    Route::controller(MethodController::class)->group(function () {
        Route::post('method-filter-list','methodFilterList');
    });

});
