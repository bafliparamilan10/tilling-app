<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    use HasFactory;
    protected $guarded = [];

    // below both function will be used for get infinite child

    // public function childProductType()
    // {
    //     return $this->hasMany(ProductType::class, 'parent_id', 'id');
    // }

    // public function allChildProductType()
    // {
    //     return $this->childProductType()->with('allChildProductType');
    // }

    public function getMainSubChildData()
    {
        return $this->hasMany(ProductType::class, 'parent_id', 'id');
    }
}
