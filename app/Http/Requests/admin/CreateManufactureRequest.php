<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateManufactureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            // 'username' => 'required',
            'logo' => 'nullable|image|mimes:jpg,jpeg,png,gif',
            'name'=> "required|unique:manufacturers,name,{$this->id},id,deleted_at,NULL",
            'ticker' => "required",
            'status' => "required",
            'website' => 'nullable|url'
        ];
    }
}
