<?php

namespace App\Http\Controllers\admin;

use App\DataTables\CmsPageDataTable;
use App\Http\Controllers\Controller;
use App\Interfaces\admin\CmsPageRepositoryInterface;
use App\Models\CmsPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class CmsController extends Controller
{
    private CmsPageRepositoryInterface $cmsPageRepository;

    public function __construct(CmsPageRepositoryInterface $cmsPageRepository)
    {
        $this->cmsPageRepository = $cmsPageRepository;
    }

    public function CmsList(CmsPageDataTable $dataTable)
    {
        return $dataTable->render('admin.cmsPage.list');
    }

    public function CmsEdit($id)
    {
        $id = Crypt::decrypt($id);
        $cmsPageData = CmsPage::find($id);

        return view('admin.cmsPage.edit',compact('cmsPageData'));
    }

    public function CmsUpdate(Request $request)
    {
        $data = $this->cmsPageRepository->CmsUpdate($request->all());
        return $data;
    }
}
