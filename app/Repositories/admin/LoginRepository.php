<?php

namespace App\Repositories\admin;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Interfaces\admin\LoginRepositoryInterface;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;

//use Your Model

/**
 * Class LoginRepository.
 */
class LoginRepository implements LoginRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        //return YourModel::class;
    }

    public function customLogin(array $data)
    {
        $admin = Admin::where('email', $data['email'])->first();
        if (!empty($admin)) {
            if (Hash::check($data['password'], $admin->password)) {
                $remember_me = isset($data['remember']) ? true : false;
                $auth = Auth::guard('admin')->attempt(['email' => $data['email'], 'password' => $data['password']]);
                if (Auth::guard('admin')->check()) {
                    $response['status'] = true;
                    $response['message'] = 'Login successfully';
                } else {
                    $response['status'] = false;
                    $response['message'] = 'Your username or password is incorrect';
                }
            } else {
                $response['status'] = false;
                $response['message'] = 'Your password is incorrect';
            }
        } else {
            $response['status'] = false;
            $response['message'] = 'Please enter valid credentials';
        }
        
        return $response;
    }

    public function adminForgetPasswordMail($emailData)
    {
        try {
            // dd($emailData);
            DB::beginTransaction();
            $checkEmail = Admin::where('email', $emailData['email'])->first();
            // $checkEmail = Admin::where('email',$emailData['email'])->first();

            if (isset($checkEmail) && $checkEmail !== '') {
                $enEmail = Crypt::encryptString($emailData['email']);
                $url = URL::to('/admin/adminrecover-password/'.$enEmail);

                $mailData = [
                    'name' => $checkEmail->first_name.' '.$checkEmail->last_name,
                    'url' => $url,
                ];

                Admin::where('email', $emailData['email'])->update(['recover_password_link' => 1]);
                Mail::to($emailData['email'])->send(new ForgotPasswordMail($mailData));
                DB::commit();

                return [
                    'status' => true,
                    'message' => 'Reset Link send your email address sucessfully',
                    'data' => new stdClass(),
                ];
            } else {
                return ['status' => false, 'message' => 'Email does not exits', 'data' => new stdClass()];
            }
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            Log::debug('forgetPasswordMail : ', ['error' => $th]);

            return ['status' => false, 'message' => 'Something went wrong', 'data' => new stdClass()];
        }
    }
    public function recoverPasswordStore($recoverData)
    {
        try {
            DB::beginTransaction();
            $checkEmail = Admin::where('email', $recoverData['email'])->first();
            if (isset($checkEmail)) {
                $responseDB = Admin::where('email', $recoverData['email'])
                ->update(['password' => Hash::make($recoverData['password']), 'recover_password_link' => 0]);
                if ($responseDB) {
                    DB::commit();

                    return ['status' => true, 'message' => 'Password change sucessfully', 'data' => new stdClass()];
                } else {
                    return ['status' => false, 'message' => 'password not change', 'data' => new stdClass()];
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            Log::debug('recoverPasswordStore : ', ['error' => $th]);

            return ['status' => false, 'message' => 'Something went wrong', 'data' => new stdClass()];
        }
    }
    public function getUserById($data)
    {
        try {
            $getData = Admin::where('id', $data)->first();

            return $getData;
        } catch (\Throwable $th) {
            Log::error(' get Admin profile : ', ['error' => $th]);
        }
    }

    public function updateAdmin($id, $userData)
    {
        try {
            DB::beginTransaction();
            $user = [
                'first_name' => $userData['first_name'],
                'last_name' => $userData['last_name'],
                'email' => $userData['email'],

            ];
            if (isset($userData['profile_picture'])) {
                $profile_picture = $userData['profile_picture'];
                $extension = $profile_picture->getClientOriginalExtension();
                $profile_pictureName = $profile_picture->getFilename();
                $profile_picture->move(public_path('/super_admin/images'), $profile_pictureName.'.'.$extension);
                $user['profile_picture'] = $profile_picture->getFilename().'.'.$extension;
            }
            $data = Admin::where('id', $id)->update($user);
            $response = [];
            if ($data) {
                $response['status'] = true;
                $response['message'] = 'Profile Update Sucessfully';
            } else {
                $response['status'] = false;
                $response['message'] = 'Profile not update';
            }
            DB::commit();

            return $response;
        } catch (\Throwable $e) {
            //throw $th;
            DB::rollback();
            Log::debug(' Admin User update : ', ['error' => $e]);

            $response['status'] = false;
            $response['message'] = 'something was wrong';

            return $response;
        }
    }

    public function changePassword($data)
    {
        try {
            DB::beginTransaction();

            $adminId = Auth::guard('admin')->user()->id;
            $check_old_password = Hash::check($data['oldpassword'], Auth::guard('admin')->user()->password);
            $response = [];

            if (isset($check_old_password) && $check_old_password == true) {
                $responseDB = Admin::where('id', Auth::guard('admin')->user()->id)
                ->update(['password' => Hash::make($data['newpassword'])]);

                if ($responseDB) {
                    $response['status'] = true;
                    $response['message'] = 'Password Change Sucessfully';
                } else {
                    $response['status'] = false;
                    $response['message'] = 'Old Password not match';
                }
            } else {
                $response['status'] = false;
                $response['message'] = 'Old Password not match';
            }
            DB::commit();

            return $response;
        } catch (\Throwable $e) {
            //throw $th;
            DB::rollback();
            Log::debug(' Admin User change pass : ', ['error' => $e]);

            $response['status'] = false;
            $response['message'] = 'something was wrong';

            return $response;
        }
    }
}
