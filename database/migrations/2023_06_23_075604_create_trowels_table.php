<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trowels', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('child_product_id')->nullable();
            $table->foreign('child_product_id')->references('id')->on('child_products')->onDelete('cascade')->onUpdate('cascade');
            $table->string('trowel_type', 200)->nullable();
            $table->string('trowel_size', 200)->nullable();
            $table->string('coverage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trowels');
    }
};
