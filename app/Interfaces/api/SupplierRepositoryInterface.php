<?php

namespace App\Interfaces\api;

interface SupplierRepositoryInterface
{
    public function supplierList(array $data);
    public function supplierFavUnfav(array $data);
    public function supplierFavList(array $data);
}
