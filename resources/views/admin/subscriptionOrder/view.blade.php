@extends('admin.layouts.master')
@push('customCSS')
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush
@section('page_title', 'View Subscription Order Details')
@section('content_header')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">View Subscription Order Details</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.subscription-order-list')}}">Subscription Orders</a></li>
                    <li class="breadcrumb-item active">View Subscription Order Details</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="col-md-11 mx-auto">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header text-center">
                    <h3 class="card-title float-none"> View Subscription Order Details </h3>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card-body box-profile">
                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>First Name : </b> 
                                    <span class="ml-2">{{$subscriptionOrder->getUserDetails->first_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Last Name : </b> 
                                    <span class="ml-2">{{$subscriptionOrder->getUserDetails->last_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Email Address : </b> 
                                    <span class="ml-2">{{$subscriptionOrder->getUserDetails->email }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Start Date : </b> 
                                    <span class="ml-2">{{$subscriptionOrder->start_date }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>End Date : </b> 
                                    <span class="ml-2">{{$subscriptionOrder->end_date }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-body box-profile">
                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Subscription Plan : </b> 
                                    <span class="ml-2">{{$subscriptionOrder->getPlanDetails->name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Price : </b>
                                    <span class="ml-2">{{$subscriptionOrder->price }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-5 text-center">
                <a href="javascript:history.back()" class="btn btn-secondary">Back</a>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
@push('customJs')
<!-- jquery-validation -->
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
@include('admin.customJs.supplier')
@endpush
