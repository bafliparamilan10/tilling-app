@extends('admin.layouts.master')

@push('customCSS')
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush

@section('page_title', 'Child Product Management')
@section('content_header')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Child Product Management</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin.supplier-list') }}">Child Product Management</a></li>
                        <li class="breadcrumb-item active">Child Product Details</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="col-md-11 mx-auto">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"> View Child Product</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-body box-profile">
                                <input type="hidden" id="id" name="id" value="{{ $childProduct->id }}">
                                <input type="hidden" id="product_type" name="product_type" value="{{ $childProduct->product_type_id }}">

                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Image / Logo : </b> 
                                        <span class="float-right"> 
                                            <img src="{{ $childProduct->logo }}" style="height: 100px; width:150px;">
                                        </span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Product Name : </b> 
                                        <span class="float-right product_name">{{ $childProduct->name }}</span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Product Manufacturer : </b> 
                                        <span class="float-right">{{ $childProduct->getManufactureData->name }}</span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Product Area : </b> 
                                        <span class="float-right">{{ $childProduct->product_area }}</span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Product ANSI : </b> 
                                        <span class="float-right">{{ (isset($childProduct->getAnsiData->name) ? $childProduct->getAnsiData->name : '') }}</span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Product Type : </b> 
                                        <span class="float-right">
                                            {{ (isset($childProduct->getProductTypeDetails->name) ? $childProduct->getProductTypeDetails->name : "") . ' / ' . (isset($childProduct->getProductTypeMainSubChildDetails->name) ? $childProduct->getProductTypeMainSubChildDetails->name : "") . ' / ' . (isset($childProduct->getProductTypeSecondarySubChildDetails->name) ? $childProduct->getProductTypeSecondarySubChildDetails->name : "") }}
                                        </span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Data Sheet (url) : </b>
                                        <span class="float-right">
                                            @foreach ($childProduct->data_sheet as $key => $dataSheetValue)
                                                <input type="radio" name="dataSheetId" value="{{ $dataSheetValue }}"
                                                    @checked($dataSheetValue == $childProduct->data_sheet_id)>
                                                <a href="{{ $dataSheetValue }}">{{ $key }}</a>
                                                <br>
                                            @endforeach
                                        </span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Supplier Name : </b>
                                        <select class="form-control getSupplier float-right" style="width: 40%">
                                            @foreach ($childProduct->getSupplierData as $getSupplierDataValue)
                                                <option value="{{ $getSupplierDataValue->id }}">
                                                    {{ $getSupplierDataValue->getSupplierDetails->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-header">
                        <h3 class="card-title"> Other Details</h3>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-body box-profile">
                                <ul class="list-group list-group-unbordered mb-3">

                                    <li class="list-group-item">
                                        <b>Supplier Product Link (url) : </b> 
                                        <span class="float-right product_links">{{ $getSupplierDataValue->product_links }}</span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Supplier Product MDL # : </b> 
                                        <span class="float-right product_mdl">{{ $getSupplierDataValue->product_mdl }}</span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Product Package Type : </b> 
                                        {{-- <span class="float-right">{{ (isset($getSupplierDataValue->product_package_type_id) ?? "") }}</span> --}}
                                        <span class="float-right">{{ (isset($childProduct->getParentProdcutPackageTypeData->name) ? $childProduct->getParentProdcutPackageTypeData->name : "") }}</span>
                                    </li>

                                    <li class="list-group-item product_size_width">
                                        <b>Product Size (width/ft.) Quantity : </b> 
                                        <span class="float-right product_size_width_qty">{{ (isset($childProduct->getAnsiData->name) ? $childProduct->getAnsiData->name : '' )}}</span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Product Size (length/ft.) Quantity : </b> 
                                        <span class="float-right get_product_size_length_qty_data">
                                            {{ (isset($getSupplierDataValue->getProductSizeLengthQtyData->size) ? $getSupplierDataValue->getProductSizeLengthQtyData->size : '') . ' / ' . (isset($childProduct->getProductTypeMainSubChildDetails->name) ? $childProduct->getProductTypeMainSubChildDetails->name : '') . ' / ' . (isset($childProduct->getProductTypeSecondarySubChildDetails->name) ? $childProduct->getProductTypeSecondarySubChildDetails->name : '') }}
                                        </span>
                                    </li>

                                    <li class="list-group-item product_size_lbs">
                                        <b>Product Size (# of lbs): </b> 
                                        {{-- {{ dd($getSupplierDataValue->getProductSizeLbs) }} --}}
                                        <span class="float-right get_product_size_lbs">
                                            {{ (isset($getSupplierDataValue->getProductSizeLbs->size) ? $getSupplierDataValue->getProductSizeLbs->size : '' )}}
                                        </span>
                                    </li>

                                    <li class="list-group-item product_size_gallon">
                                        <b>Product Size (# of gallons): </b> 
                                        <span class="float-right get_product_size_gallon">
                                            {{ (isset($getSupplierDataValue->getProductSizeGallon->size) ? $getSupplierDataValue->getProductSizeGallon->size : "" )}}
                                        </span>
                                    </li>

                                    <li class="list-group-item product_size_coverage">
                                        <b>Product Size (coverage) Quantity: </b> 
                                        <span class="float-right get_product_size_coverage">
                                            {{ (isset($getSupplierDataValue->getProductSizeCoverage->size) ? $getSupplierDataValue->getProductSizeCoverage->size : '' )}}
                                        </span>
                                    </li>

                                    <li class="list-group-item product_overage">
                                        <b>Add an overage % (waste): </b> 
                                        <span class="float-right get_product_overage">
                                            {{ (isset($getSupplierDataValue->getProductSizeOverage->percentage) ? $getSupplierDataValue->getProductSizeOverage->percentage : '' )}}
                                        </span>
                                    </li>

                                    <li class="list-group-item product_color">
                                        <b>Product Color: </b> 
                                        <span class="float-right get_product_color">
                                            {{ (isset($getSupplierDataValue->getProductColor->name) ? $getSupplierDataValue->getProductColor->name : '' )}}
                                        </span>
                                    </li>
                                </ul>

                                <br>
                                <h4 class="w-100 mb-5">Specifications (product) :</h4>
                                
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item thin_set_ansis">
                                        <b>Spec. - Thin set's ANSIs : </b> 
                                        <span class="float-right">{{ (isset($getSupplierDataValue->ansi_ids) ? $childProduct->getAnsiName($getSupplierDataValue->ansi_ids) : '') }}</span>
                                    </li>

                                    <li class="list-group-item thin_set_qty_types">
                                        <b>Spec. - Thin set's Quality Types : </b> 
                                        <span class="float-right">{{ (isset($getSupplierDataValue->thin_set_qty_type_ids) ? $childProduct->getThinSetQtyName($getSupplierDataValue->thin_set_qty_type_ids) : '')}}</span>
                                    </li>
                                    
                                    <li class="list-group-item ">
                                        <b>Product Price ($) : </b> 
                                        <span class="float-right product_price">{{ (isset($getSupplierDataValue->product_price) ? $getSupplierDataValue->product_price : '') }}</span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Product Price (Per Sq. Ft. in $) : </b> 
                                        <span class="float-right product_price_per_sq">{{ (isset($getSupplierDataValue->product_price_per_sq) ? $getSupplierDataValue->product_price_per_sq : '') }}</span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Status : </b> 
                                        <span class="float-right status">{{ (isset($getSupplierDataValue->status) && ($getSupplierDataValue->status == 1) ? 'Active' : 'Inactive') }}</span>
                                    </li>
                                </ul>

                                <div class="row w-100 trowel_hide_show">
                                    <div class="col-6 trowel mb-3">
                                        <div class="form-group col-md-12 d-flex added-d-flex">
                                            <div class="w-100">
                                                <div>
                                                    <label class="mb-3 float-left">Trowel Type</label>
                                                    <label class="mb-3 float-right">{{ (isset($getSupplierDataValue->getTrowelDefault->trowel_type) ? $getSupplierDataValue->getTrowelDefault->trowel_type : "" )}}</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-12 d-flex added-d-flex">
                                            <div class="w-100">
                                                <label class="mb-3 float-left">Trowel Size</label>
                                                <label class="mb-3 float-right">{{ (isset($getSupplierDataValue->getTrowelDefault->trowel_size) ? $getSupplierDataValue->getTrowelDefault->trowel_size : "" )}}</label>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-12 d-flex added-d-flex">
                                            <div class="w-100">
                                                <label class="mb-3 float-left">Coverage (sq. ft. - quantity)</label>
                                                <label class="mb-3 float-right">{{ (isset($getSupplierDataValue->getTrowelDefault->coverage) ? $getSupplierDataValue->getTrowelDefault->coverage : "" )}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5 text-center">
                    <a href="javascript:history.back()" class="btn btn-secondary">Back</a>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('customJs')
    <script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>

    $(document).ready(function() {
        var product_type = $("#product_type").val();  

        if(product_type == 21){

            $(".product_size_lbs").removeClass('d-flex');
            $(".product_size_lbs").addClass('d-none');

            $(".product_size_gallon").removeClass('d-flex');
            $(".product_size_gallon").addClass('d-none');

            $(".product_size_coverage").removeClass('d-flex');
            $(".product_size_coverage").addClass('d-none');

            $(".product_overage").removeClass('d-flex');
            $(".product_overage").addClass('d-none');

            $(".product_color").removeClass('d-flex');
            $(".product_color").addClass('d-none');

            $(".thin_set_ansis").removeClass('d-flex');
            $(".thin_set_ansis").addClass('d-none');

            $(".thin_set_qty_types").removeClass('d-flex');
            $(".thin_set_qty_types").addClass('d-none');

            $(".trowel_hide_show").removeClass('d-flex');
            $(".trowel_hide_show").addClass('d-none');

        }
        else if(product_type == 24){

            $(".product_size_lbs").removeClass('d-flex');
            $(".product_size_lbs").addClass('d-none');

            $(".product_size_gallon").removeClass('d-flex');
            $(".product_size_gallon").addClass('d-none');

            $(".product_size_coverage").removeClass('d-flex');
            $(".product_size_coverage").addClass('d-none');

            $(".product_color").removeClass('d-flex');
            $(".product_color").addClass('d-none');

            $(".thin_set_ansis").removeClass('d-flex');
            $(".thin_set_ansis").addClass('d-none');

            $(".thin_set_qty_types").removeClass('d-flex');
            $(".thin_set_qty_types").addClass('d-none');

            $(".trowel_hide_show").removeClass('d-flex');
            $(".trowel_hide_show").addClass('d-none');
        }
        else if(product_type == 27){

            $(".product_size_width").removeClass('d-flex');
            $(".product_size_width").addClass('d-none');

            $(".product_size_gallon").removeClass('d-flex');
            $(".product_size_gallon").addClass('d-none');

            $(".product_size_coverage").removeClass('d-flex');
            $(".product_size_coverage").addClass('d-none');
        }
        else if(product_type == 28){

            $(".product_size_width").removeClass('d-flex');
            $(".product_size_width").addClass('d-none');

            $(".product_size_length").removeClass('d-flex');
            $(".product_size_length").addClass('d-none');

            $(".product_size_lbs").removeClass('d-flex');
            $(".product_size_lbs").addClass('d-none');

            $(".product_overage").removeClass('d-flex');
            $(".product_overage").addClass('d-none');

            $(".product_color").removeClass('d-flex');
            $(".product_color").addClass('d-none');

            $(".thin_set_ansis").removeClass('d-flex');
            $(".thin_set_ansis").addClass('d-none');

            $(".thin_set_qty_types").removeClass('d-flex');
            $(".thin_set_qty_types").addClass('d-none');

            $(".trowel_hide_show").removeClass('d-flex');
            $(".trowel_hide_show").addClass('d-none');
        }
        getPerticularSupplierDetails();
    });

    $(".getSupplier").change(function(e) {
        e.preventDefault();
        getPerticularSupplierDetails();
    });

    function getPerticularSupplierDetails() {
        var id = $(".getSupplier").val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.child-product-get-supplier-view-data') }}',
            type: 'post',
            data: {
                id: id
            },
            beforeSend: function() {
                // $(".data-loader").addClass('loading');
            },
            success: function(response) {
                console.log('getPerticularSupplierDetails',response);
                $(".product_links").text(response.product_links);
                $(".product_mdl").text(response.product_mdl);
                $(".product_price").text(response.product_price);
                $(".product_price_per_sq").text(response.product_price_per_sq);
                $(".get_product_size_lbs").text(response.get_product_size_lbs ? response.get_product_size_lbs.size : '');
                $(".get_product_size_coverage").text(response.get_product_size_overage ? response.get_product_size_overage.percentage : '');
                $(".get_product_overage").text(response.get_product_size_overage ? response.get_product_size_overage.percentage : '');
                $(".get_product_color").text(response.get_product_color ? response.get_product_color.name : '');
                $(".get_product_size_length_qty_data").text(response.get_product_size_length_qty_data ? response.get_product_size_length_qty_data.size : '');
                // $(".product_size_width_qty").text(response.get_product_size_width_qty_data.size);
                // $(".get_product_size_thickness_qty_data").text(response.get_product_size_thickness_qty_data.size);
                // $(".get_specification_fastners_data").text(response.get_specification_fastners_data.squer_foot);
                // $(".get_specification_fastners_screw_data").text(response.get_specification_fastners_screw_data.length_inch);
                // $(".get_specification_fastners_nails_data").text(response.get_specification_fastners_nails_data.length_inch);
               
                var status = response.status ? '<span class="text-success">Active</span>' : '<span class="text-danger">InActive</span>';
                $(".status").html(status);
                // $(".data-loader").removeClass('loading');
            },
            error: function(data) {
                errorHandler(data);
                // $(".data-loader").removeClass('loading');
            }
        });
    }

    $('input[type=radio][name=dataSheetId]').change(function() {
        var id = $("#id").val();
        var data_sheet_id = $("input[name='dataSheetId']:checked").val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ route('admin.child-product-update-default-data-sheet') }}',
            type: 'post',
            data: {
                id: id,
                data_sheet_id: data_sheet_id
            },
            beforeSend: function() {
                $(".data-loader").addClass('loading');
            },
            success: function(response) {
                console.log(response);
                $(".data-loader").removeClass('loading');

            },
            error: function(data) {
                // $(".data-loader").removeClass('loading');
                errorHandler(data);
            }
        });
    });
    </script>
@endpush
