<div class="row w-100">
    <div class="col-6 trowel mb-3 ml-2">
        <div class="form-group col-md-12 d-flex added-d-flex">
            <div class="w-100">
                <div>
                    <label class="mb-3 float-left">Trowel Type</label>
                    <label class="float-right text-success text-bold">
                        <input type="radio" name="trowel_type_default_{{ $index }}[]" class="trowel_type_default" value="1"> Default
                    </label>
                </div>

                <select id="trowel_type" name="trowel_type_{{ $index }}[]" class="form-control trowel_type allfield">
                    <option value="">Select Trowel Type </option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                </select>
            </div>
        </div>

        <div class="form-group col-md-12 d-flex added-d-flex">
            <div class="w-100">
                <label class="mb-3">Trowel Size</label>
                <select id="trowel_size" name="trowel_size_{{ $index }}[]" class="form-control trowel_size allfield">
                    <option value="">Select Trowel Size </option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </div>
        </div>

        <div class="form-group col-md-12 d-flex added-d-flex">
            <div class="w-100">
                <label class="mb-3">Coverage (sq. ft. - quantity)</label>
                <select id="coverage" name="coverage_{{ $index }}[]" class="form-control coverage allfield">
                    <option value="">Select Coverage </option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </div>
        </div>
    </div>
</div>
