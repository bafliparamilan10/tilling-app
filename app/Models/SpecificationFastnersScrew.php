<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecificationFastnersScrew extends Model
{
    use HasFactory;
    protected $guarded = [];
}
