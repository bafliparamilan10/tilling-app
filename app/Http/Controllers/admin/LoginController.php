<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\admin\LoginRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\admin\ForgetPasswordRequest;
use Illuminate\Support\Facades\Crypt;
use App\Models\Admin;
use App\Http\Requests\admin\AdminProfileRequest;
use App\Http\Requests\admin\ChangePasswordRequest;

class LoginController extends Controller
{
    public function __construct(LoginRepositoryInterface $loginRepository)
    {
        // $this->middleware('guest:admin')->except('logout'); //Notice this middleware
        $this->loginRepository = $loginRepository;
    }
    public function showLoginForm()
    {
        return view('admin.login');
    }
    public function customLogin(Request $request)
    {

        $response = $this->loginRepository->customLogin($request->all());
        return  $response;
    }
    public function forgetPasswordPage()
    {
        return view('admin.forget-password');
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }
    public function logout(Request $request)
    {
        $this->guard('admin')->logout();
 
        // $request->session()->invalidate();
 
        return redirect('admin/login');
    }
    public function forgetPasswordMailsend(ForgetPasswordRequest $request)
    {
        $data = $this->loginRepository->adminForgetPasswordMail($request->all());

        return $data;
    }
    public function adminrecoverPasswordPage(Request $request)
    {
        // dd($request);
        $email = Crypt::decryptString($request->email);
        $user_check = Admin::where(['email' => $email, 'recover_password_link' => 1])->first();
        if (! empty($user_check)) {
            return view('admin.recover-password', compact('email'));
        } else {
            return view('admin.mail-link-error');
        }
    }
    public function recoverPasswordStore(Request $request)
    {
        $data = $this->loginRepository->recoverPasswordStore($request->all());

        return $data;
    }
    public function editPage()
    {
        $adminId = Auth::guard('admin')->user()->id;

        $data = $this->loginRepository->getUserById($adminId);

        return view('admin.admin.edit', compact('data'));
    }
    public function updateAdmin(AdminProfileRequest $request)
    {
        $adminId = Auth::guard('admin')->user()->id;

        $data = $this->loginRepository->updateAdmin($adminId, $request->all());

        return $data;
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $data = $this->loginRepository->changePassword($request->all());

        return $data;
    }
}
