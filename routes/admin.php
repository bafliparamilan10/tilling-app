<?php

use App\Http\Controllers\admin\ChildProductController;
use App\Http\Controllers\admin\CmsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\LoginController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\ManufacturerController;
use App\Http\Controllers\admin\ParentProductController;
use App\Http\Controllers\admin\SupplierController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['as' => 'admin.', 'middleware' => ['guest:admin']], function () {

    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('custom-login', 'LoginController@customLogin')->name('custom-login');
    Route::get('forget-password', 'LoginController@forgetPasswordPage')->name('forget-password');
    Route::post('forget-mail-send', 'LoginController@forgetPasswordMailsend')->name('forget-mail-send');
    Route::get('adminrecover-password/{email}', 'LoginController@adminrecoverPasswordPage')->name('adminrecover-password');
    Route::post('recover-password-store', 'LoginController@recoverPasswordStore')->name('recover-password-store');

    Route::group(['middleware' => ['auth:admin', 'XSS', 'guest']], function () {
        Route::controller(LoginController::class)->group(function () {
            Route::get('user-edit', 'editPage')->name('user-edit');
            Route::post('update-profile', 'updateAdmin')->name('update-profile');
            Route::post('change-password', 'changePassword')->name('change-password');
            Route::get('logout', 'logout')->name('logout');
        });

        Route::get('dashboard', 'DashboardController@dashboard')->name('dashboard');

        Route::controller(UserController::class)->group(function () {
            Route::get('user-add', 'addUser')->name('user-add');
            Route::get('user-list', 'list')->name('user-list');
            Route::delete('user-delete', 'deleteUser')->name('user-delete');
            Route::post('user-create', 'createUser')->name('user-create');
            Route::get('user-edit-page/{id}', 'userEditPage')->name('user-edit-page');
            Route::get('user-view/{id}', 'userDetails')->name('user-view');
            Route::post('get-city','getCity')->name('get-city');
            Route::post('user-enable-disable','userEnableDisable')->name('user-enable-disable');
            Route::get('test','test')->name('test');
        });

        Route::controller(ManufacturerController::class)->group(function () {
            Route::get('manufacturer-list', 'list')->name('manufacturer-list');
            Route::get('manufacturer-add', 'addManufacturer')->name('manufacturer-add');
            Route::post('manufacturer-create', 'manufacturerCreate')->name('manufacturer-create');
            Route::get('manufacturer-view/{id}', 'manufacturerView')->name('manufacturer-view');
            Route::get('manufacturer-edit/{id}', 'manufacturerEdit')->name('manufacturer-edit');
            Route::delete('manufacture-delete', 'deleteManufacturer')->name('manufacture-delete');
            Route::post('manufacturer-enable-disable','manufacturerEnableDisable')->name('manufacturer-enable-disable');
        });

        Route::controller(SupplierController::class)->group(function () {
            Route::get('supplier-list', 'list')->name('supplier-list');
            Route::get('supplier-add', 'addSupplier')->name('supplier-add');
            Route::post('supplier-create', 'supplierCreate')->name('supplier-create');
            Route::get('supplier-view/{id}', 'supplierView')->name('supplier-view');
            Route::get('supplier-edit/{id}', 'supplierEdit')->name('supplier-edit');
            Route::delete('supplier-delete', 'deleteSupplier')->name('supplier-delete');
            Route::post('supplier-enable-disable','supplierEnableDisable')->name('supplier-enable-disable');
        });

        Route::controller(ParentProductController::class)->group(function () {
            Route::get('parent-product-list', 'list')->name('parent-product-list');
            Route::get('parent-product-add', 'addParentProduct')->name('parent-product-add');
            Route::get('parent-product-edit/{id}', 'parentProductEdit')->name('parent-product-edit');
            Route::post('parent-product-add-supplier', 'addParentProductSupplier')->name('parent-product-add-supplier');
            Route::post('data-sheet-create', 'addDataSheet')->name('data-sheet-create');
            Route::post('product-size-width-qty-create', 'addProductSizeWidthQty')->name('product-size-width-qty-create');
            Route::post('product-size-length-qty-create', 'addProductSizeLengthQty')->name('product-size-length-qty-create');
            Route::post('product-size-thickness-qty-create', 'addProductSizeThicknessQty')->name('product-size-thickness-qty-create');
            Route::post('spec-fastener-create', 'addSpecFastener')->name('spec-fastener-create');
            Route::post('spec-fastener-screw-create', 'addSpecFastenerScrew')->name('spec-fastener-screw-create');
            Route::post('spec-fastener-nail-create', 'addSpecFastenerNail')->name('spec-fastener-nail-create');
            Route::post('get-product-type-data', 'getProductTypeData')->name('get-product-type-data');
            Route::post('parent-product-type-create', 'parentProductTypeCreate')->name('parent-product-type-create');
            Route::post('parent-product-package-type-create', 'parentProductPackageTypeCreate')->name('parent-product-package-type-create');
            Route::post('main-sub-parent-product-type-create', 'mainSubParentProductTypeCreate')->name('main-sub-parent-product-type-create');
            Route::post('secondary-sub-parent-product-type-create', 'secondarySubParentProductTypeCreate')->name('secondary-sub-parent-product-type-create');
            Route::post('parent-product-create', 'parentProductCreate')->name('parent-product-create');
            Route::post('get-supplier-data', 'getSupplierData')->name('get-supplier-data');
            Route::post('get-supplier-view-data', 'getSupplierViewData')->name('get-supplier-view-data');
            Route::post('parent-product-enable-disable', 'parentProductEnableDisable')->name('parent-product-enable-disable');
            Route::delete('parent-product-delete', 'deleteParentProduct')->name('parent-product-delete');
            Route::get('parent-product-view/{id}', 'parentProductView')->name('parent-product-view');
            Route::post('update-default-data-sheet', 'updateDefaultDataSheet')->name('update-default-data-sheet');
        });
        
        Route::controller(ChildProductController::class)->group(function () {
            Route::get('child-product-list', 'listChildProduct')->name('child-product-list');
            Route::get('child-product-add', 'addChildProduct')->name('child-product-add');
            Route::post('child-product-store', 'storeChildProduct')->name('child-product-store');
            Route::get('child-product-edit/{id}', 'editChildProduct')->name('child-product-edit');
            Route::get('child-product-view/{id}', 'viewChildProduct')->name('child-product-view');
            Route::delete('child-product-delete', 'deleteChildProduct')->name('child-product-delete');
            Route::post('child-product-enable-disable','enableDisableChildProduct')->name('child-product-enable-disable');
            Route::post('child-product-add-supplier', 'addChildProductSupplier')->name('child-product-add-supplier');
            Route::post('child-product-color-create','createColorChildProduct')->name('child-product-color-create');
            Route::post('child-product-add-trowel', 'addChildProductTrowel')->name('child-product-add-trowel');
            Route::post('child-product-size-lbs-create','createProductSizeLbs')->name('child-product-size-lbs-create');
            Route::post('child-product-size-gallon-create','createProductSizeGallon')->name('child-product-size-gallon-create');
            Route::post('child-product-size-coverage-create','createProductSizeCoverage')->name('child-product-size-coverage-create');
            Route::post('child-product-overage-create','createProductOverage')->name('child-product-overage-create');
            Route::post('child-product-update-default-data-sheet', 'updateDefaultDataSheet')->name('child-product-update-default-data-sheet');
            Route::post('child-product-get-supplier-data', 'getSupplierData')->name('child-product-get-supplier-data');
            Route::post('child-product-get-supplier-view-data', 'getSupplierViewData')->name('child-product-get-supplier-view-data');

            
        });

        Route::controller(CmsController::class)->group(function () {
            Route::get('cms-page-list', 'CmsList')->name('cms-page-list');
            Route::get('cms-page-edit/{id}', 'CmsEdit')->name('cms-page-edit');
            Route::post('cms-page-update', 'CmsUpdate')->name('cms-page-update');
        });

        Route::controller(SubscriptionOrderController::class)->group(function () {
            Route::get('subscription-order-list', 'subscriptionOrderList')->name('subscription-order-list');
            Route::get('subscription-order-view/{id}', 'subscriptionOrderView')->name('subscription-order-view');
        });
    });
});
