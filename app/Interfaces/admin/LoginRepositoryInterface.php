<?php

namespace App\Interfaces\admin;

interface LoginRepositoryInterface
{
    public function customLogin(array $data);
    public function adminForgetPasswordMail(array $emailData);
    public function recoverPasswordStore(array $recoverData);
    public function getUserById($userId);
    public function updateAdmin($id, array $userData);
    public function changePassword(array $data);
}
