<?php

namespace App\Interfaces\admin;

interface SupplierRepositoryInterface
{
    public function createSupplier(array $data);
    public function deleteSupplier($id);
    public function supplierEnableDisable(array $data);
}
