<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\admin\SupplierRepositoryInterface;
use App\DataTables\SupplierDataTable;
use App\Http\Requests\admin\CreateSupplierRequest;
use App\Models\Manufacturer;
use App\Models\Suppliers;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    private SupplierRepositoryInterface $supplierRepository;

    public function __construct(SupplierRepositoryInterface $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    public function list(SupplierDataTable $dataTable)
    {
        return $dataTable->render('admin.supplier.list');
    }

    public function addSupplier()
    {
        $manufacturer = Manufacturer::where('status', 1)->orderBy('id', 'DESC')->get();

        return view('admin.supplier.add',compact('manufacturer'));
    }

    public function supplierEdit($id)
    {
        $id = Crypt::decrypt($id);
        $manufacturer = Manufacturer::where('status', 1)->orderBy('id', 'DESC')->get();
        $supplierData = Suppliers::find($id);

        return view('admin.supplier.edit',compact('manufacturer','supplierData'));
    }

    public function supplierCreate(CreateSupplierRequest $request)
    {
        $data =  $this->supplierRepository->createSupplier($request->all());
        return $data;
    }

    public function supplierView($id)
    {
        $id = Crypt::decrypt($id);
        $supplierData = Suppliers::where('id', $id)->first();
        $manufactureCount = DB::table('manufacturers')->whereNull('deleted_at')
        ->whereRaw("FIND_IN_SET($supplierData->id, suppliers_ids)")
        ->count('id');

        return view('admin.supplier.view', compact('supplierData','manufactureCount'));
    }

    public function deleteSupplier(Request $request)
    {
        $data =  $this->supplierRepository->deleteSupplier($request['id']);
        return $data;
    }

    public function SupplierEnableDisable(Request $request)
    {
        $response = $this->supplierRepository->supplierEnableDisable($request->all());
        return  $response;
    }

}
