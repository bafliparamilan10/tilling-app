<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('product_types')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $productTypes = [
            [
                'name' => 'Backer Boards',
                'product_package_type_id' => 1,
                'is_main_sub_child' => 0,
                'is_parent' => 1,
                'child_data' => [
                    [
                        'name' => 'Cement Backers',
                        'product_package_type_id' => 1,
                        'is_main_sub_child' => 1,
                        'is_parent' => 1,
                        'child_data' => [],
                    ],
                    [
                        'name' => 'Fiber Cement Backers',
                        'product_package_type_id' => 1,
                        'is_main_sub_child' => 1,
                        'is_parent' => 1,
                        'child_data' => [],
                    ],
                    [
                        'name' => 'Foam Backers',
                        'product_package_type_id' => 1,
                        'is_main_sub_child' => 1,
                        'is_parent' => 1,
                        'child_data' => [],
                    ]
                ],
            ],
            [
                'name' => 'Membranes',
                'product_package_type_id' => 1,
                'is_main_sub_child' => 0,
                'is_parent' => 1,
                'child_data' => [
                    [
                        'name' => 'Cleavage',
                        'product_package_type_id' => 1,
                        'is_main_sub_child' => 1,
                        'is_parent' => 1,
                        'child_data' => [
                            [
                                'name' => 'Cleavage (bonded)',
                                'product_package_type_id' => 1,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 1,
                                'child_data' => [],
                            ],
                            [
                                'name' => 'Cleavage (unbonded)',
                                'product_package_type_id' => 1,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 1,
                                'child_data' => [],
                            ]
                        ],
                    ],
                    [
                        'name' => 'Crack Isolation',
                        'product_package_type_id' => 1,
                        'is_main_sub_child' => 1,
                        'is_parent' => 1,
                        'child_data' => [],
                    ],
                    [
                        'name' => 'Sound',
                        'product_package_type_id' => 1,
                        'is_main_sub_child' => 1,
                        'is_parent' => 1,
                        'child_data' => [],
                    ],
                    [
                        'name' => 'Uncoupling',
                        'product_package_type_id' => 1,
                        'is_main_sub_child' => 1,
                        'is_parent' => 1,
                        'child_data' => [
                            [
                                'name' => 'Thin set (bonded)',
                                'product_package_type_id' => 1,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 1,
                                'child_data' => [],
                            ],
                            [
                                'name' => 'Adhesive (bonded)',
                                'product_package_type_id' => 1,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 1,
                                'child_data' => [],
                            ],
                            [
                                'name' => 'Unbonded',
                                'product_package_type_id' => 1,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 1,
                                'child_data' => [],
                            ]
                        ],
                    ],
                    [
                        'name' => 'Waterproofing',
                        'product_package_type_id' => 1,
                        'is_main_sub_child' => 1,
                        'is_parent' => 1,
                        'child_data' => [
                            [
                                'name' => 'Liquid',
                                'product_package_type_id' => 1,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 1,
                                'child_data' => [],
                            ],
                            [
                                'name' => 'Fabric',
                                'product_package_type_id' => 1,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 1,
                                'child_data' => [],
                            ],
                        ],
                    ]
                ],
            ],
            [
                'name' => 'Radiant Heat',
                'product_package_type_id' => 1,
                'is_main_sub_child' => 0,
                'is_parent' => 1,
                'child_data' => [
                    [
                        'name' => 'Hydronic Heat',
                        'product_package_type_id' => 1,
                        'is_main_sub_child' => 0,
                        'is_secondary_sub_child' => 1,
                        'is_parent' => 1,
                        'child_data' => [],
                    ],
                    [
                        'name' => 'Electric Heat',
                        'product_package_type_id' => 1,
                        'is_main_sub_child' => 0,
                        'is_secondary_sub_child' => 1,
                        'is_parent' => 1,
                        'child_data' => [],
                    ]
                ],
            ],
            [
                'name' => 'Fastener',
                'product_package_type_id' => 2,
                'is_main_sub_child' => 0,
                'is_parent' => 0,
                'child_data' => [
                    [
                        'name' => 'Screws',
                        'product_package_type_id' => 2,
                        'is_main_sub_child' => 1,
                        'is_parent' => 0,
                        'child_data' => []
                    ],
                    [
                        'name' => 'Nails',
                        'product_package_type_id' => 2,
                        'is_main_sub_child' => 1,
                        'is_parent' => 0,
                        'child_data' => []
                    ]
                ]
            ],
            [
                'name' => 'Tapes',
                'product_package_type_id' => 4,
                'is_main_sub_child' => 0,
                'is_parent' => 0,
                'child_data' => [
                    [
                        'name' => 'Bond Breaker Tape',
                        'product_package_type_id' => 4,
                        'is_main_sub_child' => 1,
                        'is_parent' => 0,
                        'child_data' => []
                    ],
                    [
                        'name' => 'Mesh Tape',
                        'product_package_type_id' => 4,
                        'is_main_sub_child' => 1,
                        'is_parent' => 0,
                        'child_data' => []
                    ]
                ]
            ],
            [
                'name' => 'Thin Sets',
                'product_package_type_id' => 4,
                'is_main_sub_child' => 0,
                'is_parent' => 0,
                'child_data' => []
            ],
            [
                'name' => 'Membranes',
                'product_package_type_id' => 5,
                'is_main_sub_child' => 0,
                'is_parent' => 0,
                'child_data' => [
                    [
                        'name' => 'Cleavage',
                        'product_package_type_id' => 5,
                        'is_main_sub_child' => 1,
                        'is_parent' => 0,
                        'child_data' => [
                            [
                                'name' => 'Cleavage (bonded)',
                                'product_package_type_id' => 5,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 0,
                                'child_data' => [],
                            ],
                            [
                                'name' => 'Cleavage (unbonded)',
                                'product_package_type_id' => 5,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 0,
                                'child_data' => [],
                            ]
                        ],
                    ],
                    [
                        'name' => 'Crack Isolation',
                        'product_package_type_id' => 5,
                        'is_main_sub_child' => 1,
                        'is_parent' => 0,
                        'child_data' => [],
                    ],
                    [
                        'name' => 'Sound',
                        'product_package_type_id' => 5,
                        'is_main_sub_child' => 1,
                        'is_parent' => 0,
                        'child_data' => [],
                    ],
                    [
                        'name' => 'Uncoupling',
                        'product_package_type_id' => 5,
                        'is_main_sub_child' => 1,
                        'is_parent' => 0,
                        'child_data' => [
                            [
                                'name' => 'Thin set (bonded)',
                                'product_package_type_id' => 5,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 0,
                                'child_data' => [],
                            ],
                            [
                                'name' => 'Adhesive (bonded)',
                                'product_package_type_id' => 5,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 0,
                                'child_data' => [],
                            ],
                            [
                                'name' => 'Unbonded',
                                'product_package_type_id' => 5,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 0,
                                'child_data' => [],
                            ]
                        ],
                    ],
                    [
                        'name' => 'Waterproofing',
                        'product_package_type_id' => 5,
                        'is_main_sub_child' => 1,
                        'is_parent' => 0,
                        'child_data' => [
                            [
                                'name' => 'Liquid',
                                'product_package_type_id' => 5,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 0,
                                'child_data' => [],
                            ],
                            [
                                'name' => 'Fabric',
                                'product_package_type_id' => 5,
                                'is_main_sub_child' => 0,
                                'is_secondary_sub_child' => 1,
                                'is_parent' => 0,
                                'child_data' => [],
                            ],
                        ],
                    ]
                ],
            ],


        ];

        foreach ($productTypes as $productType) {
            $this->insertProductType($productType, null);
        }

    }

    private function insertProductType(array $productType, ?int $parentId)
    {
        $data = [
            'name' => $productType['name'],
            'product_package_type_id' => $productType['product_package_type_id'],
            'is_main_sub_child' => $productType['is_main_sub_child'],
            'is_secondary_sub_child' => array_key_exists('is_secondary_sub_child', $productType) ? $productType['is_secondary_sub_child'] : 0,
            'is_parent' => $productType['is_parent'],
            'parent_id' => $parentId,
        ];

        $productId = DB::table('product_types')->insertGetId($data);

        if (!empty($productType['child_data'])) {
            foreach ($productType['child_data'] as $childProductType) {
                $this->insertProductType($childProductType, $productId);
            }
        }

        return $productId;
    }
}
