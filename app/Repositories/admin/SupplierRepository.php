<?php

namespace App\Repositories\admin;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Interfaces\admin\SupplierRepositoryInterface;
use App\Models\Suppliers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

//use Your Model

/**
 * Class MenufacturerRepository.
 */
class SupplierRepository implements SupplierRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */

    public function createSupplier($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Supplier added successfully';
            $is_add = 1;
            $supplierData = new Suppliers();
            if (isset($data['id']) && !empty($data['id'])) {
                $supplierData = Suppliers::find($data['id']);
                $message = 'Supplier update successfully';
                $is_add = 0;
            }

            if (isset($data['logo'])) {
                $logo = $data['logo'];
                $extension = $logo->getClientOriginalExtension();
                $logoName = $logo->getFilename();
                $logo->move(public_path('/supplier'), $logoName.'.'.$extension);
                $supplierData->logo = $logo->getFilename().'.'.$extension;
            }

            $supplierData->name         = isset($data['name']) ? $data['name'] : $supplierData->name;
            $supplierData->location_url         = isset($data['location_url']) ? $data['location_url'] : $supplierData->location_url;
            $supplierData->manufacture_id         = $data['manufacture_id'];
            $supplierData->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Supplier Store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function deleteSupplier($id)
    {
        try {
            DB::beginTransaction();

            // DB::table('manufacturers')
            // ->whereRaw("FIND_IN_SET('$id', suppliers_ids)")
            // ->update(['suppliers_ids' => DB::raw("REPLACE(suppliers_ids, '$id', '')")]);

            Suppliers::find($id)->delete();

            DB::commit();
            return ['status' => true, 'message' =>  'Supplier delete successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error(' Supplier Delete : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function supplierEnableDisable($data)
    {
        try {
            DB::beginTransaction();

            $status = $data['status'] ? 0 : 1;
            Suppliers::where('id', $data['id'])->update(['status' => $status]);

            DB::commit();
            return ['status' => true, 'message' =>  'Supplier status updated successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('Supplier enable disable : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }
}
