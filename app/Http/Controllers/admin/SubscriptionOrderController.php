<?php

namespace App\Http\Controllers\admin;

use App\DataTables\SubscriptionOrderDataTable;
use App\Http\Controllers\Controller;
use App\Models\PurchasedSubsciptionPlan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class SubscriptionOrderController extends Controller
{
    public function subscriptionOrderList(SubscriptionOrderDataTable $dataTable)
    {
        return $dataTable->render('admin.subscriptionOrder.list');
    }

    public function subscriptionOrderView($id)
    {
        $id = Crypt::decrypt($id);
        $subscriptionOrder = PurchasedSubsciptionPlan::where('id', $id)->first();

        return view('admin.subscriptionOrder.view', compact('subscriptionOrder'));
    }
}
