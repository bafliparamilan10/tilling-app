<?php

namespace App\Interfaces\api;

interface SubscriptionRepositoryInterface
{
    public function subscriptionList(array $data);
    public function subscriptionPurchase(array $data);
    public function subscriptionCancel(array $data);
    public function subscriptionReceipts(array $data);
}
