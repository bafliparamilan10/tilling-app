<?php

namespace App\Repositories\admin;

use App\Interfaces\admin\ParentProductRepositoryInterface;
use App\Models\DataSheet;
use App\Models\ParentProduct;
use App\Models\ParentProductSupplier;
use App\Models\ProductPackageType;
use App\Models\ProductSizeLengthQty;
use App\Models\ProductSizeThickness;
use App\Models\ProductSizeWidthQty;
use App\Models\ProductType;
use App\Models\SpecificationFastners;
use App\Models\SpecificationFastnersNail;
use App\Models\SpecificationFastnersScrew;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class ParentProductRepository.
 */
class ParentProductRepository implements ParentProductRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function addDataSheet($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Data Sheet added successfully';
            $is_add = 1;
            $dataSheetData = new DataSheet();
            if (isset($data['id']) && !empty($data['id'])) {
                $dataSheetData = DataSheet::find($data['id']);
                $message = 'Data Sheet update successfully';
                $is_add = 0;
            }

            $dataSheetData->link         = isset($data['link']) ? $data['link'] : $dataSheetData->link;
            $dataSheetData->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $dataSheetData->id, 'link' => $dataSheetData->link]];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Data Sheet Store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function addProductSizeWidthQty($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Product size width added successfully';
            $is_add = 1;
            $ProductSizeWidthQty = new ProductSizeWidthQty();
            if (isset($data['id']) && !empty($data['id'])) {
                $ProductSizeWidthQty = DataSheet::find($data['id']);
                $message = 'Product size width update successfully';
                $is_add = 0;
            }

            $ProductSizeWidthQty->size         = isset($data['size']) ? $data['size'] : $ProductSizeWidthQty->size;
            $ProductSizeWidthQty->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $ProductSizeWidthQty->id, 'size' => $ProductSizeWidthQty->size]];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Product size store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function addProductSizeLengthQty($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Product size length added successfully';
            $is_add = 1;
            $ProductSizeLengthQty = new ProductSizeLengthQty();
            if (isset($data['id']) && !empty($data['id'])) {
                $ProductSizeLengthQty = ProductSizeLengthQty::find($data['id']);
                $message = 'Product size length update successfully';
                $is_add = 0;
            }

            $ProductSizeLengthQty->size         = isset($data['size']) ? $data['size'] : $ProductSizeLengthQty->size;
            $ProductSizeLengthQty->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $ProductSizeLengthQty->id, 'size' => $ProductSizeLengthQty->size]];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Product size length store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function addProductSizeThicknessQty($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Product size thickness added successfully';
            $is_add = 1;
            $productSizeThickness = new ProductSizeThickness();
            if (isset($data['id']) && !empty($data['id'])) {
                $productSizeThickness = ProductSizeThickness::find($data['id']);
                $message = 'Product size thickness update successfully';
                $is_add = 0;
            }

            $productSizeThickness->size         = isset($data['size']) ? $data['size'] : $productSizeThickness->size;
            $productSizeThickness->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $productSizeThickness->id, 'size' => $productSizeThickness->size]];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Product size thickness store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function addSpecFastener($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Spec. fasteners added successfully';
            $is_add = 1;
            $specificationFasteners = new SpecificationFastners();
            if (isset($data['id']) && !empty($data['id'])) {
                $specificationFasteners = SpecificationFastners::find($data['id']);
                $message = 'Spec. fasteners update successfully';
                $is_add = 0;
            }

            $specificationFasteners->squer_foot         = isset($data['squer_foot']) ? $data['squer_foot'] : $specificationFasteners->squer_foot;
            $specificationFasteners->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $specificationFasteners->id, 'squer_foot' => $specificationFasteners->squer_foot]];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Spec. fasteners store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function addSpecFastenerScrew($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Spec. fasteners screw added successfully';
            $is_add = 1;
            $specificationFastnersScrew = new SpecificationFastnersScrew();
            if (isset($data['id']) && !empty($data['id'])) {
                $specificationFastnersScrew = SpecificationFastnersScrew::find($data['id']);
                $message = 'Spec. fasteners screw update successfully';
                $is_add = 0;
            }

            $specificationFastnersScrew->length_inch         = isset($data['length_inch']) ? $data['length_inch'] : $specificationFastnersScrew->length_inch;
            $specificationFastnersScrew->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $specificationFastnersScrew->id, 'length_inch' => $specificationFastnersScrew->length_inch]];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Spec. fasteners screw store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function addSpecFastenerNail($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Spec. fasteners nail added successfully';
            $is_add = 1;
            $specificationFastnersNail = new SpecificationFastnersNail();
            if (isset($data['id']) && !empty($data['id'])) {
                $specificationFastnersNail = SpecificationFastnersNail::find($data['id']);
                $message = 'Spec. fasteners nail update successfully';
                $is_add = 0;
            }

            $specificationFastnersNail->length_inch         = isset($data['length_inch']) ? $data['length_inch'] : $specificationFastnersNail->length_inch;
            $specificationFastnersNail->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $specificationFastnersNail->id, 'length_inch' => $specificationFastnersNail->length_inch]];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Spec. fasteners nail store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function parentProductPackageTypeCreate($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Parent product package type added successfully';
            $is_add = 1;
            $parentProductType = new ProductPackageType();
            if (isset($data['id']) && !empty($data['id'])) {
                $parentProductType = ProductPackageType::find($data['id']);
                $message = 'Parent product package type update successfully';
                $is_add = 0;
            }

            $parentProductType->name         = isset($data['parent_product_package_type']) ? $data['parent_product_package_type'] : $parentProductType->parent_product_package_type;
            $parentProductType->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $parentProductType->id, 'parent_product_package_type' => $parentProductType->name]];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Parent product package type store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function mainSubParentProductTypeCreate($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Main sub parent product type added successfully';
            $is_add = 1;
            $productType = new ProductType();
            if (isset($data['id']) && !empty($data['id'])) {
                $productType = ProductType::find($data['id']);
                $message = 'Main sub parent product type update successfully';
                $is_add = 0;
            }

            $productType->name         = isset($data['main_sub_parent_product_type']) ? $data['main_sub_parent_product_type'] : $productType->name;
            $productType->parent_id         =  $data['parent_id'];
            $productType->is_main_sub_child         =  1;
            $productType->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $productType->id, 'main_sub_parent_product_type' => $productType->name]];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Main sub parent product type store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }


    public function secondarySubParentProductTypeCreate($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Secondary sub parent product type added successfully';
            $is_add = 1;
            $productType = new ProductType();
            if (isset($data['id']) && !empty($data['id'])) {
                $productType = ProductType::find($data['id']);
                $message = 'Secondary sub parent product type update successfully';
                $is_add = 0;
            }

            $productType->name         = isset($data['secondary_sub_parent_product_type']) ? $data['secondary_sub_parent_product_type'] : $productType->name;
            $productType->parent_id         =  $data['main_sub_child'];
            $productType->is_secondary_sub_child         =  1;
            $productType->save();

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add, 'data' => ['id' => $productType->id, 'secondary_sub_parent_product_type' => $productType->name]];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Secondary sub parent product type store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }


    public function parentProductCreate($data)
    {
        try {
            DB::beginTransaction();

            $message = 'Parent product added successfully';
            $is_add = 1;
            $parentProduct = new ParentProduct();
            if (isset($data['id']) && !empty($data['id'])) {
                $parentProduct = ParentProduct::find($data['id']);
                $message = 'Parent product update successfully';
                $is_add = 0;
            }

            if (isset($data['logo'])) {
                $logo = $data['logo'];
                $extension = $logo->getClientOriginalExtension();
                $logoName = $logo->getFilename();
                $logo->move(public_path('/parent_product'), $logoName.'.'.$extension);
                $parentProduct->logo = $logo->getFilename().'.'.$extension;
            }

            $parentProduct->manufacture_id         = isset($data['manufacture_id']) ? $data['manufacture_id'] : $parentProduct->manufacture_id;
            $parentProduct->name         = isset($data['name']) ? $data['name'] : $parentProduct->name;
            $parentProduct->product_area         = isset($data['product_area']) ? $data['product_area'] : $parentProduct->product_area;
            $parentProduct->ansi_id         = isset($data['ansi_id']) ? $data['ansi_id'] : $parentProduct->ansi_id;
            $parentProduct->data_sheet_id         = isset($data['data_sheet_ids']) ? $data['data_sheet_ids'][0] : $parentProduct->data_sheet_id;
            $parentProduct->data_sheet_ids         = isset($data['data_sheet_ids']) ? implode(",",$data['data_sheet_ids']) : $parentProduct->data_sheet_ids;
            $parentProduct->product_type_id         = isset($data['product_type']) ? $data['product_type'] : $parentProduct->product_type;
            $parentProduct->product_type_main_sub_child_id         = isset($data['product_type_main_sub_child_id']) ? $data['product_type_main_sub_child_id'] : $parentProduct->product_type_main_sub_child_id;
            $parentProduct->product_type_secondary_sub_child_id         = isset($data['product_type_secondary_sub_child_id']) ? $data['product_type_secondary_sub_child_id'] : $parentProduct->product_type_secondary_sub_child_id;
            $parentProduct->product_package_type_id         = isset($data['get_product_package_type_id']) ? $data['get_product_package_type_id'] : $parentProduct->get_product_package_type_id;

            $parentProduct->save();

            $parent_product_id = $parentProduct->id;


            $productLink=$data['product_link'];
            $insertData = array();
            foreach($productLink as $key => $productLinkValue)
            {
                if(!empty($productLinkValue))
                {
                    $parentProductSupplierId = isset($data['parent_product_supplier_id'][$key]) ? $data['parent_product_supplier_id'][$key] : NULL;

                    $insertData[] =[
                        'id' => $parentProductSupplierId,
                        'parent_product_id' => $parent_product_id,
                        'supplier_id' => $data['supplier_ids'][$key],
                        'product_links' => $productLinkValue,
                        'product_mdl' => $data['product_mdl'][$key],
                        'product_size_width_qty_id' => $data['product_size_width_id'][$key],
                        'product_size_length_qty_id' => $data['product_size_length_id'][$key],
                        'product_size_thickness_id' => $data['product_size_thickness_id'][$key],
                        'specification_fastners_id' => $data['specification_fastener_id'][$key],
                        'specification_fastners_screw_id' => $data['specification_fastener_screw_id'][$key],
                        'specification_fastners_nails_id' => $data['specification_fastener_nail_id'][$key],
                        'product_price' => $data['product_price'][$key],
                        'product_price_per_sq' => $data['product_price_per_sq'][$key],
                        'status' => $data['status'][$key],
                    ];
                }
            }


            $chunkCollection = collect($insertData);
            $chunks = $chunkCollection->chunk(20);
            foreach($chunks as $chunk){

                // ParentProductSupplier::insert($chunk->toArray());
                ParentProductSupplier::upsert($chunk->toArray(), ['id'], ['parent_product_id', 'supplier_id', 'product_links', 'product_mdl', 'product_size_width_qty_id', 'product_size_length_qty_id', 'product_size_thickness_id', 'specification_fastners_id', 'specification_fastners_screw_id', 'specification_fastners_nails_id', 'product_price', 'product_price_per_sq', 'status']);
            }

            DB::commit();
            return ['status' => true, 'message' =>  $message, 'is_add' => $is_add];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Parent product store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function parentProductTypeCreate($data)
    {
        try {
            DB::beginTransaction();

            $mainSubChildData = [];
            $secondarySubChildData = [];

            $message = 'Parent product type added successfully';

            $productType = ProductType::where('name',$data['product_type_name']);


            if($productType->count() > 0){
                $productTypeData = $productType->first();
            }
            else
            {
                $productTypeData = new ProductType();

                $productTypeData->name         = isset($data['product_type_name']) ? $data['product_type_name'] : $productTypeData->name;
                $productTypeData->product_package_type_id         = isset($data['product_package_type_id']) ? $data['product_package_type_id'] : $productTypeData->product_package_type_id;
                $productTypeData->save();

            }

            $resData = ['id' => $productTypeData->id, 'product_type_name' => $productTypeData->name, 'main_sub_child_name' => '', 'secondary_sub_child_name' => ''];


            if(isset($data['main_sub_child'])){

                $mainSubChildData = ProductType::where(['id' => $data['main_sub_child']])->first();
                $resData['main_sub_child_name'] = $mainSubChildData->name;

            }

            if(isset($data['secondary_sub_child'])){

                $secondarySubChildData = ProductType::where(['id' => $data['secondary_sub_child']])->first();
                $resData['secondary_sub_child_name'] = $secondarySubChildData->name;
            }


            DB::commit();

            return ['status' => true, 'message' =>  $message, 'data' => $resData];
        } catch (\Throwable $e) {
            DB::rollBack();
            Log::error('Parent product type store : ', ['error' => $e]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function parentProductEnableDisable($data)
    {
        try {
            DB::beginTransaction();

            $status = $data['status'] ? 0 : 1;
            ParentProduct::where('id', $data['id'])->update(['status' => $status]);

            DB::commit();
            return ['status' => true, 'message' =>  'Parent product status updated successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('Parent product enable disable : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function deleteParentProduct($id)
    {
        try {
            DB::beginTransaction();

            ParentProduct::find($id)->delete();

            DB::commit();
            return ['status' => true, 'message' =>  'Parent product delete successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error(' Parent product Delete : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }

    public function updateDefaultDataSheet($data)
    {
        try {
            DB::beginTransaction();

            ParentProduct::where('id', $data['id'])->update(['data_sheet_id' => $data['data_sheet_id']]);

            DB::commit();
            return ['status' => true, 'message' =>  'Default datasheet updated successfully'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('Default datasheet : ', ['error' => $th]);
            return ['status' => false, 'message' =>  'Something went wrong'];
        }
    }
}
