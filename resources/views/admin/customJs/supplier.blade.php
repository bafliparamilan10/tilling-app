<script>
    $(function () {
        $('.select2').select2();
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });


        $('#logo').change(function(){

            let reader = new FileReader();

            reader.onload = (e) => {

            $('#preview-image-before-upload').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);

        });

        $('#supplierForm').validate({
            rules: {
                name: {
                    required: true,
                },
                status: {
                    required: true,
                }
            },
            messages: {
                name: {
                    required: "Please enter supplier name"
                },
                status: {
                   required: "please select status"
                }

            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function (form) {
                var form = $('#supplierForm')[0];
                var data = new FormData(form);
                submitForm(data);
            }
        });

        function submitForm(form) {
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                url: '{{ route('admin.supplier-create') }}',
                type: 'post',
                processData: false,
                contentType: false,
                data: form,
                beforeSend: function() {
                $(".data-loader").addClass('loading');
                },
                success: function(response) {
                    if (response.status == true) {
                        $(".data-loader").removeClass('loading');
                        toastr.success('{{ config('app.name') }}', response.message)
                        setTimeout(() => {
                            window.location.href = '{{ route('admin.supplier-list') }}';
                        }, 1000);
                    } else {
                        $(".data-loader").removeClass('loading');
                        toastr.error('{{ config('app.name') }}', response.message)
                    }
                },
                error: function(data) {
                    $(".data-loader").removeClass('loading');
                    errorHandler(data);
                }
            });
    }

    });

    function deleteSupplier(id)
    {
        Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#5B73E8',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                url:"{{route('admin.supplier-delete')}}",
                type: 'delete',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {_method: 'Delete', id:id},
                success:function(msg){
                    console.log(msg);
                    if(msg.status == true){
                        Swal.fire("Deleted!",  msg.message, "success");
                        window.LaravelDataTables["supplier-table"].ajax.reload();
                    }else{
                        Swal.fire("Warning!", msg.message, "warning");
                    }
                },
                error:function(){
                    Swal.fire("Error!", 'Error in delete Record', "error");
                }
                });
            }
        })
    };

    function activeDeactive(status, id){

        var lable = status == 0 ? 'active' : 'inactive';
        Swal.fire({
            title: 'Are you sure?',
            text: "You wont to be able to "+ lable +" this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#5B73E8',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, change it!'
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                    url:"{{route('admin.supplier-enable-disable')}}",
                    type: 'post',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {status: status, id:id},
                    success:function(msg){
                        console.log(msg);
                        if(msg.status == true){
                            Swal.fire("Change!",  msg.message, "success");
                            window.LaravelDataTables["supplier-table"].ajax.reload();
                        }else{
                            Swal.fire("Warning!", msg.message, "warning");
                        }
                    },
                    error:function(){
                        Swal.fire("Error!", 'Error in delete Record', "error");
                    }
                    });
                }
            })
        }
</script>
