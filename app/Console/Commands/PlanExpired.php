<?php

namespace App\Console\Commands;

use App\Models\PurchasedSubsciptionPlan;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PlanExpired extends Command
{
    protected $signature = 'app:plan-expired';

    protected $description = 'Plan Expired';

    public function handle()
    {
        PurchasedSubsciptionPlan::where('end_date',date('Y-m-d'))->update([
            'status' => 'expire'
        ]);
        
        Log::info("plan expire job");
    }
}

