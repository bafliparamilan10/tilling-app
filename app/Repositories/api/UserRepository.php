<?php

namespace App\Repositories\api;

use App\Interfaces\api\UserRepositoryInterface;
use App\Jobs\SendEmailJob;
use App\Models\City;
use App\Models\PurchasedSubsciptionPlan;
use App\Models\State;
use App\Models\SupplierFavorite;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use stdClass;
use Str;

class UserRepository implements UserRepositoryInterface
{
    public function model()
    {
        //return YourModel::class;
    }

    public function stateList($data)
    {
        try {
            DB::beginTransaction();

            $states = State::select('id','name')->where('country_id',233)->get();
            
            DB::commit();
            return response(['message' => 'States list get successfully','data' => $states] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - State List : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }
    
    public function cityList($data)
    {
        try {
            DB::beginTransaction();
            
            $cities = City::select('id','name')->where('state_id',$data['state_id'])->get();
            
            DB::commit();
            return response(['message' => 'City list get successfully','data' => $cities] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - City List : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function signUp($data)
    {
        try {
            DB::beginTransaction();

            $user = [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'user_type' => $data['user_type'],
                'city_id' => $data['city_id'],
                'state_id' => $data['state_id'],
                'date_of_birth' => $data['date_of_birth'],
                'gender' => $data['gender'],
                'status' => $data['status'],
                'device_type' => isset($data['device_type']) ?? null,
                'device_token' => isset($data['device_token']) ?? null,
            ];

            $mailPassword = $data['password'];
            $password = Hash::make($data['password']);
            $user['password'] = $password;

            $user = User::create($user);
            $user['token'] = $user->createToken('TilingMyWay')->accessToken;

            $email_data = [
                'email' => $data['email'],
                'password' => $mailPassword,
            ];
            $template = "email.api_user_signup";
            $subject = "Registration Success";
            dispatch(new SendEmailJob($template, $email_data, $subject, $type = 'user_signup_api'));

            DB::commit();
            return response(['message' => 'User register successfully','data' => $user] ,200);
        }
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Signup : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function emailExists($data)
    {
        try {
            DB::beginTransaction();

            $email = $data['email'];

            $email_exists = User::where('email',$email);

            if($email_exists->exists()){
                return response(['message' => 'This email already exist!', 'data' => new stdClass()] ,422);
            }

            DB::commit();
            return response(['message' => 'Email not found','data' => new stdClass()] ,200);
        }
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - EmailExists : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function login($data)
    {
        try {
            DB::beginTransaction();

            $email = $data['email'];
            $password = $data['password'];
            $user = User::where('email', $email)->first();

            if(!$user){
                return response(['message' => 'User does not exist', 'data' => new stdClass()] ,422);
            }

            if (!Hash::check($password, $user->password)){
                return response(['message' => 'Password mismatch', 'data' => new stdClass()] ,422);
            } 

            DB::table('oauth_access_tokens')->where('user_id', $user->id)->delete();

            $user->update([
                'device_type' => isset($data['device_type']) ? $data['device_type'] : $user->device_type,
                'device_token' => isset($data['device_token']) ? $data['device_token'] : $user->device_token,
            ]);
            
            $response['email'] = $user->email;
            $response['device_type'] = $user->device_type;
            $response['device_token'] = $user->device_token;
            $response['is_supplier_selected'] = SupplierFavorite::where('user_id',$user->id)->exists() ? 1 : 0;
            $response['is_method_selected'] = 0;
            $response['is_notification'] = $user['notification'];
            $response['is_subscribed'] = PurchasedSubsciptionPlan::where('user_id',$user->id)->exists() ? 1 : 0;
            $response['token'] = $user->createToken('TilingMyWay')->accessToken;
            
            DB::commit();
            return response(['message' => 'User login successfully','data' => $response] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Login : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function notificationUpdate($data)
    {
        try {
            DB::beginTransaction();

            $auth_id = Auth::user()->id;
            
            User::where('id',$auth_id)->update([
                'notification' => $data['notification']
            ]);
            
            DB::commit();
            return response(['message' => 'Notification update successfully','data' => new stdClass()] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Notification Update : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function logout($data)
    {
        try {
            DB::beginTransaction();

            $access_token = auth()->user()->token();

            $access_token->revoke();
            
            DB::commit();
            return response(['message' => 'User logut successfully','data' => new stdClass()] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Logout : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function changePassword($data)
    {
        try {
            DB::beginTransaction();

            $id = auth()->user()->id;

            if(!Hash::check($data['current_password'],auth()->user()->password)){
                return response(['message' => 'Current password wrong', 'data' => new stdClass()] ,422);
            }

            User::where('id',$id)->update([
                'password' => Hash::make($data['new_password'])
            ]);

            $data = User::where('id',$id)->first();

            DB::commit();
            return response(['message' => 'Change password successfully','data' => $data] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Change Password : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function forgotPassword($data)
    {
        try {
            DB::beginTransaction();

            $token = Str::random(64);
  
            DB::table('password_resets')->insert([
              'email' => $data['email'],
              'token' => $token, 
              'created_at' => Carbon::now()
            ]);
                
            // $url = URL::to('/reset-password/'.$token);
            $url =  'https://tillingapp.aistechnolabs.co/reset-password/'.$token;
            
            $email_data = [
                'email' => $data['email'],
                'url' => $url,
            ];

            $template = "email.api_user_forgot_passsword";
            $subject = "Forgot Password";
            dispatch(new SendEmailJob($template, $email_data, $subject, $type = 'user_forgot_password_api'));
           
            DB::commit();
            return response(['message' => 'Forgot password email send successfully','data' => new stdClass()] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Forgot Password : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function resetPassword($data)
    {
        try {
            DB::beginTransaction();

            $record = DB::table('password_resets')->where('token', $data['token']);
            $passwordReset = DB::table('password_resets')->where('token', $data['token'])->first();
            if (!$passwordReset) {
                return response(['message' => 'Invalid token','data' => new stdClass()] ,422);
            }
    
            $user = User::where('email', $passwordReset->email)->first();
            if (!$user) {
                return response()->json(['message' => 'User not found','data' => new stdClass()] ,422);
            }

            $user->update(['password' => Hash::make($data['new_password']) ]);
            $record->delete();

            DB::commit();
            return response(['message' => 'Reset password successfully','data' => new stdClass()] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Reset Password : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function profileGet($data)
    {
        try {
            DB::beginTransaction();

            $id = auth()->user()->id;
            $user = User::with(['getStateDetails','getCityDetails'])->where('id',$id)->first();

            if (!$user) {
                return response(['message' => 'Profile not found!', 'data' => new stdClass()] ,422);
            }

            DB::commit();
            return response(['message' => 'Profile get successfully','data' => $user] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Profile Get : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function profileUpdate($data)
    {
        try {
            DB::beginTransaction();

            $user = User::where('id',auth()->user()->id)->first();

            if (!$user) {
                return response(['message' => 'Profile not found!', 'data' => new stdClass()] ,422);
            }

            $user->update([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'date_of_birth' => $data['date_of_birth'],
                'state_id' => $data['state_id'],
                'city_id' => $data['city_id'],
                'user_type' => !empty($data['user_type']) ? $data['user_type'] : $user->user_type,
            ]);

            $data = User::with(['getStateDetails','getCityDetails'])->where('id',$user->id)->first();

            DB::commit();
            return response(['message' => 'Profile update successfully','data' => $data] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Profile Update : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }

    public function deleteAccount($data)
    {
        try {
            DB::beginTransaction();

            $user = User::where('id',auth()->user()->id)->first();

            if (!$user) {
                return response(['message' => 'User not found!', 'data' => new stdClass()] ,422);
            }

            $user->delete();

            DB::commit();
            return response(['message' => 'Account deleted successfully','data' => new stdClass()] ,200);
        } 
        catch (Exception $e) {
            DB::rollBack();
            Log::error('API - Delete Account : ', ['error' => $e->getMessage()]);
            return response(['message' => 'Something went wrong', 'data' => new stdClass()] ,500);
        }
    }
}
