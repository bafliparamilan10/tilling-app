<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('thin_set_qty_types', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable();
            $table->boolean('status')->default(1)->comment('1 = active, 0 = deactive');
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('thin_set_qty_types');
    }
};
