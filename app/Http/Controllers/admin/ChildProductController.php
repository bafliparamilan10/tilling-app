<?php

namespace App\Http\Controllers\admin;

use App\DataTables\ChildProductDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\CreateChildProductRequest;
use App\Http\Requests\admin\ProductColorRequest;
use App\Http\Requests\admin\ProductOverageRequest;
use App\Http\Requests\admin\ProductSizeCoverageRequest;
use App\Http\Requests\admin\ProductSizeGallonRequest;
use App\Http\Requests\admin\ProductSizeLbsRequest;
use App\Interfaces\admin\ChildProductRepositoryInterface;
use App\Models\Ansi;
use App\Models\ChildProduct;
use App\Models\ChildProductSupplier;
use App\Models\DataSheet;
use App\Models\Manufacturer;
use App\Models\Overage;
use App\Models\ProdctSizeCoverageQty;
use App\Models\ProdctSizeGallon;
use App\Models\ProductColor;
use App\Models\ProductPackageType;
use App\Models\ProductSizeLbs;
use App\Models\ProductSizeLengthQty;
use App\Models\ProductSizeThickness;
use App\Models\ProductSizeWidthQty;
use App\Models\ProductType;
use App\Models\SpecificationFastners;
use App\Models\SpecificationFastnersNail;
use App\Models\SpecificationFastnersScrew;
use App\Models\Suppliers;
use App\Models\ThinSetQtyType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ChildProductController extends Controller
{
    private ChildProductRepositoryInterface $childProductRepository;

    public function __construct(ChildProductRepositoryInterface $childProductRepository)
    {
        $this->childProductRepository = $childProductRepository;
    }

    public function listChildProduct(ChildProductDataTable $dataTable)
    {
        return $dataTable->render('admin.childProduct.list');
    }

    public function addChildProduct()
    {
        $manufacturer = Manufacturer::where('status',1)->get();
        $ansi = Ansi::where('status',1)->get();
        $dataSheets = DataSheet::where('status',1)->get();
        $suppliers = Suppliers::where('status',1)->get();
        $productPackageType = ProductPackageType::where('status',1)->get();
        $productType = ProductType::where(['status' => 1, 'is_parent' => 0])->whereNull('parent_id')->get();
        $productSizeWidthQty = ProductSizeWidthQty::all();
        $productSizeLengthQty = ProductSizeLengthQty::all();
        $productSizeThickness = ProductSizeThickness::all();
        $specificationFasteners = SpecificationFastners::all();
        $specificationFastenersScrew = SpecificationFastnersScrew::all();
        $specificationFastenersNail = SpecificationFastnersNail::all();
        $thin_set_qty_types = ThinSetQtyType::where('status',1)->get();
        $product_color = ProductColor::all();
        $productSizeLbs = ProductSizeLbs::all();
        $productSizeGallon = ProdctSizeGallon::all();
        $productSizeCoverageQty = ProdctSizeCoverageQty::all();
        $productOverage = Overage::all();

        return view('admin.childProduct.add',compact('manufacturer', 'ansi', 'dataSheets', 'suppliers', 'productPackageType', 'productType', 'productSizeWidthQty', 'productSizeLengthQty', 'productSizeThickness', 'specificationFasteners', 'specificationFastenersScrew', 'specificationFastenersNail','thin_set_qty_types','product_color','productSizeLbs','productSizeGallon','productSizeCoverageQty','productOverage'));
    }

    public function addChildProductSupplier(Request $request)
    {
        $index = $request->index;
        $ansi_index = $request->ansi_index;
        $suppliers = Suppliers::where('status',1)->get();
        $ansi = Ansi::where('status',1)->get();
        $productPackageType = ProductPackageType::where('status',1)->get();
        $productSizeWidthQty = ProductSizeWidthQty::all();
        $productSizeLengthQty = ProductSizeLengthQty::all();
        $productSizeThickness = ProductSizeThickness::all();
        $specificationFasteners = SpecificationFastners::all();
        $specificationFastenersScrew = SpecificationFastnersScrew::all();
        $specificationFastenersNail = SpecificationFastnersNail::all();
        $thin_set_qty_types = ThinSetQtyType::where('status',1)->get();
        $product_color = ProductColor::all();
        $productSizeLbs = ProductSizeLbs::all();
        $productSizeGallon = ProdctSizeGallon::all();
        $productSizeCoverageQty = ProdctSizeCoverageQty::all();
        $productOverage = Overage::all();

        return view('admin.childProduct.add-supplier',compact('suppliers','ansi','productPackageType', 'productSizeWidthQty', 'productSizeLengthQty', 'productSizeThickness', 'specificationFasteners', 'specificationFastenersScrew', 'specificationFastenersNail', 'index', 'ansi_index','thin_set_qty_types','product_color','productSizeLbs','productSizeGallon','productSizeCoverageQty','productOverage'));
    }

    public function editChildProduct($id)
    {
        $id = Crypt::decrypt($id);
        $childProduct = ChildProduct::where('id',$id)->first();
        $manufacturer = Manufacturer::where('status',1)->get();
        $ansi = Ansi::where('status',1)->get();
        $dataSheets = DataSheet::where('status',1)->get();
        $suppliers = Suppliers::where('status',1)->get();
        $productPackageType = ProductPackageType::where('status',1)->get();
        $productType = ProductType::where(['status' => 1, 'is_parent' => 0])->whereNull('parent_id')->get();
        $productSizeWidthQty = ProductSizeWidthQty::all();
        $productSizeLengthQty = ProductSizeLengthQty::all();
        $productSizeThickness = ProductSizeThickness::all();
        $specificationFasteners = SpecificationFastners::all();
        $specificationFastenersScrew = SpecificationFastnersScrew::all();
        $specificationFastenersNail = SpecificationFastnersNail::all();
        $thin_set_qty_types = ThinSetQtyType::where('status',1)->get();
        $product_color = ProductColor::all();
        $productSizeLbs = ProductSizeLbs::all();
        $productSizeGallon = ProdctSizeGallon::all();
        $productSizeCoverageQty = ProdctSizeCoverageQty::all();
        $productOverage = Overage::all();

        return view('admin.childProduct.edit',compact('childProduct','manufacturer', 'ansi', 'dataSheets', 'suppliers', 'productPackageType', 'productType', 'productSizeWidthQty', 'productSizeLengthQty', 'productSizeThickness', 'specificationFasteners', 'specificationFastenersScrew', 'specificationFastenersNail','thin_set_qty_types','product_color','productSizeLbs','productSizeGallon','productSizeCoverageQty','productOverage'));
    }


    public function addChildProductTrowel(Request $request)
    {
        $product_color = ProductColor::all();
        $trowelIndex = $request->trowel_index;
        $index = $request->index;

        return view('admin.childProduct.add-trowel',compact('product_color','index','trowelIndex'));
    }


    public function storeChildProduct(CreateChildProductRequest $request)
    {
        $data =  $this->childProductRepository->storeChildProduct($request->all());
        return $data;
    }

    public function viewChildProduct($id)
    {
        $id = Crypt::decrypt($id);
        $childProduct = ChildProduct::where('id', $id)->first();
    
        return view('admin.childProduct.view', compact('childProduct'));
    }

    public function deleteChildProduct(Request $request)
    {
        $data =  $this->childProductRepository->deleteChildProduct($request['id']);
        return $data;
    }

    public function enableDisableChildProduct(Request $request)
    {
        $response = $this->childProductRepository->enableDisableChildProduct($request->all());
        return  $response;
    }

    public function createColorChildProduct(ProductColorRequest $request)
    {
        $data =  $this->childProductRepository->createColorChildProduct($request->all());
        return $data;
    }

    public function createProductSizeLbs(ProductSizeLbsRequest $request)
    {
        $data =  $this->childProductRepository->createProductSizeLbs($request->all());
        return $data;
    }

    public function createProductSizeGallon(ProductSizeGallonRequest $request)
    {
        $data =  $this->childProductRepository->createProductSizeGallon($request->all());
        return $data;
    }

    public function createProductSizeCoverage(ProductSizeCoverageRequest $request)
    {
        $data =  $this->childProductRepository->createProductSizeCoverage($request->all());
        return $data;
    }

    public function createProductOverage(ProductOverageRequest $request)
    {
        $data =  $this->childProductRepository->createProductOverage($request->all());
        return $data;
    }

    public function updateDefaultDataSheet(Request $request)
    {
        $data =  $this->childProductRepository->updateDefaultDataSheet($request->all());
        return $data;
    }

    public function getSupplierData(Request $request)
    {
        return ChildProductSupplier::where('id',$request->supplier_id)->first();
    }

    public function getSupplierViewData(Request $request)
    {
        return ChildProductSupplier::with('getProductSizeWidthQtyData','getProductSizeLengthQtyData','getProductSizeThicknessQtyData','getSpecificationFastnersData','getSpecificationFastnersScrewData','getSpecificationFastnersNailsData','getTrowelData','getProductSizeLbs','getProductSizeGallon','getProductSizeCoverage','getProductSizeOverage','getProductColor','getTrowelDefault')->where('id',$request->id)->first();
    }

}
