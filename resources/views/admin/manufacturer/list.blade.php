@extends('admin.layouts.master')
@section('page_title', 'Manufacturer Management')
@section('content_header')

@push('customCSS')
<link  href="{{ asset('admin/dist/css/datatables/jquery.dataTables.min.css') }}" rel="stylesheet">
<link  href="{{ asset('admin/dist/css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link  href="{{ asset('admin/dist/css/datatables/buttons.dataTables.min.css') }}" rel="stylesheet">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Manufacturer Management</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Manufacturer Management</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection
@section('content')
<section class="content">

    <div class="container-fluid">

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Manufacturer Management</h3>
                <a href="{{route('admin.manufacturer-add')}}" class="btn btn-outline-primary float-right"><i class="fa fa-plus"></i> Add Manufacturer </a>
              </div>

              <!-- ./card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  {{$dataTable->table(['class' => 'table table-bordered  table-striped ', 'id' => 'manufacturer-table'], true)}}
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div>

</section>
@endsection

@push('scripts')


    <!-- SweetAlert2 -->
    <script src="{{ asset('admin/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script src="{{ asset('admin/dist/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/dist/js/datatables/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/dist/js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/dist/js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/buttons.server-side.js')}}"></script>

    {{$dataTable->scripts()}}

@endpush
@push('customJs')
<!-- jquery-validation -->
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
@include('admin.customJs.manufacturer')
@endpush
