<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;


class PurchasedSubsciptionPlan extends Model
{
    use HasFactory , SoftDeletes;
    protected $guarded = [];

    public function getPlanDetails()
    {
        return $this->belongsTo(SubsciptionPlan::class, 'subscription_plan_id', 'id');
    }

    public function getUserDetails()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    protected function pdf(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value ? asset('subscription/'.$value) : null,
        );
    }
}
