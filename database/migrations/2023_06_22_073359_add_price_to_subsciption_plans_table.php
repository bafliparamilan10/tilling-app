<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('subsciption_plans', function (Blueprint $table) {
            $table->decimal('price')->nullable()->after('type');
        });
    }

    public function down(): void
    {
        Schema::table('subsciption_plans', function (Blueprint $table) {
            $table->dropColumn('notification');
        });
    }
};
