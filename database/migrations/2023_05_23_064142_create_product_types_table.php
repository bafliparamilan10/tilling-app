<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_types', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('product_types')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('is_main_sub_child')->default(0)->comment('1 = yes 0 = no');
            $table->boolean('is_secondary_sub_child')->default(0)->comment('1 = yes 0 = no');
            $table->unsignedBigInteger('product_package_type_id')->nullable();
            $table->foreign('product_package_type_id')->references('id')->on('product_package_types')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('is_parent')->default(1)->comment('1 = parent, 0 = child')->comment('1= it is used for parent product, 0 = it is used for child product');
            $table->boolean('status')->default(1)->comment('1 = active, 0 = deactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_types');
    }
};
