<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('method_filters', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('method_filters')->onDelete('cascade')->onUpdate('cascade');
            $table->string('type', 50)->default('exposure')->comment('exposure, substrates, underlayments, tile, price');
            $table->boolean('is_lable')->default(0)->comment('1 = yes 0 = no');
            $table->boolean('is_checkbox')->default(0)->comment('1 = yes 0 = no');
            $table->boolean('is_lock')->default(0)->comment('1 = yes 0 = no');
            $table->boolean('is_default_filter')->default(0)->comment('1 = yes 0 = no')->comment('for display default filters');
            $table->boolean('status')->default(1)->comment('1 = active, 0 = deactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('method_filters');
    }
};
