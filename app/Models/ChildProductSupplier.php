<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChildProductSupplier extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getSupplierDetails(){
        return $this->belongsTo(Suppliers::class, 'supplier_id', 'id');
    }

    public function getProductSizeWidthQtyData(){
        return $this->belongsTo(ProductSizeWidthQty::class, 'product_size_width_qty_id', 'id');
    }

    public function getProductSizeLengthQtyData(){
        return $this->belongsTo(ProductSizeLengthQty::class, 'product_size_length_qty_id', 'id');
    }

    public function getProductSizeThicknessQtyData(){
        return $this->belongsTo(ProductSizeThickness::class, 'product_size_thickness_id', 'id');
    }

    public function getSpecificationFastnersData(){
        return $this->belongsTo(SpecificationFastners::class, 'specification_fastners_id', 'id');
    }

    public function getSpecificationFastnersScrewData(){
        return $this->belongsTo(SpecificationFastnersScrew::class, 'specification_fastners_screw_id', 'id');
    }

    public function getSpecificationFastnersNailsData(){
        return $this->belongsTo(SpecificationFastnersNail::class, 'specification_fastners_nails_id', 'id');
    }

    public function getTrowelData()
    {
        return $this->hasMany(Trowel::class, 'child_product_supplier_id', 'id');
    }

    public function getProductSizeLbs(){
        return $this->belongsTo(ProductSizeLbs::class, 'product_size_lbs_id', 'id');
    }

    public function getProductSizeGallon(){
        return $this->belongsTo(ProdctSizeGallon::class, 'prodct_size_gallon_id', 'id');
    }

    public function getProductSizeCoverage(){
        return $this->belongsTo(ProdctSizeCoverageQty::class, 'prodct_size_coverage_qtie_id', 'id');
    }

    public function getProductSizeOverage(){
        return $this->belongsTo(Overage::class, 'overage_id', 'id');
    }

    public function getProductColor(){
        return $this->belongsTo(ProductColor::class, 'product_color_id', 'id');
    }

    public function getTrowelDefault()
    {
        return $this->hasOne(Trowel::class, 'child_product_supplier_id', 'id')->where('is_default', 1);
    }
}
