<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Manufacturer;
use App\Models\ParentProduct;
use App\Models\PurchasedSubsciptionPlan;
use App\Models\Suppliers;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $totalUsers = User::where('status',1)->count();
        $totalSubscribeUsers = PurchasedSubsciptionPlan::where('status','active')->distinct('user_id')->count('user_id');
        $totalManufacturer = Manufacturer::where('status',1)->count();
        $totalSupplier = Suppliers::where('status',1)->count();
        $manufacturerData = Manufacturer::where('status',1)->get();
        $supplierData = Suppliers::where('status',1)->get();
        $parentProduct = ParentProduct::where('status',1)->count();
        return view('admin.admin.dashboard',compact('totalUsers','totalSubscribeUsers','totalManufacturer','totalSupplier','manufacturerData','supplierData', 'parentProduct'));
    }
}
