<?php

namespace App\DataTables;

use App\Models\CmsPage;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CmsPageDataTable extends DataTable
{

    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
        ->addIndexColumn()
        ->addColumn('action', function ($data) {
            $action = "";
            $action .= '<a href="'.route('admin.cms-page-edit', ['id' => Crypt::encrypt($data->id)]).'" class="btn btn-outline-success mr-1"><i class="fa fa-edit" title="Edit"></i></a>';
            return $action;
        })->rawColumns(['action']);
    }

    public function query(CmsPage $model): QueryBuilder
    {
        return $model->newQuery();
    }

    public function html(): HtmlBuilder
    {
         return $this->builder()
                ->setTableId('cms-page-table')
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->dom('Blfrtip')
                ->orderBy(1)
                ->buttons([
                ]);
    }

    public function getColumns(): array
    {
       return [
            Column::make('DT_RowIndex')->title('Sr No.')->searchable(false)->orderable(false),
            Column::make('id')->searchable(false)->exportable(true)->hidden(true),
            Column::make('title')->title('title'),
            Column::make('description')->title('Description')->searchable(false)->orderable(false),
            Column::computed('action')->title('Actions')->exportable(false)->printable(false),
        ];
    }

    protected function filename(): string
    {
        return 'CmsPage_' . date('YmdHis');
    }
}
