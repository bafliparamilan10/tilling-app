<div class="row multiple-form-boarder mt-3 p-3 supplier-div">

    <div class="form-group col-md-6">
        <label for="email">Supplier Name</label>
        <select id="supplier_ids{{ $index }}"  name="supplier_ids[]" class="form-control allfield">
            <option value="">Select Supplier Name</option>
            @foreach ($suppliers as $suppliersValue)
                <option value="{{ $suppliersValue->id }}">{{ $suppliersValue->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="username">Supplier Product Link (url) </label>
        <input type="url" class="form-control allfield" id="product_link{{ $index }}" name="product_link[]" placeholder="Enter product link">
    </div>

    <div class="form-group col-md-6">
        <label for="username">Supplier Product MDL (#) </label>
        <input type="text" class="form-control allfield" id="product_mdl{{ $index }}" name="product_mdl[]" placeholder="Enter product MDL">
    </div>

    <div class="form-group col-md-6">
        <label for="">Product Package Type</label>
        <input type="text" class="form-control product_package_type_id_text" id="product_package_type_id_text{{ $index }}" name="product_package_type_id_text[]" readonly>
    </div>

    <div class="form-group col-md-6 d-flex">
        <div class="w-100">
            <label for="">Product Size (width/ft.) Quantity</label>
            <select id="product_size_width_id{{ $index }}"  name="product_size_width_id[]" class="form-control product_size_width_id allfield" onchange="getProductPrice({{ $index }})">
            <option value="">Select Product Size (width/ft.) Quantity</option>
            @foreach ($productSizeWidthQty as $productSizeWidthQtyValue)
                <option value="{{ $productSizeWidthQtyValue->id }}">{{ $productSizeWidthQtyValue->size }}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('product-size-width-qty');"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div class="form-group col-md-6 d-flex">
        <div class="w-100">
            <label for="">Product Size (length/ft.) Quantity</label>
            <select id="product_size_length_id{{ $index }}"  name="product_size_length_id[]" class="form-control product_size_length_id allfield" onchange="getProductPrice({{ $index }})">
            <option value="">Select Product Size (length/ft.) Quantity</option>
            @foreach ($productSizeLengthQty as $productSizeLengthQtyValue)
                <option value="{{ $productSizeLengthQtyValue->id }}">{{ $productSizeLengthQtyValue->size }}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('product-size-length-qty');"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div class="form-group col-md-6 d-flex">
        <div class="w-100">
            <label for="">Product Size (thickness/inch.)</label>
            <select id="product_size_thickness_id{{ $index }}"  name="product_size_thickness_id[]" class="form-control product_size_thickness_id allfield">
            <option value="">Select Product Size (thickness/inch.)</option>
            @foreach ($productSizeThickness as $productSizeThicknessValue)
                <option value="{{ $productSizeThicknessValue->id }}">{{ $productSizeThicknessValue->size }}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('product-size-thickness-qty');"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div class="col-12">
        <h4 class="m-4">Specifications (product) :</h4>
    </div>

    <div class="form-group col-md-6 d-flex">
        <div class="w-100">
            <label for="">Spec. - # Fasteners (per sq. ft.) req.</label>
            <select id="specification_fastener_id{{ $index }}"  name="specification_fastener_id[]" class="form-control specification_fastener_id allfield">
            <option value="">Select Fasteners (per sq. ft.)</option>
            @foreach ($specificationFasteners as $specificationFastenersValue)
                <option value="{{ $specificationFastenersValue->id }}">{{ $specificationFastenersValue->squer_foot }}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('spec-fastener');"><i class="fa fa-plus"></i></button>
        </div>
    </div>


    <div class="form-group col-md-6 d-flex">
        <div class="w-100">
            <label for="">Spec. - # Fasteners Screw (length/inch.) req.</label>
            <select id="specification_fastener_screw_id{{ $index }}"  name="specification_fastener_screw_id[]" class="form-control specification_fastener_screw_id allfield">
            <option value="">Select Fasteners Screw (length/inch.)</option>
            @foreach ($specificationFastenersScrew as $specificationFastenersScrewValue)
                <option value="{{ $specificationFastenersScrewValue->id }}">{{ $specificationFastenersScrewValue->length_inch }}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('spec-fastener-screw');"><i class="fa fa-plus"></i></button>
        </div>
    </div>


    <div class="form-group col-md-6 d-flex">
        <div class="w-100">
            <label for="">Spec. - # Fasteners Nail (length/inch.) req.</label>
            <select id="specification_fastener_nail_id{{ $index }}"  name="specification_fastener_nail_id[]" class="form-control specification_fastener_nail_id allfield">
            <option value="">Select Fasteners Nail (length/inch.)</option>
            @foreach ($specificationFastenersNail as $specificationFastenersNailValue)
                <option value="{{ $specificationFastenersNailValue->id }}">{{ $specificationFastenersNailValue->length_inch }}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group plus-btn-div">
            <button type="button" class="btn btn-primary" onclick="managePopup('spec-fastener-nail');"><i class="fa fa-plus"></i></button>
        </div>

    </div>

    <div class="col-12 m-3">
    </div>

    <div class="form-group col-md-6">
        <label for="username">Product Price ($) </label>
        <input type="number" class="form-control allfield" id="product_price{{ $index }}" name="product_price[]" placeholder="Enter product price" min="0" onkeyup="getProductPrice({{ $index }});">
    </div>

    <div class="form-group col-md-6">
        <label for="username">Product Price (Per Sq. Ft. in $) </label>
        <input type="number" class="form-control" id="product_price_per_sq{{ $index }}" name="product_price_per_sq[]" placeholder="Enter product price" min="0" readonly>
    </div>

    <div class="form-group col-md-6">
        <label for="">Status</label>
        <select id="status{{ $index }}"  name="status[]" class="form-control allfield">
          <option value="">Select Status</option>
          <option value="1">Active</option>
          <option value="0">InActive</option>
        </select>
    </div>

    {{-- <a class="btn btn-app remove_button remove-mutiple float-right" ><i class="fas fa-minus"></i></a></div> --}}
</div>
