<?php

namespace App\Interfaces\admin;

interface ChildProductRepositoryInterface
{
    public function storeChildProduct(array $data);
    public function deleteChildProduct(array $data);
    public function enableDisableChildProduct(array $data);
    public function createColorChildProduct(array $data);
    public function createProductSizeLbs(array $data);
    public function createProductSizeGallon(array $data);
    public function createProductSizeCoverage(array $data);
    public function createProductOverage(array $data);
    public function updateDefaultDataSheet(array $data);
}