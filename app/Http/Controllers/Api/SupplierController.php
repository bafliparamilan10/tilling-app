<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\SupplierFavUnfavRequest;
use App\Interfaces\api\SupplierRepositoryInterface;
use App\Models\Suppliers;
use Illuminate\Http\Request;
use Laravel\Passport\HasApiTokens;

class SupplierController extends Controller
{
    use HasApiTokens;

    private SupplierRepositoryInterface $supplierRepository;

    public function __construct(SupplierRepositoryInterface $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }
    
    public function supplierList(Request $request)
    {
        $response = $this->supplierRepository->supplierList($request->all());
        
        return $response;
    }

    public function supplierFavUnfav(SupplierFavUnfavRequest $request)
    {
        $response = $this->supplierRepository->supplierFavUnfav($request->all());
        
        return $response;
    }

    public function supplierFavList(Request $request)
    {
        $response = $this->supplierRepository->supplierFavList($request->all());
        
        return $response;
    }
}
