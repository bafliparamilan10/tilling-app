<html>
<head>
    <title>Subscription Plan Receipt</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <h1 style="text-align: center; margin-bottom: 100px;">Subscription Plan Receipt</h1>
    <p><b>Invoice Date : </b> {{ date('Y-m-d') }}</p>
    <p><b>Subscription Plan Details</b></p>
  
    <table class="table table-bordered">
        <tr>
            <th>Name</th>
            <th>Plan Name</th>
            <th>Plan Price</th>
            <th>Plan Start Date</th>
            <th>Plan End Date</th>
        </tr>
        <tr>
            <td>{{ $user_name }}</td>
            <td>{{ $plan }}</td>
            <td>{{ $price }}</td>
            <td>{{ $start_date }}</td>
            <td>{{ $end_date }}</td>
        </tr>
    </table>
</body>
</html>