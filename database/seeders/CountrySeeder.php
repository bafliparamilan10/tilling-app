<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('countries')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $countries = array(
            array('id' => 233,'code' => 'US','name' => "United States",'phonecode' => 1),
        );

        DB::table('countries')->insert($countries);
    }
}
