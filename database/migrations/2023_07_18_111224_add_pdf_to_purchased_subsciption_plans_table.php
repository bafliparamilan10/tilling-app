<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('purchased_subsciption_plans', function (Blueprint $table) {
            $table->string('pdf' , 250)->nullable()->after('end_date');
        });
    }

    public function down(): void
    {
        Schema::table('purchased_subsciption_plans', function (Blueprint $table) {
            $table->dropColumn('pdf');
        });
    }
};
