@extends('admin.layouts.master')
@section('page_title', 'User List')
@section('content_header')

@push('customCSS')
<link  href="{{ asset('admin/dist/css/datatables/jquery.dataTables.min.css') }}" rel="stylesheet">
<link  href="{{ asset('admin/dist/css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link  href="{{ asset('admin/dist/css/datatables/buttons.dataTables.min.css') }}" rel="stylesheet">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{ asset('admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">User Management</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">User Management</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection
@section('content')
<section class="content">

    <div class="container-fluid">

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">User Management</h3>
                <a href="{{route('admin.user-add')}}" class="btn btn-outline-primary float-right"><i class="fa fa-plus"></i> Add User </a>
              </div>
              <div class="row">
                <div class="form-group col-md-3 ml-3 mt-3 row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">Status : </label>
                  <div class="col-sm-9">
                    <select class="form-control" id="status-filter" name="status-filter">
                      <option value="">All</option>
                      <option value="1">Active</option>
                      <option value="0">Inactive</option>
                    </select>
                  </div>
                </div>
                <div class="form-group col-md-5  mt-3 row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">Subscription : </label>
                  <div class="col-sm-9">
                    <select class="form-control" id="subscription-filter" name="subscription-filter">
                      <option value="">All</option>
                      <option value="free">Free</option>
                      <option value="pro">Pro</option>
                    </select>
                  </div>
                </div>
                <div class="form-group col-md-3  mt-3 row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">State : </label>
                  <div class="col-sm-9">
                    <select class="form-control" id="state-filter" name="state-filter">
                      <option value="">All</option>
                      @if(count($state) > 0)
                          @foreach ($state as $s)
                            <option value="{{$s->id}}">{{$s->name}}</option>
                          @endforeach
                      @endif
                    </select>
                  </div>
                </div>
              </div>

              <!-- ./card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  {{$dataTable->table(['class' => 'table table-bordered  table-striped ', 'id' => 'user-table'], true)}}
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div>

</section>
@endsection

@push('scripts')


    <!-- SweetAlert2 -->
    <script src="{{ asset('admin/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script src="{{ asset('admin/dist/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/dist/js/datatables/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/dist/js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/dist/js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/buttons.server-side.js')}}"></script>

    {{$dataTable->scripts()}}

@endpush
@push('customJs')
<!-- jquery-validation -->
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-validation/additional-methods.min.js') }}"></script>
@include('admin.customJs.user')
@endpush
