<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(SubscriptionPlanSeeder::class);
        // $this->call(CountrySeeder::class);
        // $this->call(StateSeeder::class);
        // $this->call(CitySeeder::class);
        $this->call(ProductPackageTypeSeeder::class);
        $this->call(ProductTypeSeeder::class);
        $this->call(MethodFilterSeeder::class);
        $this->call(AnsiSeeder::class);
        $this->call(QualityTypeSeeder::class);
        $this->call(ThinSetQtyTypeSeeder::class);
        $this->call(CmsPageSeeder::class);
    }
}
